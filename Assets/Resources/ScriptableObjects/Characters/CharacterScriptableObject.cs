﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ProjectGold
{
	public class CharacterScriptableObject : ScriptableObject
	{
		public string Name;

		public Sprite Icon;
		public Sprite LargeImage;

		public string[] Descriptions = { };

		public GameObject Prefab;

#if UNITY_EDITOR
		[MenuItem("Project Gold/キャラクター/新規キャラクターアセット作成")]
		static void CreateInstance()
		{
			var o = CreateInstance<CharacterScriptableObject>();

			AssetDatabase.CreateAsset(o, "Assets/Resources/ScriptableObjects/Characters/NewCharacter.asset");
			AssetDatabase.Refresh();
		}
#endif
	}
}