﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using ProjectGold;
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(CharacterScriptableObject))]
public class CharacterScriptableObjectInspector : Editor
{
	CharacterScriptableObject character;
	void OnEnable()
	{
		character = (CharacterScriptableObject)target;
	}

	public override void OnInspectorGUI()
	{
		EditorGUIUtility.labelWidth = 64;

		EditorGUI.BeginChangeCheck();

		EditorGUILayout.BeginHorizontal();

		character.Icon = (Sprite)EditorGUILayout.ObjectField(character.Icon, typeof(Sprite), false, GUILayout.Width(64),
			GUILayout.Height(64));

		EditorGUILayout.BeginVertical();

		character.Name = EditorGUILayout.TextField("名前", character.Name);
		character.Prefab = (GameObject)EditorGUILayout.ObjectField("プレハブ", character.Prefab, typeof(GameObject), false);

		EditorGUILayout.EndVertical();

		EditorGUILayout.EndHorizontal();

		EditorGUILayout.LabelField("メイン絵");

		character.LargeImage = (Sprite)EditorGUILayout.ObjectField(character.LargeImage, typeof(Sprite), false,
			GUILayout.Height(200),
			GUILayout.Width(200)
			);

		EditorGUILayout.LabelField("操作説明");

		var listDescr = new List<string>(character.Descriptions);

		int? deleteIndex = null;

		EditorGUILayout.Separator();
		for (int i = 0; i < listDescr.Count; i++)
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField((i + 1) + "ページ目");
			if (GUILayout.Button("削除"))
			{
				deleteIndex = i;
			}
			EditorGUILayout.EndHorizontal();

			listDescr[i] = EditorGUILayout.TextArea(
				character.Descriptions[i],
				GUILayout.MinHeight(EditorGUIUtility.singleLineHeight)
				);
			EditorGUILayout.Separator();
		}

		if (GUILayout.Button("説明を追加"))
		{
			listDescr.Add("");
		}
		if (deleteIndex.HasValue)
		{
			listDescr.RemoveAt(deleteIndex.Value);
			GUI.FocusControl("");
		}

		character.Descriptions = listDescr.ToArray();

		if (EditorGUI.EndChangeCheck())
			EditorUtility.SetDirty(character);
	}
}
#endif