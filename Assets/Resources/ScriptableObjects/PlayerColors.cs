﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ProjectGold
{
	public class PlayerColors : ScriptableObject
	{
		public Color[] Colors;

#if UNITY_EDITOR
		[MenuItem("Project Gold/色/新規プレイヤーカラーアセット作成")]
		static void CreateInstance()
		{
			var o = CreateInstance<PlayerColors>();

			AssetDatabase.CreateAsset(o, "Assets/Resources/ScriptableObjects/NewColors.asset");
			AssetDatabase.Refresh();
		}
#endif
	}
}
