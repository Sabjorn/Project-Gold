﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ProjectGold
{
	public class CommonParticles : ScriptableObject
	{
		public ParticleSystem GrabParticle, ReleaseParticle, HitParticle,CooledParticle;

#if UNITY_EDITOR
		[MenuItem("Project Gold/パーティクル/新規コモンパーティクルアセット作成")]
		static void CreateInstance()
		{
			var o = CreateInstance<CommonParticles>();

			AssetDatabase.CreateAsset(o, "Assets/Resources/ScriptableObjects/NewParticles.asset");
			AssetDatabase.Refresh();
		}
#endif
	}
}