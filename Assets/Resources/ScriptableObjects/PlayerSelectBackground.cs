﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ProjectGold
{
	public class PlayerSelectBackground : ScriptableObject
	{
		public Sprite[] PlayerBackgroundSprites;
		public Sprite ComBackgroundSprite;

#if UNITY_EDITOR
		[MenuItem("Project Gold/新規プレイヤーセレクト背景アセット作成")]
		static void CreateInstance()
		{
			var o = CreateInstance<PlayerSelectBackground>();

			AssetDatabase.CreateAsset(o, "Assets/Resources/ScriptableObjects/NewBackgrounds.asset");
			AssetDatabase.Refresh();
		}
#endif
	}
}
