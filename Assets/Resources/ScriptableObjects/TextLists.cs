﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectGold;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ProjectGold
{
	public class TextLists : ScriptableObject
	{
		public string[] Texts = {};

#if UNITY_EDITOR
		[MenuItem("Project Gold/テキスト/新規テキストデータリスト")]
		static void CreateInstance()
		{
			var o = CreateInstance<TextLists>();

			AssetDatabase.CreateAsset(o, "Assets/Resources/ScriptableObjects/newTextLists.asset");
			AssetDatabase.Refresh();
		}
#endif
	}
}
#if UNITY_EDITOR
[CustomEditor(typeof(TextLists))]
public class TextListsInspector : Editor
{
	TextLists textLists;
	void OnEnable()
	{
		textLists = (TextLists)target;
	}

	public override void OnInspectorGUI()
	{
		EditorGUI.BeginChangeCheck();

		var listDescr = new List<string>(textLists.Texts);

		int? deleteIndex = null;

		EditorGUILayout.Separator();
		for (int i = 0; i < listDescr.Count; i++)
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField((i + 1) + "ページ目");
			if (GUILayout.Button("削除"))
			{
				deleteIndex = i;
			}
			EditorGUILayout.EndHorizontal();

			listDescr[i] = EditorGUILayout.TextArea(
				textLists.Texts[i],
				GUILayout.MinHeight(EditorGUIUtility.singleLineHeight * 3.0f)
				);
			EditorGUILayout.Separator();
		}

		if (GUILayout.Button("テキストを追加"))
		{
			listDescr.Add("");
		}
		if (deleteIndex.HasValue)
		{
			listDescr.RemoveAt(deleteIndex.Value);
			GUI.FocusControl("");
		}

		textLists.Texts = listDescr.ToArray();
		if (EditorGUI.EndChangeCheck())
			EditorUtility.SetDirty(textLists);

	}
}
#endif