﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectGold.UI
{
	public abstract class UISelectable : MonoBehaviour
	{
		// 選択時
		public abstract void OnSelect();

		// カーソルフォーカス時
		public abstract void OnFocus();
		// カーソルフォーカス離脱時
		public abstract void OnUnfocus();
	}
}