﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace ProjectGold.UI
{
	public class UISelector : MonoBehaviour
	{
		public UnityEvent OnTick, OnDecide;

		private readonly Subject<int> _selectionSubject = new Subject<int>();
		public IObservable<int> SelectionObservable
		{
			get { return _selectionSubject; }
		}

		private int _position;
		public int Position
		{
			get
			{
				return _position;
			}
			set
			{
				SetPosition(value);
			}
		}

		private InputDevice enablePad;

		void Start()
		{
			var selectables = GetComponentsInChildren<UISelectable>();
			selectables[0].OnFocus();
			enablePad = null;
		}

		void Update ()
		{
			if (MappedInput.InputDevices.Any(pad => ((enablePad!= null && pad == enablePad) || (enablePad == null)) && pad.GetAxisButtonDown(MappedAxis.Vertical, AxisDirection.Negative)))
			{
				SetPosition(_position + 1);

				OnTick.Invoke();
			}
			if (MappedInput.InputDevices.Any(pad => ((enablePad!= null && pad == enablePad) || (enablePad == null)) && pad.GetAxisButtonDown(MappedAxis.Vertical, AxisDirection.Positive)))
			{
				SetPosition(_position - 1);

				OnTick.Invoke();
			}

			if (MappedInput.InputDevices.Any(pad => ((enablePad!= null && pad == enablePad) || (enablePad == null)) && 
													pad.GetButtonDown(MappedButton.Decide)))
			{
				var selectables = GetComponentsInChildren<UISelectable>();
				selectables[_position].OnSelect();

				OnDecide.Invoke();

				_selectionSubject.OnNext(_position);
			}
		}

		void SetPosition(int newPosition, bool clamp = false)
		{
			var selectables = GetComponentsInChildren<UISelectable>();

			if (clamp)
			{
				newPosition = Mathf.Clamp(newPosition, 0, selectables.Length - 1);
			}
			else
			{
				newPosition = (newPosition + selectables.Length) % selectables.Length;
			}

			if (newPosition == _position) return;

			selectables[_position].OnUnfocus();
			selectables[newPosition].OnFocus();

			_position = newPosition;
		}

		public void SetEnablePad(InputDevice pad){
			enablePad = pad;
		}
	}
}