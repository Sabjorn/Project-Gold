﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectGold.UI
{
	[RequireComponent(typeof(Animator))]
	public class UIAnimatorSelectable : UISelectable
	{
		private Animator _animator;

		public string SelectTrigger = "Select", FocusTrigger = "Focus", UnfocusTrigger = "Unfocus";

		void Awake()
		{
			_animator = GetComponent<Animator>();
		}

		public override void OnSelect()
		{
			_animator.SetTrigger(SelectTrigger);
		}

		public override void OnFocus()
		{
			_animator.SetTrigger(FocusTrigger);
		}

		public override void OnUnfocus()
		{
			_animator.SetTrigger(UnfocusTrigger);
		}
	}
}