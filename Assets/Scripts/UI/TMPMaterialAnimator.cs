﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TMPMaterialAnimator : MonoBehaviour
{
	[HideInInspector]
	public Color OutlineColor;

	private TextMeshProUGUI _text;
	
	void Start()
	{
		_text = GetComponent<TextMeshProUGUI>();
		OutlineColor = _text.outlineColor;

		this.ObserveEveryValueChanged(c => c.OutlineColor)
			.DistinctUntilChanged()
			.Subscribe(color =>
			{
				_text.outlineColor = color;
				_text.SetMaterialDirty();
			});
	}
}
