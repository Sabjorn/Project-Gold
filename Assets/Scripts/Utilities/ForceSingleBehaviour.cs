﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ProjectGold.Utilities
{
	public class ForceSingleBehaviour : MonoBehaviour
	{
		void Awake()
		{
			if (FindObjectsOfType<ForceSingleBehaviour>().Any(c => c != this && c.gameObject.name == gameObject.name))
			{
				gameObject.SetActive(false);
				Destroy(gameObject);
			}
		}
	}
}