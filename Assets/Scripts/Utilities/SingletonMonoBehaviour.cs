﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace ProjectGold.Utilities
{
	public class SingletonMonoBehaviour<T> : MonoBehaviour where T : SingletonMonoBehaviour<T>
	{
		protected static T instance;
		public static T Instance
		{
			get
			{
				if (instance == null)
				{
					instance = (T)FindObjectOfType(typeof(T));

					if (instance == null)
					{
						Debug.LogWarning(typeof(T) + "のインスタンスがありません");
					}
				}

				return instance;
			}
		}

		protected void Awake()
		{
			CheckInstance();
		}

		protected bool CheckInstance()
		{
			if (instance == null)
			{
				instance = (T)this;
				return true;
			}
			else if (Instance == this)
			{
				return true;
			}

			Debug.LogWarning("インスタンスが複数見つかりました。重複するインスタンスは削除されます");
			Destroy(this);
			return false;
		}
	}
}
