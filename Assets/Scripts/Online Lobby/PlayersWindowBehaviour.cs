﻿using System.Collections;
using System.Collections.Generic;
using ProjectGold.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

namespace ProjectGold.Online
{
	public class PlayersWindowBehaviour : SingletonMonoBehaviour<PlayersWindowBehaviour>
	{
		[SerializeField] private Transform playerEntityTransform;
		[SerializeField] private GameObject playerEntityPrefab;

		private readonly Dictionary<int, GameObject> connectionEntityDictionary = new Dictionary<int, GameObject>();

		public void RefreshEntities(int[] playerIds)
		{
			foreach (var entity in connectionEntityDictionary)
			{
				Destroy(entity.Value);
			}
			connectionEntityDictionary.Clear();

			foreach (var id in playerIds)
			{
				AddEntity(id);
			}
		}

		void AddEntity(int connectionId)
		{
			if (connectionEntityDictionary.ContainsKey(connectionId))
			{
				Debug.LogWarning("Connection ID Already Occupied.");
				return;
			}

			var go = Instantiate(playerEntityPrefab, playerEntityTransform, false);
			go.GetComponent<TextMeshProUGUI>().text = "Player " + connectionId;
			if (NetworkManager.singleton.client.connection.connectionId == connectionId)
				go.GetComponent<TextMeshProUGUI>().color = Color.red;
			connectionEntityDictionary.Add(connectionId, go);
		}

		void RemoveEntity(int connectionId)
		{
			if (!connectionEntityDictionary.ContainsKey(connectionId))
			{
				Debug.LogWarning("Connection ID Not Found.");
				return;
			}

			Destroy(connectionEntityDictionary[connectionId]);
			connectionEntityDictionary.Remove(connectionId);
		}
	}
}