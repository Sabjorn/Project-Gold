﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProjectGold.Online;
using ProjectGold.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UniRx;

public class ChatWindowBehaviour : SingletonMonoBehaviour<ChatWindowBehaviour>
{
	[SerializeField] private TMP_InputField InputField;
	[SerializeField]
	private TextMeshProUGUI TextUGUI;

	void Start()
	{
		TextUGUI.text = "";
	}

	public void Initialize()
	{
		Chatterer.Singleton.OnMessageObservable.Subscribe(_ =>
		{
			TextUGUI.text = Chatterer.Singleton.Messages.Reverse().Aggregate("", (a, e) => a + e + "\n");
		});
	}

	public void OnEnterText(string text)
	{
		InputField.text = "";
		if (LobbyPlayer.LocalInstance != null)
			LobbyPlayer.LocalInstance.CmdMessage(text);
	}
}
