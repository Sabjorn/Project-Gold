﻿using Open.Nat;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

namespace ProjectGold.Online
{
	public class OfflineLobbyManager : MonoBehaviour
	{
		[SerializeField] private TextMeshProUGUI addressText;

		[SerializeField] private GameObject chatWindow;

		private string _localIp;
		private string _globalIp;

		private string LocalIp
		{
			get { return _localIp; }
			set
			{
				_localIp = value;
				UpdateAddressText();
			}
		}

		private string GlobalIp
		{
			get { return _globalIp; }
			set
			{
				_globalIp = value;
				UpdateAddressText();
			}
		}

		void Start()
		{
			chatWindow.SetActive(false);
		}

		public void StartHost()
		{
			if (!NetworkManager.singleton.isNetworkActive)
			{
				var discoverer = new NatDiscoverer();

				var deviceTask = discoverer.DiscoverDeviceAsync();
				if (deviceTask.Wait(1000))
				{
					var ipTask = deviceTask.Result.GetExternalIPAsync();
					if (ipTask.Wait(1000))
					{
						GlobalIp = ipTask.Result.ToString();

						var portTcpTask = deviceTask.Result.CreatePortMapAsync(new Mapping(Protocol.Tcp, 7777, 7777));
						var portUdpTask = deviceTask.Result.CreatePortMapAsync(new Mapping(Protocol.Udp, 7777, 7777));

						if (!Task.WaitAll(new[] { portTcpTask, portUdpTask }, 1000))
							Debug.LogWarning("Port Mapping failed.");
					}
					else
					{
						Debug.LogWarning("Failed to get IP Address.");
					}
				}
				else
				{
					Debug.LogWarning("NAT Discoverer Device not found.");
				}

				LocalIp = Dns.GetHostAddresses(Dns.GetHostName()).First(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString();

				NetworkManager.singleton.StartHost();

				NetworkServer.RegisterHandler(MsgType.Connect, msg =>
				{
					NetworkManager.singleton.OnServerConnect(msg.conn);

					Debug.Log("Connection from " + msg.conn.address);

					NetworkServer.SpawnObjects();
				});
				NetworkServer.RegisterHandler(MsgType.Disconnect, msg =>
				{
					NetworkManager.singleton.OnServerDisconnect(msg.conn);

					Debug.Log("Disconnection of " + msg.conn.address);
				});
				NetworkServer.RegisterHandler(MsgType.Ready, msg =>
				{
					NetworkManager.singleton.OnServerReady(msg.conn);

					Debug.Log("Ready: " + msg.conn.address);
				});

				chatWindow.SetActive(true);

				Chatterer.Singleton.ServerClearMessages();
			}
		}

		public void StartClient()
		{
			if (!NetworkManager.singleton.isNetworkActive)
			{
				NetworkManager.singleton.StartClient();

				chatWindow.SetActive(true);
			}
		}

		public void Stop()
		{
			if (NetworkManager.singleton.isNetworkActive)
			{
				if (NetworkServer.active)
					NetworkManager.singleton.StopHost();
				else
					NetworkManager.singleton.StopClient();

				chatWindow.SetActive(false);
			}
		}

		void UpdateAddressText()
		{
			addressText.text = "<size=60%>ルームアドレス</size>\n<b>"
							   + (!string.IsNullOrEmpty(GlobalIp) ? GlobalIp : "-")
							   + "</b> <size=60%>(外部)</size> / <b>"
							   + (!string.IsNullOrEmpty(LocalIp) ? LocalIp : "-")
							   + "</b> <size=60%>(内部)</size>";
		}

		public void NextScene()
		{
			NetworkManager.singleton.ServerChangeScene("online_practice");
		}
	}
}