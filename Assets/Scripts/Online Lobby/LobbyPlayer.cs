﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace ProjectGold.Online
{
	public class LobbyPlayer : NetworkBehaviour
	{
		private static LobbyPlayer _localInstance;

		public static LobbyPlayer LocalInstance
		{
			get
			{
				if (_localInstance == null)
				{
					var players = FindObjectsOfType<LobbyPlayer>();
					if (players.Any(p => p.isLocalPlayer))
					{
						_localInstance = players.First(p => p.isLocalPlayer);
					}
				}
				return _localInstance;
			}
		}

		[Command]
		public void CmdMessage(string message)
		{
			Chatterer.Singleton.ServerAddMessage(message);
		}
	}
}