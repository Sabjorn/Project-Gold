﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;

public class Chatterer : NetworkBehaviour
{
	private static Chatterer _singleton;
	public static Chatterer Singleton
	{
		get
		{
			return _singleton ?? (_singleton = FindObjectOfType<Chatterer>());
		}
	}

	private readonly Subject<Unit> _messageSubject = new Subject<Unit>();
	public IObservable<Unit> OnMessageObservable { get { return _messageSubject; } }

	public SyncListString Messages = new SyncListString();

	void Start()
	{
		ChatWindowBehaviour.Instance.Initialize();

		Messages.Callback = (op, i) =>
		{
			_messageSubject.OnNext(Unit.Default);
		};
	}

	[Server]
	public void ServerAddMessage(string message)
	{
		Messages.Add(message);
	}

	[Server]
	public void ServerClearMessages()
	{
		Messages.Clear();
	}
}
