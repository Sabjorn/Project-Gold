﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProjectGold.Online;
using ProjectGold.Utilities;
using UnityEngine;
using UnityEngine.Networking;

public class OnlineLobbyManager : NetworkBehaviour
{
	[Server]
	void Update()
	{
		if (Time.frameCount % 30 == 0)
			RpcRefreshEntities(NetworkServer.connections.Select(c => c.connectionId).ToArray());
	}

	[ClientRpc]
	void RpcRefreshEntities(int[] playerIds)
	{
		PlayersWindowBehaviour.Instance.RefreshEntities(playerIds);
	}
}
