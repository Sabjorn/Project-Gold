﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InputDeviceViewer : MonoBehaviour
{
	public static int NumViewer = 0;

	public IControllerInput InputDevice;

	[SerializeField] private GameObject _selfPrefab;

	[SerializeField] private RectTransform _axesDotRectTransform;
	[SerializeField] private RectTransform _buttonsRectTransform;

	[SerializeField] private GameObject _buttonTextPrefab;

	private int _viewerNo;
	private Dictionary<MappedButton, TextMeshProUGUI> _buttonTextsDictionary;

	// Use this for initialization
	void Start()
	{
		_viewerNo = NumViewer;
		NumViewer++;

		foreach (RectTransform t in _buttonsRectTransform)
		{
			Destroy(t.gameObject);
		}

		_buttonTextsDictionary = new Dictionary<MappedButton, TextMeshProUGUI>();
		foreach (var mappedButton in (MappedButton[])Enum.GetValues(typeof(MappedButton)))
		{
			var go = Instantiate(_buttonTextPrefab, _buttonsRectTransform, false);

			var text = go.GetComponent<TextMeshProUGUI>();
			text.text = mappedButton.ToString();

			_buttonTextsDictionary[mappedButton] = text;
		}
	}

	// Update is called once per frame
	void Update()
	{
		if (Time.frameCount % 10 == 0)
		{
			var devices = FindObjectsOfType<NetworkCharacterController>();
			if (devices.Length > _viewerNo)
				InputDevice = FindObjectsOfType<NetworkCharacterController>()[_viewerNo];
			else if (_viewerNo > 0) Destroy(gameObject);

			if (devices.Length > NumViewer)
			{
				Instantiate(_selfPrefab, transform.parent, false);
			}
		}

		Vector2 anchorVector = InputDevice != null
			? new Vector2(InputDevice.GetAxis(MappedAxis.Horizontal) * 0.5f + 0.5f, InputDevice.GetAxis(MappedAxis.Vertical) * 0.5f + 0.5f)
			: Vector2.one * 0.5f;
		_axesDotRectTransform.anchorMax = anchorVector;
		_axesDotRectTransform.anchorMin = anchorVector;

		foreach (var kv in _buttonTextsDictionary)
		{
			if (InputDevice != null)
				kv.Value.color = InputDevice.GetButton(kv.Key) ? Color.white : new Color(1, 1, 1, 0.5f);
		}
	}

	void OnDestroy()
	{
		NumViewer--;
	}
}
