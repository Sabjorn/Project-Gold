﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using ProjectGold.Utilities;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectGold.Game
{
	public class ResultEntityBehaviour : MonoBehaviour
	{
		public TextMeshProUGUI PlayerLabel;
		public RectTransform PointsTransform;
		public GameObject ScorePrefab;

		private readonly List<GameObject> _scoreEntities = new List<GameObject>();

		private int _playerNo;
		public int PlayerNo
		{
			get { return _playerNo; }
			set
			{
				_playerNo = value;
				PlayerLabel.text = String.Format("{0}P", value + 1);
			}
		}

		void Awake()
		{
			for (int i = 0; i < Environment.Game.ScoreThreshold; i++)
			{
				var e = Instantiate(ScorePrefab, PointsTransform, false);
				e.GetComponent<Image>().color = new Color(1, 1, 1, 0.2f);

				_scoreEntities.Add(e);
			}
		}

		public void SetImmediate(int score)
		{
			for (int i = 0; i < Environment.Game.ScoreThreshold; i++)
			{
				_scoreEntities[i].GetComponent<Image>().color = i < Environment.Game.Scores[PlayerNo] ? Color.white : new Color(1, 1, 1, 0.2f);
			}
		}

		public void SetWinner()
		{
			this.UpdateAsObservable()
				.Select(_ => (Mathf.Sin(Time.time * 2 * Mathf.PI) + 1) / 2)
				.Subscribe(v =>
				{
					_scoreEntities[Environment.Game.Scores[PlayerNo] - 1].transform.localScale = Vector3.one * (1 + v * 0.2f);
				});
		}
	}
}