﻿using System.Collections;
using System.Collections.Generic;
using ProjectGold.Character;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace ProjectGold
{
	public class AISenseZoneBehaviour : MonoBehaviour
	{
		public int PlayerNo;
		public ZoneBehaviour Zone;

		private int _numGold;
		public int NumGold { get { return _numGold; } }

		public bool hasRedGold; 


		void Awake()
		{
			PlayerNo = Zone.PlayerNo;
			Zone.ObserveEveryValueChanged(z => z.PlayerNo)
			.DistinctUntilChanged()
			.Subscribe(no =>
			{
				PlayerNo = no;
			});
			hasRedGold = false;
		}

		void Update ()
		{
			int count = 0;
			foreach (var coin in GameObject.FindGameObjectsWithTag("Gold")) {
				if (coin.GetComponent<GoldBehaviour> ().nowSenseZone == this) {
					count++;
				}
			}
			if (count < _numGold) {
				_numGold = count;
				Debug.Log("adjuest coin count");
			}
		}

		void OnTriggerEnter (Collider col)
		{
			var gold = col.gameObject.GetComponent<GoldBehaviour> ();
			if (gold != null) {
				if(gold.GetType() == typeof(GoldBehaviour)){
					_numGold++;
				}else if(gold.GetType() == typeof(RedGoldBehaviour)){
					hasRedGold = true;
				}
				gold.nowSenseZone = this;
			}
		}

		void OnTriggerExit (Collider col)
		{
			var gold = col.gameObject.GetComponent<GoldBehaviour> ();
			if (gold != null) {
				if (gold.GetType () == typeof(GoldBehaviour)) {
					_numGold--;
				}else if(gold.GetType() == typeof(RedGoldBehaviour)){
					hasRedGold = false;
				}
				gold.nowSenseZone = null;
			}
		}
	}
}