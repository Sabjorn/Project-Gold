﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectGold.Character;
using UniRx;
using UnityEngine;

namespace ProjectGold
{
	public enum OccupationTier
	{
		Grab = 0, Skill = 1, Contact = 2
	}

	public class GrabbableBehaviour : MonoBehaviour
	{
		public bool IsGrabbable;

		private Tuple<CharacterBaseBehaviour, OccupationTier>? _occupation = null;
		private IDisposable _occupationSubscription;
		public CharacterBaseBehaviour grabPlayer;

		public CharacterBaseBehaviour OccupiedBy
		{
			get { return _occupation.HasValue ? _occupation.Value.Item1 : null; }
		}

		protected void Awake()
		{
			IsGrabbable = true;
			grabPlayer = null;
		}

		public void OnGrabbed()
		{
			var colliders = GetComponents<Collider>();
			foreach (var col in colliders)
			{
				col.gameObject.layer = 11;
			}
		}

		public void OnReleased()
		{
			var colliders = GetComponents<Collider>();
			foreach (var col in colliders)
			{
				col.gameObject.layer = 10;
			}
		}

		public void OccupyTime(CharacterBaseBehaviour character, OccupationTier tier, float duration = 0.5f)
		{
			if (_occupation == null || _occupation.Value.Item2 >= tier)
			{
				if (_occupationSubscription != null) _occupationSubscription.Dispose();
				_occupationSubscription = Observable.Defer(() =>
				{
					_occupation = new Tuple<CharacterBaseBehaviour, OccupationTier>(character, tier);
					return Observable.Timer(TimeSpan.FromSeconds(duration));
				}).Subscribe(_ =>
				{
					_occupation = null;
				});
			}
		}

		public void Occupy(CharacterBaseBehaviour character, OccupationTier tier)
		{
			if (_occupation == null || _occupation.Value.Item2 >= tier)
			{
				if (_occupationSubscription != null) _occupationSubscription.Dispose();
				_occupation = new Tuple<CharacterBaseBehaviour, OccupationTier>(character, tier);
			}
		}

		public void Free(CharacterBaseBehaviour character, OccupationTier tier)
		{
			if (_occupation != null && _occupation.Value.Item1 == character && _occupation.Value.Item2 >= tier)
			{
				_occupation = null;
			}
		}
	}
}