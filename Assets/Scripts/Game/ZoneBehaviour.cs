﻿using System.Collections;
using System.Collections.Generic;
using ProjectGold.Character;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace ProjectGold
{
	public class ZoneBehaviour : MonoBehaviour
	{
		public int PlayerNo;

		public int MaxGolds;

		private Subject<Unit> _filledSubject;
		public IObservable<Unit> OnFilledObservable { get { return _filledSubject; } }

		private Subject<CharacterBaseBehaviour> _repeledSubject;

		public IObservable<CharacterBaseBehaviour> OnRepeledObservable
		{
			get
			{
				return _repeledSubject
					.Sample(this.LateUpdateAsObservable());
			}
		}

		private int _numGold;
		public int NumGold { get { return _numGold; } }

		private AudioSource _audioSource;

		void Awake()
		{
			_filledSubject = new Subject<Unit>();
			_repeledSubject = new Subject<CharacterBaseBehaviour>();

			_audioSource = GetComponent<AudioSource>();
		}

		void Update ()
		{
			int count = 0;
			foreach (var coin in GameObject.FindGameObjectsWithTag("Gold")) {
				if (coin.GetComponent<GoldBehaviour> ().nowZone == this) {
					count++;
				}
			}
			if (count < _numGold) {
				_numGold = count;
			}
			if (_numGold >= MaxGolds)
			{
				_filledSubject.OnNext(Unit.Default);

				if (!_audioSource.isPlaying) _audioSource.Play();
			}
			else
			{
				if(_audioSource.isPlaying)_audioSource.Stop();
			}
		}

		void OnTriggerEnter (Collider col)
		{
			var gold = col.gameObject.GetComponent<GoldBehaviour> ();
			if (gold != null) {
				if(gold.GetType() == typeof(GoldBehaviour)){
					_numGold++;
				}
				gold.nowZone = this;
			}
			if (col.gameObject.tag == "Character") {
				if (col.gameObject.GetComponent<CharacterBaseBehaviour> ().PlayerNo == this.PlayerNo) {
					col.gameObject.GetComponent<CharacterBaseBehaviour> ().onMyZone = true;
				}
            }
		}

		void OnTriggerExit (Collider col)
		{
			var gold = col.gameObject.GetComponent<GoldBehaviour> ();
			if (gold != null) {
				if (gold.GetType () == typeof(GoldBehaviour)) {
					_numGold--;
				}
                col.gameObject.GetComponent<GoldBehaviour>().nowZone = null;

				var repeledBy = col.gameObject.GetComponent<GoldBehaviour>().OccupiedBy;

				if (repeledBy != null)
				{
					_repeledSubject.OnNext(repeledBy);
				}
			}
            if (col.gameObject.tag == "Character")
            {
                col.gameObject.GetComponent<CharacterBaseBehaviour>().onMyZone = false;
            }
		}
	}
}