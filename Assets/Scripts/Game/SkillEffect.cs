﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace ProjectGold
{
	/// <summary>
	/// スキル効果の種類。
	/// </summary>
	public enum EffectTag
	{
		SkillCasting,   // 詠唱中のスキル。	ex) ジグスのQ
		SkillActive,    // 発動中のスキル。	ex) ダッシュ
		Debuf,          // デバフ。			ex) スタン
		SkillIntrinsic  // 解除不能スキル。	ex) ワーウィックのパッシブ
	}

	/// <summary>
	/// スキル効果のクラス。
	/// </summary>
	public class Effect : IDisposable
	{
		private bool _isDone;
		/// <summary>
		/// スキル効果が終了したかどうか。
		/// </summary>
		public bool IsDone { get { return _isDone; } }

		/// <summary>
		/// スキルのタグ。
		/// </summary>
		public HashSet<EffectTag> Tags;

		private IObservable<Unit> _sequence;
		private CompositeDisposable _subscription;

		/// <summary>
		/// 新しいスキル効果を作成する。
		/// </summary>
		/// <param name="sequence">スキル効果が記述されたストリーム。</param>
		/// <param name="cancelAction">スキル効果キャンセル時の処理。</param>
		public Effect(IObservable<Unit> sequence, Action cancelAction)
		{
			_isDone = false;
			_sequence = sequence;

			_subscription = new CompositeDisposable(Disposable.Create(() =>
			 {
				 cancelAction();
				 _isDone = true;
			 }));
		}

		/// <summary>
		/// スキル効果を発動する。
		/// </summary>
		public void Subscribe()
		{
			_sequence
				.DoOnCompleted(() => { _isDone = true; })
				.Subscribe()
				.AddTo(_subscription);
		}

		/// <summary>
		/// スキル効果をキャンセルする。
		/// </summary>
		public void Dispose()
		{
			_subscription.Dispose();
		}
	}
}