﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;

namespace ProjectGold.Game
{
	[ExecuteInEditMode]
	public class PracticePanelBehaviour : MonoBehaviour
	{
		public CharacterScriptableObject Character;
		public int PlayerNo;
		public float DescrDisplayTime;

		[SerializeField] private TextMeshProUGUI PlayerNoText, CharacterText, DescriptionText, PageText, ReadyText;

		private Animator _animator;

		private int _descrPage;

		private PlayerColors _colors;

		void Start()
		{
			_animator = GetComponent<Animator>();
			ReadyText.enabled = false;

			_colors = Resources.Load<PlayerColors>("ScriptableObjects/DefaultColors");

			Observable.Interval(TimeSpan.FromSeconds(DescrDisplayTime))
				.Subscribe(_ =>
				{
					_descrPage = (_descrPage + 1 + Character.Descriptions.Length);
					_descrPage %= Character.Descriptions.Length;
				});
		}

		void Update()
		{
			if (PlayerNo < MappedInput.InputDevices.Count)
			{
				var pad = MappedInput.InputDevices[PlayerNo];

				if (Character.Descriptions.Length > 0)
				{
					if (pad.GetButtonDown(MappedButton.LTrigger))
					{
						// _descrPage = (_descrPage + 1 + Character.Descriptions.Length);
					}
					if (pad.GetButtonDown(MappedButton.RTrigger))
					{
						// _descrPage = (_descrPage - 1 + Character.Descriptions.Length);
					}
				}
			}
			_descrPage %= Character.Descriptions.Length;

			PlayerNoText.text = "Player " + (PlayerNo + 1);
			PlayerNoText.color = _colors.Colors[PlayerNo];

			CharacterText.text = Character.Name;
			if (Character.Descriptions.Length > 0)
			{
				DescriptionText.text = Character.Descriptions[_descrPage];
				PageText.text = (_descrPage + 1) + " / " + Character.Descriptions.Length;
			}
		}

		public void Ready()
		{
			_animator.SetTrigger("Ready");
		}
	}
}