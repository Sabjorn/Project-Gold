﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProjectGold.UI;
using ProjectGold.Character;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityEngine.SceneManagement;
using TMPro;

namespace ProjectGold.Game
{
	public class PracticePauseBehaviourScript : MonoBehaviour
	{
		public UISelector Selector;

		private CanvasGroup _canvasGroup;

		private bool _isOpen;

		private InputDevice enablePad;

		[SerializeField] private TextMeshProUGUI[] playerSettingTexts;

		public GameObject RedGoldPrefab;

		void Start ()
		{
			_canvasGroup = GetComponent<CanvasGroup> ();
			enablePad = null;
			_isOpen = false;

			Selector.enabled = false;

			foreach (var character in PracticeNewWatcher.Instance.Characters) {
				character.isAI = Environment.Game.CharacterAIFlags [character.PlayerNo];
			}
			for (int i = 0; i < Environment.Game.CharacterAIFlags.Length; i++) {
				if (Environment.Game.CharacterAIFlags [i]) {
					playerSettingTexts[i].text = string.Format("{0}P: COM", i + 1);
				} else {
					playerSettingTexts[i].text = string.Format("{0}P: プレイヤー", i + 1);
				}
			}

			Selector.SelectionObservable.Subscribe(selection =>
			{
				switch(selection){
				case 0:
					Close();
					break;
				case 1:// red coin
					var redGold = Instantiate (RedGoldPrefab);
					redGold.transform.position = new Vector3 (UnityEngine.Random.Range (-12.0f, 12.0f), 1.0f, UnityEngine.Random.Range (-12.0f, 12.0f));
					break;
				case 2:
				case 3:
				case 4:
				case 5:
					int num = selection - 2;
					if(PracticeNewWatcher.Instance.Characters[num].isAI){
						PracticeNewWatcher.Instance.Characters[num].ToggleAIController(false);
						playerSettingTexts[num].text = string.Format("{0}P: プレイヤー", num + 1);
					}else{
						PracticeNewWatcher.Instance.Characters[num].ToggleAIController(true);
						playerSettingTexts[num].text = string.Format("{0}P: COM", num + 1);
					}
					break;
				case 6:
					Close();
					SceneManager.LoadScene("Character_Select");
					break;
				}
			});


		
		}

		void Update ()
		{
			if (!_isOpen) {
				var pad = MappedInput.InputDevices.Find (p => p.GetButtonDown (MappedButton.Start));
				if (pad != null) {
					Open(pad);
				}
			}
			else if (_isOpen) {
				if (enablePad != null && enablePad.GetButtonDown (MappedButton.Start)) {
					Close();
				}
			}
		}

		void Open(InputDevice pad){
			Time.timeScale = 0;
			_isOpen = true;
			Selector.enabled = true;
			Selector.SetEnablePad (enablePad);
			enablePad = pad;

			foreach (var c in GameObject.FindObjectsOfType<CharacterBaseBehaviour>()) {
				c.IsControllable = false;
			}

			Observable.Range (1, 10).Select (v => v / 10f)
			.Zip (this.UpdateAsObservable (), (a, b) => a)
			.Subscribe (v => {
				_canvasGroup.alpha = v;
			}, () => {
				_canvasGroup.alpha = 1;
				_canvasGroup.blocksRaycasts = true;
			});
		}

		void Close(){
			Time.timeScale = 1;
			_isOpen = false;
			//enablePad = null;

			Selector.enabled = false;

			foreach (var c in GameObject.FindObjectsOfType<CharacterBaseBehaviour>()) {
				c.IsControllable = true;
			}

			Observable.Range(1, 10).Select(v => v / 10f)
				.Zip(this.UpdateAsObservable(), (a, b) => a)
				.Subscribe(v =>
				{
					_canvasGroup.alpha = 1 - v;
				}, () =>
				{
					_canvasGroup.alpha = 0;
					_canvasGroup.blocksRaycasts = false;
				});
		}
	}
}