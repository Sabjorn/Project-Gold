﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectGold.Character;
using ProjectGold.Game;
using ProjectGold.Utilities;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace ProjectGold.Game
{
	public class PracticeNewWatcher : SingletonMonoBehaviour<PracticeNewWatcher>
	{
		private CharacterBaseBehaviour[] _characters;
		public CharacterBaseBehaviour[] Characters { get { return _characters; } }

		void Start()
		{
			_characters = new CharacterBaseBehaviour[Environment.Game.CharacterSelections.Length];

			for (int i = 0; i < Environment.Game.CharacterSelections.Length; i++)
			{
				CharacterScriptableObject charAsset;
				if (Environment.Game.CharacterSelections [i].Name == "ランダム") {
					charAsset = Environment.Game.GetRandomCharacterScriptableObjects();
				} else {
					charAsset = Environment.Game.CharacterSelections [i];
				}

				// キャラ生成・配置
				var charObject = Instantiate(charAsset.Prefab);
				//charObject.transform.position = Vector3.Lerp(new Vector3(-10, 1, -10), new Vector3(10, 1, -10), (float)i / Environment.Game.CharacterSelections.Length);
				charObject.transform.rotation = Quaternion.identity;

				// キャラ初期化
				_characters[i] = charObject.GetComponent<CharacterBaseBehaviour>();
				_characters[i].SetDefaultPosition(i);
				_characters[i].PlayerNo = i;
				_characters[i].TeamNo = i;
				_characters[i].IsControllable = true;
				_characters[i].isAI = Environment.Game.CharacterAIFlags[i];
				if (MappedInput.InputDevices.Count > i)
					_characters[i].Controller = new LocalInputDevice(MappedInput.InputDevices[i]);
				else
					_characters[i].Controller = new LocalInputDevice();
				
				// Pause画面初期化

			}

		}

	}
}