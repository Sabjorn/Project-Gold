﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectGold.Character;
using ProjectGold.Game;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace ProjectGold.Game
{
	public class PracticeWatcher : MonoBehaviour
	{
		public float Timer;

		public UnityEvent OnReady;

		[SerializeField] private PracticePanelBehaviour[] _panels;
		[SerializeField] private CanvasGroup _standbyCanvasGroup;

		[SerializeField] private PlayerColors _playerColors;

		[SerializeField] private TextMeshProUGUI _timerText;

		private bool[] _oks;

		private bool _isStandby;

		void Start()
		{
			var _characters = new CharacterBaseBehaviour[Environment.Game.CharacterSelections.Length];

			for (int i = 0; i < Environment.Game.CharacterSelections.Length; i++)
			{
				var charAsset = Environment.Game.CharacterSelections[i];

				// キャラ生成・配置
				var charObject = Instantiate(charAsset.Prefab);
				charObject.transform.position = Vector3.Lerp(new Vector3(-10, 1, -10), new Vector3(10, 1, -10), (float)i / Environment.Game.CharacterSelections.Length);
				charObject.transform.rotation = Quaternion.identity;

				// キャラ初期化
				_characters[i] = charObject.GetComponent<CharacterBaseBehaviour>();
				_characters[i].PlayerNo = i;
				_characters[i].TeamNo = i;
				if(Environment.Game.CharacterAIFlags[i] == false) _characters[i].IsControllable = true;
				if (MappedInput.InputDevices.Count > i)
					_characters[i].Controller = new LocalInputDevice(MappedInput.InputDevices[i]);
				else
					_characters[i].Controller = new LocalInputDevice();

				// パネル初期化
				_panels[i].PlayerNo = i;
				_panels[i].Character = charAsset;
			}

			float timer = Timer;
			this.UpdateAsObservable()
				.Do(_ =>
				{
					timer -= Time.deltaTime;
					if (timer < 0) timer = 0;

					var timespan = TimeSpan.FromSeconds(timer);
					_timerText.text = string.Format("{0:0}:{1:00}", timespan.Minutes, timespan.Seconds);
				}).TakeWhile(_ => timer > 0)
				.Subscribe(_ => { }, () =>
				{
					for (int i = 0; i < 4; i++)
					{
						_oks[i] = true;
						_panels[i].Ready();
					}
				});

			_oks = new bool[Environment.Game.CharacterSelections.Length];

			_isStandby = false;

			_standbyCanvasGroup.alpha = 0;
		}

		void Update()
		{
			int i = 0;
			foreach (var pad in MappedInput.InputDevices)
			{
				if (pad.GetButtonDown(MappedButton.Start))
				{
					OnReady.Invoke();
					_oks[i] = true;
					_panels[i].Ready();
				}
				i++;
			}
			if (!_isStandby)
			{
				bool flag = true;
				foreach (var b in _oks)
				{
					flag &= b;
				}
				if (flag)
				{
					_isStandby = true;
					_standbyCanvasGroup.alpha = 1;
					Observable.Timer(TimeSpan.FromSeconds(3.0)).Subscribe(_ =>
					{
						switch (Environment.Game.GameModeSelection)
						{
							case Environment.Game.GameMode.DeathMatch:
								SceneManager.LoadScene("death_4");
								break;
							case Environment.Game.GameMode.TeamMatch:
								SceneManager.LoadScene("team_2v2");
								break;
						}
					});
				}
			}
		}
	}
}