﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProjectGold.Character;
using ProjectGold.Utilities;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace ProjectGold.Game
{
	public class DeathMatchInitializer : SingletonMonoBehaviour<DeathMatchInitializer>
	{
		public Transform[] SpawnTransforms;
		public ZoneBehaviour[] Zones;
		public AISenseZoneBehaviour[] SenseZones;
		public Transform[] ZonePositions;
		public PercentageBehaviour[] Percentages;
		public RectTransform[] PercentagePositions;

		public CharacterBaseBehaviour[] Characters;

		void Start ()
		{
			if (Environment.isInitialized == false)
				Environment.Initialize ();
			Resources.Load<GameObject> ("Prefabs/Game/Nice");
			var shuffleNumbers = new int[]{0, 1, 2, 3};
			if(Environment.Game.RandomPosition) shuffleNumbers = shuffleNumbers.OrderBy(_ => Guid.NewGuid()).ToArray();

			Characters = new CharacterBaseBehaviour[Environment.Game.CharacterSelections.Length];


			//TODO AI_DEBUG
			int j = 0;
			foreach (var c in GameObject.FindGameObjectsWithTag("Character")) {
				Characters [j] = c.GetComponent<CharacterBaseBehaviour> ();
				j++;
			}



			for (int i = 0; i < Environment.Game.CharacterSelections.Length; i++) {
				if (Characters [i] == null) {//TODO
					CharacterScriptableObject charAsset;
					if (Environment.Game.CharacterSelections [i].Name == "ランダム") {
						if (Environment.Game.LastCharacterSelections [i] != null) {
							charAsset = Environment.Game.LastCharacterSelections [i];
						} else {
							var randomCharacter = Environment.Game.GetRandomCharacterScriptableObjects();
							Environment.Game.LastCharacterSelections[i] = randomCharacter;
							charAsset = randomCharacter;
						}
					} else {
						charAsset = Environment.Game.CharacterSelections [i];
					}
					var charObject = Instantiate (charAsset.Prefab);
					charObject.transform.position = SpawnTransforms [shuffleNumbers[i]].position;
					charObject.transform.rotation = Quaternion.identity;

					Characters [i] = charObject.GetComponent<CharacterBaseBehaviour> ();
				}
				Characters [i].PlayerNo = i;
				Characters [i].TeamNo = i;
				Characters [i].IsControllable = false;

				Characters[i].isAI = Environment.Game.CharacterAIFlags[i];
				if (MappedInput.InputDevices.Count > i)
					Characters[i].Controller = new LocalInputDevice(MappedInput.InputDevices[i]);
				else
					Characters[i].Controller = new LocalInputDevice();

				// Logger.Push("Character Selection", "Player " + i, Characters[i].GetType().Name);

				var i1 = i;

				Characters[i].TravelObservable.Subscribe(d =>
				{
					Environment.PlayerDatas[i1][Environment.DataCategory.Distance] += d;
				});
				Characters[i].HitObservable.Subscribe(_ =>
				{
					// Logger.Push("Hit", "Player " + i1);
					Environment.PlayerDatas[i1][Environment.DataCategory.Hits]++;
				});
				Characters[i].HittedObservable.Subscribe(_ =>
				{
					// Logger.Push("Hitted", "Player " + i1);
					Environment.PlayerDatas[i1][Environment.DataCategory.Damages]++;
				});


				// Zone Stetting
				Zones[i].PlayerNo = i;
				Zones[i].transform.position = ZonePositions[shuffleNumbers[i]].position;
				SenseZones[i].transform.position = ZonePositions[shuffleNumbers[i]].position;
				Zones[i].OnRepeledObservable
					.Where(_ => Zones[i1].NumGold == Zones[i1].MaxGolds - 1)
					.Subscribe(character =>
					{
						var repelingPlayer = character.PlayerNo;
						var repeledPlayer = i1;

						Debug.Log("Repeled By " + repelingPlayer + " From " + repeledPlayer);

						if (repelingPlayer != repeledPlayer)
						{
							// Logger.Push("Block", "Player " + repelingPlayer, "From Player " + repeledPlayer,
							// 	"Score " + GameWatcher.Instance.CurrentCombo[repeledPlayer]);

							Environment.PlayerDatas[repelingPlayer][Environment.DataCategory.Defenses] += GameWatcher.Instance.CurrentCombo[repeledPlayer];

							var nice = Instantiate(Resources.Load<GameObject>("Prefabs/Game/Nice"));
							nice.transform.position = Characters[repelingPlayer].transform.position + Vector3.up;
							Observable.Timer(TimeSpan.FromSeconds(0.5)).Subscribe(_ =>
							{
								Destroy(nice);
							});
						}
					});

				// Percentage Setting
				Percentages[i].PositionNo = shuffleNumbers[i];
				Percentages[i].transform.position = PercentagePositions[shuffleNumbers[i]].position;
			}

			GameWatcher.Instance.OnFinishObservable.Subscribe(_ => Finish());
			GameWatcher.Instance.OnFinishObservable.SelectMany(_ => this.UpdateAsObservable())
				.Subscribe(_ =>
				{
					for (int i = 0; i < 4; i++)
					{
						Characters[i].IsControllable = false;
					}
				});
		}

		public void Go()
		{
			foreach (var character in Characters)
			{
				character.IsControllable = true;
			}
		}

		public void Finish()
		{
			foreach (var character in Characters)
			{
				character.IsControllable = false;
				character.TargetVelocity = Vector3.zero;
			}
		}
	}
}