﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace ProjectGold.Game
{
	public class PercentageBehaviour : MonoBehaviour
	{
		public int TeamNo;

		private int _positionNo;
		public int PositionNo
		{
			get { return _positionNo; }
			set
			{
				_positionNo = value;
				if (_positionNo == 2 || _positionNo == 3)
				{
					_starsTransform.localPosition = Vector3.forward * 6;
				}
				else
				{
					_starsTransform.localPosition = Vector3.forward * -6;
				}
			}
		}

		[SerializeField] private TextMeshPro _percentageText;
		[SerializeField] private Renderer _backRenderer;

		[SerializeField] private PlayerColors _playerColors;

		[SerializeField] private Transform _starsTransform;
		[SerializeField] private GameObject _starPrefab;

		public AnimationCurve WobblingCurve;

		void Start()
		{
			var starsNum = Environment.Game.Scores[TeamNo];

			for (int i = 0; i < starsNum; i++)
			{
				var go = Instantiate(_starPrefab, _starsTransform, false);
				go.transform.localPosition = Vector3.right * 3 * i + Vector3.left * 1.5f * (starsNum - 1);
			}
		}

		void Update()
		{
			_backRenderer.material.color = _playerColors.Colors[TeamNo];
			_percentageText.text = (Mathf.Floor(GameWatcher.Instance.Scores[TeamNo] * 100f)).ToString("##0");

			var zone = GameWatcher.Instance.Zones[TeamNo];
			var delta = zone.NumGold - zone.MaxGolds;
			if (delta >= 0)
			{
				transform.localScale = Vector3.one * WobblingCurve.Evaluate((Time.time * (delta + 1) * 2) % 1);
				_backRenderer.material.color = Color.Lerp(_playerColors.Colors[TeamNo], Color.white, (1 + Mathf.Sin(2 * Mathf.PI * (2 * Time.time * (delta + 1)))) / 2 / 3);
			}
			else
			{
				transform.localScale = Vector3.one;
				_backRenderer.material.color = _playerColors.Colors[TeamNo];
			}
		}
	}
}