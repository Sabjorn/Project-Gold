﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProjectGold.Utilities;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using TMPro;

namespace ProjectGold.Game
{
	public class ResultBehaviour : SingletonMonoBehaviour<ResultBehaviour>
	{
		public GameObject ResultEntityPrefab;
		public RectTransform PlayersTransform;
        [SerializeField]
        private TextMeshProUGUI okLabel;
        [SerializeField]
        private Text winPlayerLabel;
        [SerializeField]
        private PlayerColors _playerColors; 
        [SerializeField]
        private AudioClip _applauseClip;

        private AudioSource _audioSource;

        private bool _nextFlag = false;

        private void Start()
        {
            okLabel.enabled = false;
            winPlayerLabel.enabled = false;
			_audioSource = GetComponent<AudioSource>();
        }

        void Update()
		{
			if (_nextFlag)
			{
				if (MappedInput.InputDevices.Any(pad => pad.GetButtonDown(MappedButton.Decide)))
				{
					GameWatcher.Instance.NextScene();
				}
			}
		}

		public void Show(int? winPlayer)
		{
			Observable.Defer(() =>
			{
				for (int i = 0; i < 4; i++)
				{
					var e = Instantiate(ResultEntityPrefab, PlayersTransform, false);
					var c = e.GetComponent<ResultEntityBehaviour>();
					c.PlayerNo = i;
					c.SetImmediate(Environment.Game.Scores[i]);

					if (i == winPlayer) c.SetWinner();
				}

				var canvasGroup = GetComponent<CanvasGroup>();
				Observable.Range(1, 20)
					.Select(v => v / 20f)
					.Zip(this.UpdateAsObservable(), (a, b) => a)
					.Subscribe(v =>
					{
						canvasGroup.alpha = v;
					});

				return Observable.Timer(TimeSpan.FromSeconds(1.5f));
			})
			.Subscribe(_ =>
				{
					_nextFlag = true;
                    okLabel.enabled = true;
                    if(Environment.Game.Scores.Any(score => score >= Environment.Game.ScoreThreshold))
                    {
                        winPlayerLabel.text = String.Format("{0}P WIN!!", winPlayer.Value + 1);
                        winPlayerLabel.GetComponent<Outline>().effectColor = _playerColors.Colors[winPlayer.Value];
                        winPlayerLabel.enabled = true;
                        _audioSource.PlayOneShot(_applauseClip);
                    }
				});
		}
	}
}