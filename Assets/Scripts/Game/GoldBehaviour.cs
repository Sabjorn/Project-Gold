﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectGold.Character;
using UniRx;
using UnityEngine;

namespace ProjectGold
{
	public class GoldBehaviour : GrabbableBehaviour
	{
		/// <summary>
		/// 摩擦係数。
		/// </summary>
		public float Friction;

		private Rigidbody _rigidBody;

		public ZoneBehaviour nowZone;
        public AISenseZoneBehaviour nowSenseZone;

		protected new void Awake()
		{
			base.Awake();

			_rigidBody = GetComponent<Rigidbody>();
            nowZone = null;
		}
		protected void FixedUpdate ()
		{
			
			var v = -_rigidBody.velocity * Friction;
			v.y = -100.0f;
			_rigidBody.AddForce (v, ForceMode.Acceleration);
			if (transform.position.y <= 1.0f) {
				transform.position = new Vector3(transform.position.x, 1.0f, transform.position.z);
				_rigidBody.velocity = new Vector3(_rigidBody.velocity.x, 0.0f, _rigidBody.velocity.z);
				_rigidBody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
			}
		}
	}
}