﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectGold;
using ProjectGold.Character;
using UniRx;
using UniRx.Triggers;

namespace ProjectGold{

	public class FakeGoldBehaviour : GoldBehaviour {

		[SerializeField]
		private ParticleSystem SmokePrefab;
		[SerializeField]
		private AudioClip _smokeClip;

		private AudioSource AudioSource;

		// Use this for initialization
		void Awake () {
			base.Awake();
			AudioSource = GetComponent<AudioSource>();
			this.ObserveEveryValueChanged(g => g.grabPlayer)
				.DistinctUntilChanged()
				.Subscribe(p => 
				{
					if(p != null){
						Observable.Timer(TimeSpan.FromSeconds(1.0f))
						.Subscribe(_ => 
						{
							Vanish(p);
						}).AddTo(this.gameObject);
					}
				});
		}

		// 煙を上げて消える処理
		public void Vanish (CharacterBaseBehaviour character = null)
		{
			var smoke = Utilities.Particle.InstantiateParticle(SmokePrefab);
			smoke.transform.position = transform.position;
			smoke.Play();
			AudioSource.PlayOneShot(_smokeClip);
			Observable.Timer(TimeSpan.FromSeconds(2.5)).Subscribe(__ => Destroy(smoke));


			if(character != null) character.Release();
			Destroy(this.gameObject);
		}

	}
}