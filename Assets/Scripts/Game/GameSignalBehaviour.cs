﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectGold.Utilities;
using UnityEngine;
using UnityEngine.Events;
using UniRx;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace ProjectGold.Game
{
	public class GameSignalBehaviour : SingletonMonoBehaviour<GameSignalBehaviour>
	{
		public UnityEvent OnGo;

		private Animator _animator;

		[SerializeField]
		private Text _winText,_goText;

		[SerializeField] private AudioClip _startClip;
		[SerializeField] private AudioClip _finishClip;
		[SerializeField] private AudioClip _applauseClip;

		[SerializeField] private PlayerColors _playerColors;

		private AudioSource _audioSource;

		private int? _winTeam = 0;

		void Awake()
		{
			_animator = GetComponent<Animator>();
			_audioSource = GetComponent<AudioSource>();
		}

		void Start()
		{
			GameWatcher.Instance.OnFinishObservable.Subscribe(winTeam =>
			{
				_animator.SetTrigger("Finish");

				_winTeam = winTeam;
				if (winTeam.HasValue)
				{
					_winText.text = string.Format("{0}P WIN!", winTeam.Value + 1);
					_winText.GetComponent<Outline>().effectColor = _playerColors.Colors[winTeam.Value];
				}
				else
				{
					_winText.text = "DRAW...";
					_winText.GetComponent<Outline>().effectColor = Color.gray;
				}

				_audioSource.PlayOneShot(_finishClip);
				_audioSource.PlayOneShot(_applauseClip);
			});
		}

		public void SetText(string text)
		{
			_goText.text = text;
		}

		public void Go()
		{
			_audioSource.PlayOneShot(_startClip);
			OnGo.Invoke();
		}

		public void Play(AudioClip clip)
		{
			_audioSource.PlayOneShot(clip);
		}

		public void ShowResult()
		{
			ResultBehaviour.Instance.Show(_winTeam);
		}
	}
}