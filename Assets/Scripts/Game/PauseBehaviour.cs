﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProjectGold.UI;
using ProjectGold.Character;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace ProjectGold.Game
{
	public class PauseBehaviour : MonoBehaviour
	{
		public UISelector Selector;

		public UnityEvent OnOpen, OnClose;

		private CanvasGroup _canvasGroup;

		private bool _isOpen;

		private InputDevice enablePad;


		void Start()
		{
			_canvasGroup = GetComponent<CanvasGroup>();
			enablePad = null;
			_isOpen = false;

			Selector.enabled = false;

			Selector.SelectionObservable.Subscribe(selection =>
			{
				if (selection == 0)
				{
					Close();
				}
				else if (selection == 1)
				{
					Close();
					SceneManager.LoadScene("Character_Select");
				}
			});
		}

		void Update ()
		{
			if (!_isOpen && GameWatcher.Instance.IsRunning) {
				var pad = MappedInput.InputDevices.Find (p => p.GetButtonDown (MappedButton.Start));
				if (pad != null) {
					Open(pad);
				}
			}
			else if (_isOpen) {
				if (enablePad != null && enablePad.GetButtonDown (MappedButton.Start)) {
					Close();
				}
			}
		}

		void Open (InputDevice pad)
		{
			OnOpen.Invoke ();

			Time.timeScale = 0;
			_isOpen = true;
			Selector.enabled = true;
			Selector.SetEnablePad (enablePad);
			enablePad = pad;

			foreach (var c in GameObject.FindObjectsOfType<CharacterBaseBehaviour>()) {
				c.IsControllable = false;
			}

			Observable.Range (1, 10).Select (v => v / 10f)
			.Zip (this.UpdateAsObservable (), (a, b) => a)
			.Subscribe (v => {
				_canvasGroup.alpha = v;
			}, () => {
				_canvasGroup.alpha = 1;
				_canvasGroup.blocksRaycasts = true;
			});
		}

		void Close(){
			OnClose.Invoke();

			Time.timeScale = 1;
			_isOpen = false;
			//enablePad = null;

			Selector.enabled = false;

			foreach (var c in GameObject.FindObjectsOfType<CharacterBaseBehaviour>()) {
				c.IsControllable = true;
			}

			Observable.Range(1, 10).Select(v => v / 10f)
				.Zip(this.UpdateAsObservable(), (a, b) => a)
				.Subscribe(v =>
				{
					_canvasGroup.alpha = 1 - v;
				}, () =>
				{
					_canvasGroup.alpha = 0;
					_canvasGroup.blocksRaycasts = false;
				});
		}
	}
}