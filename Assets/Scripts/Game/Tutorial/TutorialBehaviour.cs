﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using ProjectGold;
using ProjectGold.Character;
using ProjectGold.Utilities;

namespace ProjectGold.Game
{

	public class TutorialBehaviour : SingletonMonoBehaviour<TutorialBehaviour> {

		[SerializeField]
		private CharacterBaseBehaviour character;
		[SerializeField]
		private GameObject tutorialWindow;
		[SerializeField]
		private TextMeshProUGUI mainMassage;
		[SerializeField]
		private TextMeshProUGUI nextButton;

		public float messageSkipSpeed = 1.0f;

		[SerializeField]
		private GameObject arrowPrefab;
		private GameObject arrowObject;

		[Header("TASK0 Move")]
		[SerializeField]
		private CapsuleCollider zone3;
		[Header("TASK1 Coin")]
		[SerializeField]
		private ZoneBehaviour zone0;
		private GameObject[] golds;
		[Header("TASK6 DashHit")]
		[SerializeField]
		private GameObject enemy1;
		[Header("TASK7 RedGold")]
		[SerializeField]
		private GameObject RedGoldPrefab;
		private GameObject redCoin;


		private TextLists messages;

		private bool isNextMessage;

		int taskNumber;

		void Awake ()
		{
			// キャラクター初期化
			character.Controller = new LocalInputDevice (MappedInput.InputDevices [0]);
			character.Look (new Vector3 (0.0f, 0.0f, -1.0f));
			character.IsControllable = false;

			enemy1.SetActive(false);

			// 矢印初期化
			arrowObject = Instantiate (arrowPrefab);
			arrowObject.SetActive (false);

			// コイン初期化
			golds = GameObject.FindGameObjectsWithTag ("Gold");
			foreach (var gold in golds) {
				gold.SetActive(false);
			}

			// メッセージ初期化
			messages = Resources.Load<TextLists>("ScriptableObjects/TutorialMessages");
			
			int messageCount = 0;
			taskNumber = 0;
			List<IObservable<Unit>> tutorialSequences = new List<IObservable<Unit>>();

			mainMassage.SetText(messages.Texts[0]);
			isNextMessage = false;

			nextButton.enabled = false;
			var buttonSequence = Observable.Timer(TimeSpan.FromSeconds(messageSkipSpeed));
			buttonSequence.Subscribe(x => nextButton.enabled = true).AddTo(this);

			// チュートリアルシーケンス
			var tutorialMessageSequence = this.UpdateAsObservable()
				.Where(_ => isNextMessage && nextButton.enabled) // メッセージ送り
				.Select(_ => messageCount + 1)
				.Where(i => i <= messages.Texts.Length)
				.Subscribe(i => 
				{
					if(i == messages.Texts.Length){
						SceneManager.LoadScene("title");
						return;
					}
					messageCount = i;
					isNextMessage = false;
					nextButton.enabled = false;
					// TASK
					if(messages.Texts[messageCount].Contains("[TASK]")){ // TASKだったら
						Observable.Timer(TimeSpan.FromSeconds(0.02f)).Subscribe(_ => { character.IsControllable = true; });
						TaskSet(taskNumber);
						mainMassage.SetText(messages.Texts[messageCount].Remove(0,7));

						tutorialSequences[taskNumber].First().Subscribe(_ => // TASK完了したら
						{
							TaskClean(taskNumber);
							character.IsControllable = false;
							taskNumber++;
							messageCount++;
							mainMassage.SetText(messages.Texts[messageCount]);
							// ボタン表示用
							buttonSequence.Subscribe(x => nextButton.enabled = true).AddTo(this);
						});
						return;
					}

					mainMassage.SetText(messages.Texts[messageCount]);
					// ボタン表示用
					buttonSequence.Subscribe(x => nextButton.enabled = true).AddTo(this);
				});

			// Task完了条件シーケンス登録
			// Task 0 Move
			tutorialSequences.Add( 
				zone3.OnTriggerEnterAsObservable()
				.Where(col => col.gameObject == character.gameObject)
				.AsUnitObservable()
			);
			// Task 1 Coin
			tutorialSequences.Add(
				this.UpdateAsObservable()
				.Where(_ => zone0.NumGold >= 1)
			);
			// Task 2 3Coins
			tutorialSequences.Add(
				this.UpdateAsObservable()
				.Where(_ => zone0.NumGold >= 3)
			);
			// Task 3 7Coins Demo
			tutorialSequences.Add(
				Observable.Timer(TimeSpan.FromSeconds(4.0f))
				.AsUnitObservable()
			);
			// Task 4 Skill1 Dash
			tutorialSequences.Add(
				this.UpdateAsObservable()
				.Where(_ => character.CooldownSessions[0].IsCool == false)
				.SkipUntil(Observable.Timer(TimeSpan.FromSeconds(4.0f)))
			);
			// Task 5 Dash Hit
			tutorialSequences.Add(
				enemy1.UpdateAsObservable()
				.Where(e => enemy1.GetComponent<CharacterBaseBehaviour>().HasEffect("Stun") && character.CooldownSessions[0].IsCool == false)
			);
			// Task 6 Hammer Hit
			tutorialSequences.Add(
				enemy1.UpdateAsObservable()
				.Where(e => enemy1.GetComponent<CharacterBaseBehaviour>().HasEffect("Stun") && character.CooldownSessions[1].IsCool == false)
			);
			// Task 7 Red Coin
			tutorialSequences.Add(
				this.UpdateAsObservable()
				.Where(_ => redCoin == null)
			);

		}

		// 各Task用の準備
		void TaskSet (int number)
		{
			switch (number) {
			case 0:
				arrowObject.SetActive (true);
				arrowObject.transform.position = new Vector3 (20.0f, 0.1f, -15.0f);
				break;
			case 1:
				golds [0].SetActive (true);
				arrowObject.SetActive (true);
				arrowObject.GetComponent<SpriteRenderer> ().flipY = true;
				arrowObject.transform.position = new Vector3 (-20.0f, 0.1f, 15.0f);
				break;
			case 2:
				golds [1].SetActive (true);
				golds [2].SetActive (true);
				arrowObject.SetActive (true);
				arrowObject.GetComponent<SpriteRenderer> ().flipY = true;
				arrowObject.transform.position = new Vector3 (-20.0f, 0.1f, 15.0f);
				GameWatcher.Instance.StartTimer ();
				break;
			case 3:
				foreach (var gold in golds) {
					gold.SetActive(true);
				}
				character.IsControllable = false;
				break;
			case 4:
				character.CooldownSessions[0].RemainingTime = 0.0f;
				break;
			case 5:
				character.CooldownSessions[0].RemainingTime = 0.0f;
				enemy1.SetActive(true);
				break;
			case 6:
				character.CooldownSessions[1].RemainingTime = 0.0f;
				break;
			case 7:
				redCoin = Instantiate(RedGoldPrefab);
				redCoin.transform.position = new Vector3(0.0f, 1.0f, 7.0f);
				break;
			default:
				break;
			}
		}
		// 各タスク終わりの後片付け
		void TaskClean (int number)
		{
			switch (number) {
			case 2:
				Observable.Timer(TimeSpan.FromSeconds(5.0f))
					.Subscribe(_ => 
					{
						GameWatcher.Instance.PauseTimer();
						golds[2].transform.position += new Vector3(15.0f, 0.0f, 0.0f);
					});
				break;
			case 3:
				foreach (var gold in golds) {
					gold.SetActive(false);
				}
				character.IsControllable = false;
				character.CooldownSessions[0].RemainingTime = 0.0f;
				break;
			case 4:
			case 5:
				character.CooldownSessions[1].RemainingTime = 0.0f;
				break;
			default:
				break; 
			}
			character.Stop();
			arrowObject.SetActive(false);
		}

		// 各タスク中の処理
		void TaskUpdate (int number)
		{
			switch (number) {
			case 0:
			case 1:
			case 2:
				character.CooldownSessions [0].Heat ();
				character.CooldownSessions [1].Heat ();
				break;
			case 3:
				character.CooldownSessions [0].Heat ();
				character.CooldownSessions [1].Heat ();
				character.IsControllable = false;
				break;
			case 4:
			case 5:
				character.CooldownSessions[1].Heat();
				break;
			case 6:
				character.CooldownSessions[0].Heat();
				break;
			}
		}

		void Update ()
		{
			if (MappedInput.InputDevices [0].GetButtonDown (MappedButton.Decide)) {
				isNextMessage = true;
			} else {
				isNextMessage = false;
			}
			TaskUpdate(taskNumber);
		}
	}
}