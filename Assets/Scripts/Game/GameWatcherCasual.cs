﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProjectGold.Utilities;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace ProjectGold.Game
{
	public class GameWatcherCasual : GameWatcher
	{
		[Header("Casual Mode Setting")]
		private float[] coinHoldingTime = {0.0f, 0.0f, 0.0f, 0.0f};
		public float coinHoldThrethold = 2.5f;
		public GameObject coinPrefab;
		public GameObject coinsParent;
		public ParticleSystem coinCompleteParticle;

		public UnityEvent OnScore;

		void Awake ()
		{
			_finishSubject = new Subject<int?> ();

			_isRunning = false;
			_timeRemaining = GameSeconds;

			_scores = new[] { 0f, 0f, 0f, 0f, 0f, 0f };


			_hasScored = false;

			//redGoldTimings.Add(2.0f);
			if (UseRedGold) {
				int redGoldMax = UnityEngine.Random.Range (1, 4);
				float lastTiming = 0.0f;
				for (int i = 0; i < redGoldMax; i++) {
					lastTiming += (GameSeconds / (redGoldMax + 1)) + UnityEngine.Random.Range (-15.0f, 15.0f);
					redGoldTimings.Add (lastTiming);
				}
			}
		}

		void Update ()
		{
			if (_isRunning) {
				int player = 0;
				foreach (var zone in Zones) {
					if (zone.NumGold >= zone.MaxGolds) {
						coinHoldingTime [player] += Time.deltaTime;
						if (_scores [player] > 1)
							_scores [player] = 1;
					} else {
						if (isScoring [player]) {
							BreakCombo (player);

							isScoring [player] = false;
						}

						coinHoldingTime [player] = 0.0f;
					}
					if (_scores [player] >= 1) {
						BreakCombo (player);

						Finish (player);

						break;
					}

					// Coin Hold Complete
					if (coinHoldingTime [player] >= coinHoldThrethold) {
						coinHoldingTime [player] = 0.0f;
						ScoreUp (0.1f, player);
						OnScore.Invoke();

						var coins = GameObject.FindGameObjectsWithTag ("Gold");
						int count = 0;
						foreach (var coin in coins) {
							var coinBehaviour = coin.GetComponent<GoldBehaviour>();
							if ( coinBehaviour.GetType() == typeof(GoldBehaviour) && coinBehaviour.nowZone == zone) {
								Destroy (coin);
								var newCoin = Instantiate (coinPrefab);
								newCoin.transform.position = new Vector3 (UnityEngine.Random.Range(-8.0f, 8.0f), 200.0f, UnityEngine.Random.Range(-3.0f, 3.0f));
								newCoin.transform.parent = coinsParent.transform;
								count++;
								var particle = Utilities.Particle.InstantiateParticle (coinCompleteParticle, coin.transform.position);
							}
							if (count == zone.MaxGolds)
								break;
						}
					}

					if (isScoring [player] && !_hasScored) {
						_hasScored = true;

						// スピードスターの集計
						// Logger.Push ("Speed Star", "Player " + player);
						Environment.PlayerDatas [player] [Environment.DataCategory.SpeedStar]++;
					}

					player++;
				}

				_timeRemaining -= Time.deltaTime;

				// RedGold
				if (UseRedGold) {
					if (redGoldTimings.Count > 0 && _timeRemaining < (GameSeconds - redGoldTimings.First ())) {
						var redGold = Instantiate (RedGoldPrefab);
						redGold.transform.position = new Vector3 (UnityEngine.Random.Range (-12.0f, 12.0f), 1.0f, UnityEngine.Random.Range (-12.0f, 12.0f));
						redGoldTimings.RemoveAt (0);

						OnRedCoin.Invoke();
					}
				}

				if (_timeRemaining < 0) {
	
					_timeRemaining = 0;

					int topTeam = 0;
					for (int t = 0; t < Zones.Length; t++) {
						if (_scores [topTeam] < _scores [t])
							topTeam = t;
					}

					// トップでないチームがスコアを伸ばしていたら延長
					bool isExtending = false;
					for (int t = 0; t < Zones.Length; t++) {
						if (isScoring [t] && topTeam != t) {
							isExtending = true;
						}
					}
					int maxScore = 0;
					for (int t = 0; t < Zones.Length; t++) {
						if (maxScore < Mathf.FloorToInt (_scores [t] * 100f)) {
							maxScore = Mathf.FloorToInt (_scores [t] * 100f);
						}
					}
					int maxTeams = 0;
					for (int t = 0; t < Zones.Length; t++) {
						if (maxScore == Mathf.FloorToInt (_scores [t] * 100f)) {
							maxTeams++;
						}
					}
					if(maxTeams >= 2)isExtending = true;


					if (!isExtending)
					{
						Finish(topTeam);
					}
				}
			}
		}



		override public void NextScene()
		{
			Environment.Game.Round++;

			if (Environment.Game.Scores.Any(s => s >= Environment.Game.ScoreThreshold))
			{
				SceneManager.LoadScene("Character_Select");
			}
			else
			{
				SceneManager.LoadScene("death_4_casual");
			}
		}
	}
}