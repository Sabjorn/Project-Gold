﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProjectGold.Utilities;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace ProjectGold.Game
{
	public class GameWatcher : SingletonMonoBehaviour<GameWatcher>
	{
		public ZoneBehaviour[] Zones;

		protected float[] _scores;
		public float[] Scores { get { return _scores; } }

		protected bool[] isScoring = new[] { false, false, false, false, false, false };
		protected float[] _scoreCues = new float[4];

		protected float[] _currentCombo = new[] { 0f, 0, 0, 0, 0, 0 };
		public float[] CurrentCombo { get { return _currentCombo; } }

		public float GameSeconds;

		protected Subject<int?> _finishSubject;
		public IObservable<int?> OnFinishObservable { get { return _finishSubject.First(); } }

		protected float _timeRemaining;
		public float TimeRemaining { get { return _timeRemaining; } }

		protected bool _isRunning;
		public bool IsRunning { get { return _isRunning; } }

		protected bool _hasScored;

		public bool UseRedGold = true;
		[SerializeField]
		protected GameObject RedGoldPrefab;
		protected List<float> redGoldTimings = new List<float>();

		public UnityEvent OnRedCoin;

		void Awake ()
		{
			_finishSubject = new Subject<int?> ();

			_isRunning = false;
			_timeRemaining = GameSeconds;

			_scores = new[] { 0f, 0f, 0f, 0f, 0f, 0f };


			_hasScored = false;

			//redGoldTimings.Add(2.0f);
			if (UseRedGold) {
				int redGoldMax = UnityEngine.Random.Range (1, 4);
				float lastTiming = 0.0f;
				for (int i = 0; i < redGoldMax; i++) {
					lastTiming += (GameSeconds / (redGoldMax + 1)) + UnityEngine.Random.Range (-15.0f, 15.0f);
					redGoldTimings.Add (lastTiming);
				}
			}
		}

		void Update ()
		{
			if (_isRunning) {
				int player = 0;
				foreach (var zone in Zones) {
					if (zone.NumGold >= zone.MaxGolds) {
						
						if (!isScoring [player]) {
							_scoreCues [player] = _scores [player];
							isScoring [player] = true;
						}

						_scores [player] += (zone.NumGold - zone.MaxGolds + 1) * Time.deltaTime / 20f;
						
						if (_scores [player] > 1)
							_scores [player] = 1;
					} else {
						if (isScoring [player]) {
							BreakCombo (player);

							isScoring [player] = false;
						}

					}
					if (_scores [player] >= 1) {
						BreakCombo (player);

						Finish (player);

						break;
					}


					if (isScoring [player] && !_hasScored) {
						_hasScored = true;

						// スピードスターの集計
						// Logger.Push ("Speed Star", "Player " + player);
						Environment.PlayerDatas [player] [Environment.DataCategory.SpeedStar]++;
					}

					player++;
				}

				_timeRemaining -= Time.deltaTime;

				// RedGold
				if (UseRedGold) {
					if (redGoldTimings.Count > 0 && _timeRemaining < (GameSeconds - redGoldTimings.First ())) {
						var redGold = Instantiate (RedGoldPrefab);
						redGold.transform.position = new Vector3 (UnityEngine.Random.Range (-12.0f, 12.0f), 1.0f, UnityEngine.Random.Range (-12.0f, 12.0f));
						redGoldTimings.RemoveAt (0);

						OnRedCoin.Invoke();
					}
				}

				if (_timeRemaining < 0)
				{
	
					_timeRemaining = 0;

					int topTeam = 0;
					for (int t = 0; t < Zones.Length; t++)
					{
						if (_scores[topTeam] < _scores[t])
							topTeam = t;
					}

					// トップでないチームがスコアを伸ばしていたら延長
					bool isExtending = false;
					for (int t = 0; t < Zones.Length; t++)
					{
						if (isScoring[t] && topTeam != t)
						{
							isExtending = true;
						}
					}

					if (!isExtending)
					{
						Finish(topTeam);
					}
				}
			}
		}

		protected void Finish(int? winPlayer)
		{
			_isRunning = false;

			// 勝利数集計
			if (winPlayer.HasValue)
			{
				Environment.Game.Scores[winPlayer.Value]++;

				// Logger.Push("Win", "Player " + winPlayer, "Remaining " + _timeRemaining.ToString());
				Environment.PlayerDatas[winPlayer.Value][Environment.DataCategory.Wins]++;
			}
			for (int i = 0; i < 4; i++)
			{
				// Logger.Push("Score", "Player " + i, Scores[i].ToString());
				// Logger.Push("Traveled Distance", "Player " + i, Environment.PlayerDatas[i][Environment.DataCategory.Distance].ToString());
				Environment.PlayerDatas[i][Environment.DataCategory.Scores] += Scores[i];
			}

			// デバッグ用
			for (int i = 0; i < 4; i++)
				Debug.Log(Environment.PlayerDatas[i].ToDebugString());

			_finishSubject.OnNext(winPlayer);
		}

		protected void BreakCombo(int team)
		{
			_currentCombo[team] = _scores[team] - _scoreCues[team];

			// Logger.Push("Combo Break", "Player " + team, "Combo " + _currentCombo[team]);

			var maxCombo = Environment.PlayerDatas[team][Environment.DataCategory.Combo];
			Environment.PlayerDatas[team][Environment.DataCategory.Combo] = Mathf.Max(maxCombo, _currentCombo[team]);
		}

		public void ScoreUp (float score, int playerNo)
		{
			_scores[playerNo] += score;
			if(_scores[playerNo] > 1)_scores[playerNo] = 1;
		}

		public void StartTimer()
		{
			_timeRemaining = GameSeconds;
			_isRunning = true;
		}

		public void PauseTimer()
		{
			_isRunning = false;
		}

		public void Resume()
		{
			_isRunning = true;
		}

		virtual public void NextScene()
		{
			Environment.Game.Round++;

			if (Environment.Game.Scores.Any(s => s >= Environment.Game.ScoreThreshold))
			{
				SceneManager.LoadScene("Character_Select");
			}
			else
			{
				SceneManager.LoadScene("death_4");
			}
		}
	}
}