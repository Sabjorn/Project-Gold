﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ProjectGold.Character;
using ProjectGold.Game;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace ProjectGold.Game
{
	public class OnlinePracticeWatcher : NetworkBehaviour
	{
		[SerializeField] private GameObject onlinePlayerPrefab;
		public float Timer;

		public UnityEvent OnReady;

		[SerializeField] private PracticePanelBehaviour[] _panels;
		[SerializeField] private CanvasGroup _standbyCanvasGroup;

		[SerializeField] private PlayerColors _playerColors;

		[SerializeField] private TextMeshProUGUI _timerText;

		private bool[] _oks = { false, false, false, false };

		private bool _isStandby;

		private CharacterBaseBehaviour[] _characters;

		void Start()
		{
			if (isServer)
				Observable.Interval(TimeSpan.FromMilliseconds(200f))
					.Where(_ =>
					{
						var connections = NetworkServer.connections;
						return connections.Select(c => c.isReady).Aggregate((a, e) => a && e);
					})
					.First()
					.Subscribe(_ =>
					{
						ServerOnlineReady();
					});

			float timer = Timer;
			this.UpdateAsObservable()
				.Do(_ =>
				{
					timer -= Time.deltaTime;
					if (timer < 0) timer = 0;

					var timespan = TimeSpan.FromSeconds(timer);
					_timerText.text = string.Format("{0:0}:{1:00}", timespan.Minutes, timespan.Seconds);
				}).TakeWhile(_ => timer > 0)
				.Subscribe(_ => { }, () =>
				{
					for (int i = 0; i < 4; i++)
					{
						_oks[i] = true;
						_panels[i].Ready();
					}
				});

			_oks = new bool[Environment.Game.CharacterSelections.Length];

			_isStandby = false;

			_standbyCanvasGroup.alpha = 0;
		}

		[Server]
		void ServerOnlineReady()
		{
			var list = new List<GameObject>();
			foreach (var networkConnection in NetworkServer.connections)
			{
				NetworkServer.DestroyPlayersForConnection(networkConnection);

				//var playerObject = Instantiate(onlinePlayerPrefab);

				//NetworkServer.AddPlayerForConnection(networkConnection, playerObject, 0);

				//list.Add(playerObject);
			}

			_characters = new CharacterBaseBehaviour[Environment.Game.CharacterSelections.Length];

			for (int i = 0; i < Environment.Game.CharacterSelections.Length; i++)
			{
				var charAsset = Environment.Game.CharacterSelections[i];
				charAsset = Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Hammer");   // DEBUG

				// キャラ生成・配置
				var charObject = Instantiate(charAsset.Prefab);
				charObject.transform.position = Vector3.Lerp(new Vector3(-10, 1, -10), new Vector3(10, 1, -10), (float)i / Environment.Game.CharacterSelections.Length);
				charObject.transform.rotation = Quaternion.identity;

				// キャラ初期化
				_characters[i] = charObject.GetComponent<CharacterBaseBehaviour>();
				_characters[i].PlayerNo = i;
				_characters[i].TeamNo = i;
				_characters[i].IsControllable = true;

				NetworkServer.Spawn(charObject);

				// パネル初期化
				// _panels[i].PlayerNo = i;
				// _panels[i].Character = charAsset;
			}

			// RpcClientReady(list.ToArray());
		}

		[ClientRpc]
		void RpcClientReady(GameObject[] playerObjects)
		{
			for (int i = 0; i < playerObjects.Length; i++)
			{
				var nCon = playerObjects[i].GetComponent<NetworkCharacterController>();

				nCon.TargetCharacter = _characters[i];
				_characters[i].Controller = nCon;
			}
		}

		void Update()
		{
			int i = 0;
			foreach (var pad in MappedInput.InputDevices)
			{
				if (pad.GetButtonDown(MappedButton.Start))
				{
					OnReady.Invoke();
					_oks[i] = true;
					_panels[i].Ready();
				}
				i++;
			}
			if (!_isStandby)
			{
				bool flag = true;
				foreach (var b in _oks)
				{
					flag &= b;
				}
				if (flag)
				{
					_isStandby = true;
					_standbyCanvasGroup.alpha = 1;
					Observable.Timer(TimeSpan.FromSeconds(3.0)).Subscribe(_ =>
					{
						switch (Environment.Game.GameModeSelection)
						{
							case Environment.Game.GameMode.DeathMatch:
								SceneManager.LoadScene("death_4");
								break;
							case Environment.Game.GameMode.TeamMatch:
								SceneManager.LoadScene("team_2v2");
								break;
						}
					});
				}
			}
		}
	}
}