﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectGold.Character;
using ProjectGold.Game;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace ProjectGold
{
	public class RedGoldBehaviour : GoldBehaviour
	{

		[SerializeField]
		private ParticleSystem spornParticlePrefab;
		[SerializeField]
		private ParticleSystem completeParticlePrefab;
		private SpriteRenderer renderer;
		[SerializeField]
		private AnimationCurve holdingCurve;

		[SerializeField] private AudioClip _completeClip;

		public float MaxHoldTime = 3.5f;
		private float holdingTime = 0.0f;

		private Vector3 originScale;

		void Awake()
		{
			base.Awake();
			renderer = GetComponent<SpriteRenderer>();
			originScale = transform.localScale;
		}

		void Start()
		{
			var particle = Utilities.Particle.InstantiateParticle(spornParticlePrefab, this.transform.position + new Vector3(0.0f, 1.0f, 0.0f));
			particle.transform.parent = transform;
		}



		void Update()
		{
			if (nowZone != null)
			{
				holdingTime += Time.deltaTime;
				//transform.localScale = originScale * holdingCurve.Evaluate ((Time.time * 2) % 1);
				transform.localScale = originScale * (1.0f + 1.0f * holdingTime / MaxHoldTime);
				//renderer.color = Color.Lerp (Color.red, Color.white, (1 + Mathf.Sin (2 * Mathf.PI * (2 * Time.time * (0 + 1)))) / 2 / 3);
			}
			else
			{
				//transform.localScale = originScale;
				renderer.color = Color.red;
				if (holdingTime > MaxHoldTime * 0.7f) holdingTime = MaxHoldTime * 0.7f;
				transform.localScale = originScale * (1.0f + 1.0f * holdingTime / MaxHoldTime);
				//holdingTime = 0.0f;
			}
			if (holdingTime >= MaxHoldTime)
			{
				HoldComplete();
			}
		}


		public void HoldComplete()
		{
			var particle = Utilities.Particle.InstantiateParticle(completeParticlePrefab, this.transform.position + new Vector3(0.0f, 1.0f, 0.0f));
			particle.gameObject.UpdateAsObservable()
				.Where(_ => particle.IsAlive() == false)
				.Subscribe(_ =>
				{
					GameWatcher.Instance.ScoreUp(0.3f, this.nowZone.PlayerNo);
				});

			var soundObject = new GameObject();
			var component = soundObject.AddComponent<AudioSource>();
			component.PlayOneShot(_completeClip);

			soundObject.UpdateAsObservable()
				.Where((_) => !component.isPlaying)
				.Take(1)
				.Subscribe(_ => { Destroy(soundObject); });

			Destroy(this.gameObject);
		}


	}
}