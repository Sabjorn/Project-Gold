﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectGold.Game
{
	public class TimerBehaviour : MonoBehaviour
	{
		private Text _timerText;
		[SerializeField] private Gradient _warningGradient;
		[SerializeField] private AnimationCurve _warningScaleCurve;

		void Awake()
		{
			_timerText = GetComponent<Text>();
		}

		void Update()
		{
			var timeSpan = TimeSpan.FromSeconds(GameWatcher.Instance.TimeRemaining);
			_timerText.text = string.Format("{0:00}:{1:00}.{2:0}", timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds / 100);

			if (timeSpan.TotalSeconds <= 15) {
				var t = (Time.time * 2f) % 1f;
				_timerText.color = _warningGradient.Evaluate (t);
				_timerText.rectTransform.localScale = Vector3.one * _warningScaleCurve.Evaluate (t);
			} 
			if (timeSpan.TotalSeconds == 0 && GameWatcher.Instance.IsRunning)
			{
				_timerText.text = "延長中！";
			}

		}
	}
}