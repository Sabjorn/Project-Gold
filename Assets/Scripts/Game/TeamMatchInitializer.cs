﻿using System.Collections;
using System.Collections.Generic;
using ProjectGold.Character;
using UnityEngine;
using UniRx;

namespace ProjectGold.Game
{
	public class TeamInitializer : MonoBehaviour
	{
		public Transform[] SpawnTransforms;
		public ZoneBehaviour[] Zones;

		private CharacterBaseBehaviour[] _characters;

		void Start()
		{
			_characters = new CharacterBaseBehaviour[Environment.Game.CharacterSelections.Length];

			for (int i = 0; i < Environment.Game.CharacterSelections.Length; i++)
			{
				var charAsset = Environment.Game.CharacterSelections[i];

				var charObject = Instantiate(charAsset.Prefab);
				charObject.transform.position = SpawnTransforms[i].position;
				charObject.transform.rotation = Quaternion.identity;

				_characters[i] = charObject.GetComponent<CharacterBaseBehaviour>();
				_characters[i].PlayerNo = i;
				_characters[i].TeamNo = i/2;
				_characters[i].IsControllable = false;
			}

			GameWatcher.Instance.OnFinishObservable.Subscribe(_ => Finish());
		}

		public void Go()
		{
			foreach (var character in _characters)
			{
				character.IsControllable = true;
			}
		}

		public void Finish()
		{
			foreach (var character in _characters)
			{
				character.IsControllable = false;
				character.TargetVelocity = Vector3.zero;
			}
		}
	}
}