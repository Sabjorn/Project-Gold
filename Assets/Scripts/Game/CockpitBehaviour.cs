﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectGold.Character;
using ProjectGold.Game;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace ProjectGold
{
	public class CockpitBehaviour : MonoBehaviour
	{
		public float Score;

		public CharacterBaseBehaviour TargetCharacter;
		public ZoneBehaviour TargetZone;

		[SerializeField] private Text ScoreText;
		[SerializeField] private Text GoldsText;

		[SerializeField]
		private Color ColorAvailable, ColorUnavailable;

		[SerializeField]
		private Image[] CooldownGauges;

		private bool _isFinished;

		void Start()
		{
			TargetZone.OnFilledObservable.Subscribe(_ =>
			{
				GoldsText.color = Color.yellow;
			});

			_isFinished = false;

			GameWatcher.Instance.OnFinishObservable.Subscribe(_ => { _isFinished = true; });
		}


		void Update()
		{
			int i = 0;
			foreach (var gauge in CooldownGauges)
			{
				if (i < TargetCharacter.CooldownSessions.Length)
				{
					var cooldown = TargetCharacter.CooldownSessions[i];
					gauge.fillAmount = 1 - cooldown.RemainingProgress;

					gauge.color = cooldown.IsCool ? ColorAvailable : ColorUnavailable;
				}
				i++;
			}

			if (TargetZone.NumGold >= TargetZone.MaxGolds)
			{
				if (!_isFinished)
					Score += (TargetZone.NumGold - TargetZone.MaxGolds + 1) * Time.deltaTime;
			}

			if (TargetZone.NumGold < TargetZone.MaxGolds) GoldsText.color = Color.white;

			GoldsText.text = String.Format("金塊: {0}", TargetZone.NumGold);
			ScoreText.text = String.Format("Score: {0:#0.0}", Score);
		}
	}
}