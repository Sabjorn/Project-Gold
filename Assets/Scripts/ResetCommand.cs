﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ProjectGold
{
	public class ResetCommand : MonoBehaviour
	{
		void Start()
		{
			DontDestroyOnLoad(gameObject);
		}

		void Update()
		{
			foreach (var pad in GamepadInput.Instance.Gamepads)
			{
				if (
					pad.GetButton(GamepadButton.LeftBumper)
					&& pad.GetButton(GamepadButton.RightBumper)
					&& pad.GetAxis(GamepadAxis.LeftTrigger) >= 0.5f
					&& pad.GetAxis(GamepadAxis.RightTrigger) >= 0.5f
					&& pad.GetButton(GamepadButton.Start)
					&& pad.GetButton(GamepadButton.Back)

					&& SceneManager.GetActiveScene().name != "title"
				)
				{
					SceneManager.LoadScene("title");
				}
			}

			if (Input.GetKey(KeyCode.E) && Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.C)
				&& SceneManager.GetActiveScene().name != "title")
			{
				SceneManager.LoadScene("title");
			}

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
		}
	}
}