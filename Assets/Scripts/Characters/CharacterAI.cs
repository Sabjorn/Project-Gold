﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using ProjectGold;
using ProjectGold.Character;
using BehaviourTree;
using System;

public class CharacterAI : MonoBehaviour, IControllerInput {

	protected bool isInitialized;

    protected CharacterBaseBehaviour behaviour;
    protected ThinkData thinkData;

    protected ZoneBehaviour myZone;

    [SerializeField]
    protected Transform AIPivotTransform;

    protected NodeBase rootNode;
    protected NodeBase commonNode;

    public AIInput Input;
    private bool isSearchUpdateTiming;

    // Controller
	public float GetAxis (MappedAxis axis)
	{
		switch (axis) {
		case MappedAxis.Horizontal:
			return Input.HorizontalAxis;
			break;
		case MappedAxis.Vertical:
			return Input.VerticalAxis;
			break;
		}
		return 0;
	}

	public bool GetButton(MappedButton button)
	{
		if (Input != null)
			return Input.Button[button];
		return false;
	}

	public bool GetButtonDown(MappedButton button)
	{
		if (Input != null)
			return Input.ButtonDown[button];
		return false;
	}

	public bool GetButtonUp(MappedButton button)
	{
		if (Input != null)
			return Input.ButtonUp[button];
		return false;
	}


	// Use this for initialization
	protected void Start ()
	{
		isInitialized = false;
		Input = new AIInput ();

		commonNode = new SequencerNode (new List<NodeBase> {
			new ActionNode (SearchCoin),
			new SelectorNode (new List<NodeBase> {
				new ActionNode (GrabCoin),
				new ActionNode (GoToCoin)
			})
		});

		// 陣地のコインの数を監視
		isSearchUpdateTiming = true;
		foreach (var zone in GameObject.FindObjectsOfType<AISenseZoneBehaviour>()) {
			zone.ObserveEveryValueChanged(z => {
				return z.NumGold + (z.hasRedGold ? 1 : 0);
			})
			.Subscribe(num =>
			{
				isSearchUpdateTiming = true;
				Debug.Log("coin keep changed at zone:" + zone.PlayerNo.ToString());
			});
		}


    }

	// Start()で初期化するとgameObject間のinitialize順が保障されないので一部を明示的に初期化
	protected void Initialize ()
	{
		thinkData = new ThinkData();
        myZone = GameObject.FindObjectsOfType<ZoneBehaviour>().ToList().Find(x => x.GetComponent<ZoneBehaviour>().PlayerNo == behaviour.PlayerNo );
        isInitialized = true;

	}
	
	// Update is called once per frame
	protected void Update ()
	{
		if (isInitialized == false) {
			Initialize ();
		}
		AIPivotTransform.rotation = Quaternion.Slerp (AIPivotTransform.rotation, Quaternion.LookRotation (behaviour.Direction, Vector3.up), 0.1f);

		if (behaviour.IsControllable) {
			rootNode.Execute ();
		}
		if (thinkData.targetCoin != null && thinkData.targetCoin.activeInHierarchy == false) {
			thinkData.targetCoin = null;
		}
		if (thinkData.redCoin != null && thinkData.redCoin.activeInHierarchy == false) {
			thinkData.redCoin = null;
		}
		Input.Update();
	}

    protected class ThinkData
    {
        public GameObject targetCoin;
        public GameObject redCoin;
        public GameObject grabCandidate;
    }

    public class AIInput
	{
		public float VerticalAxis;
		public float HorizontalAxis;
		public Dictionary<MappedButton, bool> Button;
		public Dictionary<MappedButton, bool> ButtonDown;
		public Dictionary<MappedButton, bool> ButtonUp;
		private Dictionary<MappedButton, bool> lastButton;
		private Dictionary<MappedButton, bool> nextButton;

		public AIInput()
		{
			Button = new Dictionary<MappedButton, bool>();
			ButtonDown = new Dictionary<MappedButton, bool>();
			ButtonUp = new Dictionary<MappedButton, bool>();
			lastButton = new Dictionary<MappedButton, bool>();
			nextButton = new Dictionary<MappedButton, bool>();
			foreach(MappedButton name in Enum.GetValues(typeof(MappedButton)))
			{
				Button.Add(name, false);
				ButtonDown.Add(name, false);
				ButtonUp.Add(name, false);
				lastButton.Add(name, false);
				nextButton.Add(name, false);
			}
		}

		public void SetAxis (Vector2 v)
		{
			v = v.normalized;
			HorizontalAxis = v.x;
			VerticalAxis = v.y;
		}

		public void SetButton (MappedButton button)
		{
			nextButton[button] = true;
		}

		public bool GetButtonStateLast (MappedButton button)
		{
			return lastButton[button];
		}

		public void Update ()
		{
			
			foreach (MappedButton name in Enum.GetValues(typeof(MappedButton))) {
				
				Button[name] = nextButton [name];

				if (lastButton [name] == false && Button [name]) {
					ButtonDown [name] = true;
				} else {
					ButtonDown [name] = false;
				}
				if (lastButton [name] && Button [name] == false) {
					ButtonUp [name] = true;
				} else {
					ButtonUp [name] = false;
				}

				lastButton[name] = Button[name];
				nextButton[name] = false;
			}
		}
	}


	// Actions
    protected bool SearchCoin ()
	{
        // =========掴めるコイン検出=========
        thinkData.grabCandidate = behaviour.GetGrabCandidate();

        //isSearchUpdateTiming = true;
		if(isSearchUpdateTiming == false) return true;
		Debug.Log("search Coin player:" + behaviour.PlayerNo.ToString());
		//=========狙うコインを決める==========
		thinkData.targetCoin = null;
		thinkData.redCoin = null;
		var coins = GameObject.FindGameObjectsWithTag ("Gold");

		// 近くのコイン検出
		float minCoinDistance = Mathf.Infinity;
		if (thinkData.targetCoin == null) {
			float min = Mathf.Infinity;
			foreach (var coin in coins) {
				if (behaviour.GetType () == typeof(JiggsBehaviour) && ((JiggsBehaviour)behaviour).myFakeCoin == coin) {
					continue;
				}
				var goldBehaviour = coin.GetComponent<GoldBehaviour> ();
				if (goldBehaviour.nowZone != null && goldBehaviour.nowZone.PlayerNo == behaviour.PlayerNo) {
					continue;
				}

				float distance = Vector3.Distance (coin.transform.position, behaviour.transform.position);

				// RedGold
				if (goldBehaviour.GetType () == typeof(RedGoldBehaviour)) {
					minCoinDistance = distance / 2.0f;
					thinkData.targetCoin = coin;
					thinkData.redCoin = coin;
					continue;
					//break;
				}

				if (goldBehaviour.grabPlayer != null) {
					continue;
				}

				if (distance < min) {
					min = distance;
					minCoinDistance = distance;
					// とりあえず近いものを候補に
					thinkData.targetCoin = coin;
				}
			}
		}

		// 多く集めてるプレーヤのコインを狙う
		var zones = GameObject.FindObjectsOfType<ZoneBehaviour> ().ToList ();
		var senseZones = GameObject.FindObjectsOfType<AISenseZoneBehaviour> ().ToList ();
		Dictionary<ZoneBehaviour, int> zoneCoinCounts = zones.ToDictionary (zone => zone, zone => 0);
		Dictionary<AISenseZoneBehaviour, int> senseZoneCoinCounts = senseZones.ToDictionary (zone => zone, zone => 0);

		foreach (var zone in zones) {
			zoneCoinCounts [zone] = zone.NumGold;
		}
		foreach (var zone in senseZones) {
			senseZoneCoinCounts [zone] = zone.NumGold;
		}
		List<ZoneBehaviour> targetZoneCandidate = new List<ZoneBehaviour>();
		List<AISenseZoneBehaviour> targetSenseZoneCandidate = new List<AISenseZoneBehaviour>();
		ZoneBehaviour targetZone = null;
		AISenseZoneBehaviour targetSenseZone = null;
		int max = zoneCoinCounts.Values.Max ();
		int senseMax = senseZoneCoinCounts.Values.Max ();

		// 3つ以上持っているやつを優先
		if (max >= 3) {
			float min = Mathf.Infinity;
			foreach (var zoneCoinCount in zoneCoinCounts) {
				if (zoneCoinCount.Value == max && zoneCoinCount.Key.PlayerNo != behaviour.PlayerNo) {
					//targetZoneCandidate.Add(zoneCoinCount.Key);
					float distance = Vector3.Distance (zoneCoinCount.Key.transform.position, behaviour.transform.position);
					if (distance < min ) {
						min = distance;
						//targetZone = zoneCoinCount.Key;
						targetZoneCandidate.Add(zoneCoinCount.Key);
					}
				}
			}
		}
		else if (senseMax >= 1) {
			// 1個以上所持している一番近い敵のエリアを探す
			float min = Mathf.Infinity;
			int maxCoin = 0;
			foreach (var zoneCoinCount in senseZoneCoinCounts) {
				if (zoneCoinCount.Value >= 1 && zoneCoinCount.Key.PlayerNo != behaviour.PlayerNo) {
					//targetZoneCandidate.Add(zoneCoinCount.Key.GetComponentInParent<ZoneBehaviour>());
					float distance = Vector3.Distance (zoneCoinCount.Key.transform.position, behaviour.transform.position);
					//if (distance < min || maxCoin < zoneCoinCount.Value) {
						min = distance;
						maxCoin = zoneCoinCount.Value;
						//targetSenseZone = zoneCoinCount.Key;
						targetSenseZoneCandidate.Add(zoneCoinCount.Key);
						//targetZone = zoneCoinCount.Key.GetComponentInParent<ZoneBehaviour>();
					//}
				}
			}
		}
		if(targetZoneCandidate.Count > 0) targetZone = targetZoneCandidate[UnityEngine.Random.Range(0, targetZoneCandidate.Count)];
		if(targetSenseZoneCandidate.Count > 0) targetSenseZone = targetSenseZoneCandidate[UnityEngine.Random.Range(0, targetSenseZoneCandidate.Count)];
		if (targetZone != null || targetSenseZone != null ) {
			float min = Mathf.Infinity;
			GameObject zoneCoinCandidate = null;
			foreach (var coin in coins) {
				if (behaviour.GetType () == typeof(JiggsBehaviour) && ((JiggsBehaviour)behaviour).myFakeCoin == coin) {
					continue;
				}
				if ( (targetZone != null && coin.GetComponent<GoldBehaviour> ().nowZone == targetZone)
				|| (targetSenseZone != null && coin.GetComponent<GoldBehaviour> ().nowSenseZone == targetSenseZone)) {
					if (thinkData.targetCoin == coin) {
						zoneCoinCandidate = null;
						break;
					}
					float distance = Vector3.Distance (coin.transform.position, behaviour.transform.position);
					if (distance < min /*|| max >= 3*/) {
						min = distance;
						zoneCoinCandidate = coin;
					}
				}
			}
			if (zoneCoinCandidate != null && (max >= 3 ? minCoinDistance * 4.0f > min : minCoinDistance * 2.0f > min * Mathf.Sqrt(2.0f))) { // それでも近いコインを優先
				thinkData.targetCoin = zoneCoinCandidate;
			}
		}



        isSearchUpdateTiming = false;
		return true;
	}

    protected bool GoToCoin ()
	{
		if (thinkData.targetCoin == null) {
			isSearchUpdateTiming = true;
			return false;
		}
        var v = thinkData.targetCoin.transform.position - behaviour.transform.position;
        Input.SetAxis(new Vector2(v.x, v.z));
        return true;
    }

	protected bool GrabCoin ()
	{
		if (thinkData.grabCandidate == null || behaviour.Grabbing != null)
			return false;
		if (thinkData.grabCandidate.GetComponent<GoldBehaviour> ().nowZone != null
		    && thinkData.grabCandidate.GetComponent<GoldBehaviour> ().nowZone.PlayerNo == behaviour.PlayerNo) {
		    return false;
		}
		if (thinkData.grabCandidate == thinkData.targetCoin) {
			Input.SetButton (MappedButton.Grab);
			return true;
		}
		var coin = thinkData.grabCandidate.GetComponent<GoldBehaviour>();
		if (thinkData.grabCandidate != null && (coin.nowZone == null || (coin.nowZone != null && coin.nowZone.PlayerNo != behaviour.PlayerNo))) {
			Input.SetButton (MappedButton.Grab);
			return true;
		}
		return false;
    }

	protected bool ReleaseCoin ()
	{
		if (behaviour.Grabbing != null) {
			Input.SetButton (MappedButton.Grab);
			return true;
		}
		return false;
    }

	protected bool BackHome()
    {
		var v = myZone.transform.position - behaviour.transform.position;
		Input.SetAxis(new Vector2(v.x, v.z));
        return true;
    }

    // Conditions
	protected bool IsGrabbingCoin()
    {
        if (behaviour.Grabbing == null) return false;
        return true;
    }

	protected bool IsOnHomeWithCoin ()
	{
		if (behaviour.Grabbing != null && behaviour.onMyZone) {
			return true;
		}
        return false;
    }

    protected bool IsRedCoinNear ()
	{
		if (thinkData.redCoin != null && behaviour.Grabbing != thinkData.redCoin) {
			if (Vector3.Distance (thinkData.redCoin.transform.position, this.transform.position) < 20.0f) {
				return true;
			}
		}
		return false;
    }
}
