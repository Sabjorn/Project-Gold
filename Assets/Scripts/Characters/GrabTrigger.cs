﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ProjectGold.Character
{
	public class GrabTrigger : MonoBehaviour
	{
		public GameObject GrabbingCandidate
		{
			get
			{
				if (_grabCandidates.Any(g => g != null && g.GetComponent<GrabbableBehaviour>().IsGrabbable && g.activeInHierarchy))
				{
					return _grabCandidates
						.Where(g => g != null && g.GetComponent<GrabbableBehaviour>().IsGrabbable && g.activeInHierarchy)
						.OrderBy(go => (transform.position - go.transform.position).magnitude)
						.First();
				}

				return null;
			}
		}

		private List<GameObject> _grabCandidates=new List<GameObject>();

		void Awake()
		{
			_grabCandidates = new List<GameObject>();
		}

		void Update ()
		{
			foreach (var g in _grabCandidates) {
				if(g == null || g.gameObject.activeInHierarchy == false)
					_grabCandidates.Remove(g);
					break;
			}
		}

		void OnTriggerEnter(Collider collider)
		{
			if (collider.GetComponent<GrabbableBehaviour>() != null)
			{
				_grabCandidates.Add(collider.gameObject);
			}
		}

		void OnTriggerExit(Collider collider)
		{
			if (collider.GetComponent<GrabbableBehaviour>() != null)
			{
				_grabCandidates.Remove(collider.gameObject);
			}
		}
	}
}