﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectGold.Character
{
	public class CooldownSession
	{
		public float MaxTime { get; set; }
		public float RemainingTime { get; set; }

		public bool IsCool { get { return RemainingTime <= 0; } }
		public float RemainingProgress { get { return RemainingTime / MaxTime; } }

		public CooldownSession(float maxTime)
		{
			MaxTime = maxTime;
			RemainingTime = 0;
		}

		public void Update()
		{
			RemainingTime -= Time.deltaTime;
			if (RemainingTime < 0) RemainingTime = 0;
		}

		public void Heat()
		{
			RemainingTime = MaxTime;
		}

		public void InstantHeat(float time){
			RemainingTime = time;
		}
	}
}