﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace BehaviourTree
{

    public abstract class NodeBase
    {
        public abstract bool Execute();
    }

    public class ActionNode : NodeBase
    {
        private Func<bool> action;

        public ActionNode(Func<bool> action)
        {
            this.action = action;
        }

        public override bool Execute()
        {
            return action();
        }
    }

    public class DecoratorNode : NodeBase
    {
        private NodeBase child;
        private Func<bool> conditionF;

        public DecoratorNode(Func<bool> conditionFunction, NodeBase child)
        {
            this.conditionF = conditionFunction;
            this.child = child;
        }

        public override bool Execute()
        {
            if (conditionF()) return child.Execute();
            else return false;
        }
    }

    public class SelectorNode : NodeBase
    {
        private List<NodeBase> children;

        public SelectorNode(List<NodeBase> children)
        {
            this.children = children;
        }

        public override bool Execute()
        {
            foreach(var child in children)
            {
                if (child.Execute()) return true;
            }
            return false;
        }
    }

    public class SequencerNode : NodeBase
    {
        private List<NodeBase> children;

        public SequencerNode(List<NodeBase> children)
        {
            this.children = children;
        }

        public override bool Execute()
        {
            foreach(var child in children)
            {
                if (!child.Execute()) return false;
            }
            return true;
        }
    }
}