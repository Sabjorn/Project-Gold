﻿using System.Collections;
using System.Collections.Generic;
using ProjectGold.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UniRx;

namespace ProjectGold.Title
{
	public class TitleBehaviour : MonoBehaviour
	{
		public Animator CanvasAnimator;
		public UISelector Selector;
		public UnityEvent OnDecide;

		void Start()
		{
			Environment.Initialize();

			Selector.SelectionObservable.Subscribe(i =>
			{
				switch (i)
				{
					case 0: // casual battle
						Environment.Game.GameModeSelection = Environment.Game.GameMode.DeathMatchCasual;
						SceneManager.LoadScene("Character_Select");
						break;
					case 1: // gachi battle
						Environment.Game.GameModeSelection = Environment.Game.GameMode.DeathMatch;
						SceneManager.LoadScene("Character_Select");
						break;
					/*
					case 1: // online lobby
						SceneManager.LoadScene("online_lobby");
						break;
					*/
					case 2: // practice
						Environment.Game.GameModeSelection = Environment.Game.GameMode.Practice;
						SceneManager.LoadScene("Character_Select");
						break;
					case 3: // tutorial
						Environment.Game.GameModeSelection = Environment.Game.GameMode.Tutorial;
						SceneManager.LoadScene("tutorial");
						break;
                    case 4: // Quit
                        Application.Quit();
                        break;
					default:
						break;
				}
			});

			Selector.enabled = false;
		}
		void Update()
		{
			if (CanvasAnimator.GetCurrentAnimatorStateInfo(0).IsName("First"))
			{
				foreach (var pad in MappedInput.InputDevices)
				{
					if (
						pad.GetButtonDown(MappedButton.Start)
						|| pad.GetButtonDown(MappedButton.Grab)
						|| pad.GetButtonDown(MappedButton.Skill1)
						|| pad.GetButtonDown(MappedButton.Skill2)
						|| pad.GetButtonDown(MappedButton.Skill3)
					)
					{
						OnDecide.Invoke();
						Selector.enabled = true;
						CanvasAnimator.SetTrigger("Next");
					}
				}
			}
		}
	}
}