﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UniRx;
using UnityEngine;

namespace ProjectGold.Title
{
	public class MenuSelector : MonoBehaviour
	{
		public IObservable<int> SelectionObservable
		{
			get { return _selectionSubject; }
		}
		private readonly Subject<int> _selectionSubject = new Subject<int>();

		public bool IsActive;

		private int _position;

		void Start()
		{
			SetFontMaterial(transform.GetChild(_position).GetComponent<TextMeshProUGUI>(), Color.yellow, new Color(1, 0.5f, 0));
		}

		void Update()
		{
			if (IsActive)
			{
				int beforePosition = _position;

				if (MappedInput.InputDevices.Any(pad => pad.GetAxisButtonDown(MappedAxis.Vertical, AxisDirection.Negative)))
				{
					_position++;
				}
				if (MappedInput.InputDevices.Any(pad => pad.GetAxisButtonDown(MappedAxis.Vertical, AxisDirection.Positive)))
				{
					_position--;
				}

				_position = (_position + transform.childCount) % transform.childCount;

				if (beforePosition != _position)
				{
					SetFontMaterial(transform.GetChild(beforePosition).GetComponent<TextMeshProUGUI>(), Color.white, Color.black);

					SetFontMaterial(transform.GetChild(_position).GetComponent<TextMeshProUGUI>(), Color.yellow,
						new Color(1, 0.5f, 0));
				}


				if (MappedInput.InputDevices.Any(pad => pad.GetButtonDown(MappedButton.Decide) ||
				                                        pad.GetButtonDown(MappedButton.Start)))
				{
					_selectionSubject.OnNext(_position);
				}
			}
		}

		void SetFontMaterial(TextMeshProUGUI text, Color innerColor, Color outlineColor)
		{
			text.color = innerColor;
			text.outlineColor = (Color32)outlineColor;
			text.SetMaterialDirty();
		}
	}
}