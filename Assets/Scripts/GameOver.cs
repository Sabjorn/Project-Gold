﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ProjectGold
{
	public class GameOver : MonoBehaviour
	{
		public float Duration;

		// Use this for initialization
		void Start()
		{
			// Logger.Push("Game Over");

			// Logger.Export();

			Observable.Timer(TimeSpan.FromSeconds(Duration)).Subscribe(_ =>
			{
				SceneManager.LoadScene("title");
			});
		}
	}
}