﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectGold
{
	public static class Environment
	{
		public static void Initialize()
		{
			Game.GameModeSelection = Game.GameMode.DeathMatchCasual;

			Game.CharacterSelections = new CharacterScriptableObject[]
			{
				null,null,null,null//Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/HammerGuy"),
				//Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Random"),
				//Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Random"),
				//Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Random")
			};
			// ランダムセレクト記憶用
			Game.LastCharacterSelections = new CharacterScriptableObject[]
			{
				null, null, null, null
			};

			Game.Round = 0;

			PlayerDatas = new Dictionary<DataCategory, float>[4]
			{
				new Dictionary<DataCategory, float>(),
				new Dictionary<DataCategory, float>(),
				new Dictionary<DataCategory, float>(),
				new Dictionary<DataCategory, float>()
			};
			foreach (Dictionary<DataCategory, float> data in PlayerDatas)
			{
				foreach (DataCategory key in Enum.GetValues(typeof(DataCategory)))
				{
					data[key] = 0;
				}
			}

			for (int i = 0; i < 4; i++)
			{
				Game.Scores[i] = 0;
			}

			isInitialized = true;
		}

		public static class Game
		{
			public enum GameMode
			{
				DeathMatch, DeathMatchCasual, TeamMatch, Practice, Tutorial
			}

			public static bool PracticeNeeded = true;
			public static GameMode GameModeSelection = GameMode.DeathMatchCasual;
			public static CharacterScriptableObject[] CharacterSelections =
			{
				Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Random"),
				Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Random"),
				Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Random"),
				Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Random")
			};
			public static CharacterScriptableObject[] LastCharacterSelections;
			public static bool[] CharacterAIFlags =
			{
				false,
				true,
				true,
				true
			};

			public static int Round = 0;
			public static int[] Scores = { 0, 0, 0, 0 };
			public static int ScoreThreshold = 2;

			public static bool RandomPosition = false;

			public static CharacterScriptableObject GetRandomCharacterScriptableObjects()
			{
				string[] charaList = {
					"Hammer", "Blitzcrank", "Ziggs", "Anivia", "Warwick", "AurelionSol", "Kagari"
				};

				return Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/" + charaList[UnityEngine.Random.Range(0, charaList.Length)]);
			}
		}

		// デバッグ用
		public static bool isInitialized = false;

		public static Dictionary<DataCategory, float>[] PlayerDatas = new Dictionary<DataCategory, float>[4]
		{
			new Dictionary<DataCategory, float>(),
			new Dictionary<DataCategory, float>(),
			new Dictionary<DataCategory, float>(),
			new Dictionary<DataCategory, float>()
		};

		public static readonly Dictionary<DataCategory, string> DataCategoryNames = new Dictionary<DataCategory, string>()
		{
			{DataCategory.Wins, "勝利数"},
			{DataCategory.Scores, "総得点"},
			{DataCategory.Defenses, "得点阻止"},
			{DataCategory.Distance, "走行距離"},
			{DataCategory.Hits, "攻撃命中回数"},
			{DataCategory.Damages, "攻撃喰らい回数"},
			{DataCategory.SpeedStar, "スピードスター"},
			{DataCategory.Combo, "最大連続得点"}
		};

		public static readonly Dictionary<DataCategory, float> DataCategoryCoefficients = new Dictionary<DataCategory, float>()
		{
			{DataCategory.Wins, 100},		// １勝利あたりの得点
			{DataCategory.Scores, 50},		// 総得点１００点毎の得点
			{DataCategory.Defenses, 40},	// 防衛時のコンボ量１００点あたりの得点
			{DataCategory.Distance, 0.04f},	// 移動１unit毎の得点
			{DataCategory.Hits, 1.8f},		// 攻撃命中１回あたりの得点
			{DataCategory.Damages, 1.8f},		// 攻撃喰らい１回あたりの得点
			{DataCategory.SpeedStar, 30},	// スピードスター１回あたりの得点
			{DataCategory.Combo, 80}		// 最大連続得点１００点毎の得点
		};

		public enum DataCategory
		{
			Wins, Scores, Defenses, Distance, Hits, Damages, SpeedStar, Combo
		}
	}
}