﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace ProjectGold.CharacterSelect
{
	public class CharacterSelector : MonoBehaviour
	{
		public UnityEvent OnSelected;
		public UnityEvent OnCanceled;
		public UnityEvent OnStart;

		[SerializeField] private Transform CharacterButtonsTransform;
		[SerializeField] private PlayerDock[] PlayerDocks;

		[SerializeField] private TextMeshProUGUI RoundText;
		[SerializeField] private TextMeshProUGUI ModeText;

		[SerializeField] private Button StartButton;
		[SerializeField] private Button SettingButton;

		private readonly List<int> _controllersTargetDock = new List<int>();

		void Start()
		{
			if (Environment.isInitialized == false)
			{
				Environment.Initialize();
			}
			if (Environment.Game.GameModeSelection == Environment.Game.GameMode.DeathMatch)
			{
				ModeText.text = "ガチバトル";

				SettingButton.gameObject.SetActive(true);
			}
			if (Environment.Game.GameModeSelection == Environment.Game.GameMode.DeathMatchCasual)
			{
				ModeText.text = "カジュアルバトル";

				SettingButton.gameObject.SetActive(true);
			}
			if (Environment.Game.GameModeSelection == Environment.Game.GameMode.Practice)
			{
				ModeText.text = "プラクティス";

				SettingButton.gameObject.SetActive(false);
			}

			int i = 0;
			foreach (var inputDevice in MappedInput.InputDevices)
			{
				_controllersTargetDock.Add(i);
				i++;
			}

			foreach (Transform button in CharacterButtonsTransform)
			{
				var charButton = button.GetComponent<CharacterButton>();
				charButton.OnEnterAsObservable.Subscribe(eventData =>
				{
					if (!PlayerDocks[_controllersTargetDock[eventData.ControllerNo]].IsDecided)
					{
						//OnSelected.Invoke();
						Environment.Game.CharacterSelections[_controllersTargetDock[eventData.ControllerNo]] = charButton.Character;
					}
				});
				charButton.OnClickAsObservable.Subscribe(eventData =>
				{
					OnSelected.Invoke();
					Select(eventData.ControllerNo, charButton.Character);
				});
			}

			int j = 0;
			foreach (var dock in PlayerDocks)
			{
				dock.CharacterNo = j;
				dock.TeamNo = j / 2;

				if (Environment.Game.GameModeSelection == Environment.Game.GameMode.Practice)
				{
					dock.IsAi = false;
				}
				else
				{
					dock.IsAi = j >= MappedInput.InputDevices.Count;
				}
				if (j < MappedInput.InputDevices.Count)
				{
					dock.AssignedControllerNo = j;

					SetTarget(j, j);
				}

				if (Environment.Game.CharacterSelections[j] != null) dock.Decide();
				else
					Environment.Game.CharacterSelections[j] =
						Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Random");

				j++;
			}

			this.UpdateAsObservable()
				.Select(_ => MappedInput.InputDevices.Any(pad => pad.GetButton(MappedButton.Cancel)))
				.DistinctUntilChanged()
				.Throttle(TimeSpan.FromSeconds(1))
				.Where(b => b)
				.Subscribe(_ =>
				{
					SceneManager.LoadScene("title");
				});
		}

		void Update()
		{
			var isReady = PlayerDocks.Select(d => d.IsDecided).Aggregate((a, e) => a && e);
			StartButton.gameObject.SetActive(isReady);

			int controllerNo = 0;
			foreach (var pad in MappedInput.InputDevices)
			{
				if (controllerNo >= _controllersTargetDock.Count)
				{
					_controllersTargetDock.Add(controllerNo);
					PlayerDocks[controllerNo].AssignedControllerNo = controllerNo;

					for (int i = 0; i < 4; i++)
					{
						if (i < MappedInput.InputDevices.Count)
						{
							SetTarget(i, i);
							PlayerDocks[i].Unselect();
						}
					}
				}

				if (pad.GetButtonDown(MappedButton.Cancel))
				{
					OnCanceled.Invoke();
					Back(controllerNo);
				}
				controllerNo++;
			}
		}

		void SetTarget(int controllerNo, int dockNo)
		{
			PlayerDocks[_controllersTargetDock[controllerNo]].ManipulatingControllerNo = null;

			_controllersTargetDock[controllerNo] = dockNo;

			PlayerDocks[dockNo].ManipulatingControllerNo = controllerNo;
		}

		void Select(int controllerNo, CharacterScriptableObject character)
		{
			Environment.Game.CharacterSelections[_controllersTargetDock[controllerNo]] = character;
			PlayerDocks[_controllersTargetDock[controllerNo]].Decide();

			for (int i = _controllersTargetDock[controllerNo] + 1; i < 4; i++)
			{
				if ((!PlayerDocks[i].AssignedControllerNo.HasValue || PlayerDocks[i].AssignedControllerNo == controllerNo)
					&& !_controllersTargetDock.Contains(i)
					&& !PlayerDocks[i].IsDecided
					)
				{
					SetTarget(controllerNo, i);
					break;
				}
			}
		}
		void Back(int controllerNo)
		{
			if (!PlayerDocks[_controllersTargetDock[controllerNo]].IsDecided)
			{
				for (int i = _controllersTargetDock[controllerNo]; i >= 0; i--)
				{
					if ((!PlayerDocks[i].AssignedControllerNo.HasValue || PlayerDocks[i].AssignedControllerNo == controllerNo)
						&& !_controllersTargetDock.Contains(i)
						&& PlayerDocks[i].IsDecided)
					{
						SetTarget(controllerNo, i);
						break;
					}
				}
			}

			PlayerDocks[_controllersTargetDock[controllerNo]].Unselect();
		}

		public void Ready()
		{
			OnStart.Invoke();

			foreach (var dock in PlayerDocks)
			{
				Environment.Game.CharacterAIFlags[dock.CharacterNo] = dock.IsAi;
			}

			switch (Environment.Game.GameModeSelection)
			{
				case Environment.Game.GameMode.DeathMatch:
					for (int i = 0; i < PlayerDocks.Length; i++)
					{
						Environment.Game.Scores[i] = 0;
						Environment.Game.LastCharacterSelections[i] = null;
					}
					SceneManager.LoadScene("death_4");
					break;
				case Environment.Game.GameMode.DeathMatchCasual:
					for (int i = 0; i < PlayerDocks.Length; i++)
					{
						Environment.Game.Scores[i] = 0;
						Environment.Game.LastCharacterSelections[i] = null;
					}
					SceneManager.LoadScene("death_4_casual");
					break;
				case Environment.Game.GameMode.TeamMatch:
					SceneManager.LoadScene("team_2v2");
					break;
				case Environment.Game.GameMode.Practice:
					SceneManager.LoadScene("practice_new");
					break;
			}
		}
	}
}