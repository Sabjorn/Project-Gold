﻿using System.Collections;
using System.Collections.Generic;
using ProjectGold;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class SettingRandomPositionEntity : MonoBehaviour
{
	public Toggle toggle;

	// Use this for initialization
	void Start()
	{
		toggle.isOn = Environment.Game.RandomPosition;
		toggle.OnValueChangedAsObservable().Subscribe(b => 
		{
			Environment.Game.RandomPosition = b;
		});		

	}


}
