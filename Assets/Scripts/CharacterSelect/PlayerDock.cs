﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectGold.CharacterSelect
{
	[ExecuteInEditMode]
	public class PlayerDock : MonoBehaviour
	{
		public enum Mode
		{
			Normal, Description
		}

		public int CharacterNo;
		public int TeamNo;

		private int? _assignedControllerNo;
		public int? AssignedControllerNo
		{
			get { return _assignedControllerNo; }
			set
			{
				_assignedControllerNo = value;

				if (value.HasValue)
				{
					IsAi = false;
					AIToggle.interactable = true;
				}
				else
				{
					IsAi = true;
					AIToggle.interactable = false;
				}
			}
		}

		public int? ManipulatingControllerNo
		{
			set
			{
				if (value.HasValue)
				{
					_manipulatingPlayerText.text = string.Format("Player {0}", value + 1);

					_animator.SetBool("Player Appear", true);
				}
				else
				{
					_animator.SetBool("Player Appear", false);
				}
			}
		}

		private bool _isDecided;
		public bool IsDecided { get { return _isDecided; } }

		private Mode _mode;
		public Mode CurrentMode { get { return _mode; } }
		public int DescriptionPage;

		public bool IsAi
		{
			get { return AIToggle.isOn; }
			set
			{
				AIToggle.isOn = value;

				if (!value)
				{
					if (AssignedControllerNo.HasValue)
						_backImage.sprite = _backgrounds.PlayerBackgroundSprites[AssignedControllerNo.Value];
					else _backImage.sprite = _backgrounds.ComBackgroundSprite;
				}
				else _backImage.sprite = _backgrounds.ComBackgroundSprite;
			}
		}

		[SerializeField] private TextMeshProUGUI _playerText;
		[SerializeField] private TextMeshProUGUI _teamText;
		[SerializeField] private TextMeshProUGUI _characterText;
		[SerializeField] private TextMeshProUGUI _descriptionText;
		[SerializeField] private Image _largeImage;
		[SerializeField] private Image _backImage;

		[SerializeField] private Toggle AIToggle;

		[SerializeField] private PlayerColors _playerColors;
		[SerializeField] private PlayerSelectBackground _backgrounds;

		[SerializeField] private TextMeshProUGUI _manipulatingPlayerText;

		private Animator _animator;

		void Awake()
		{
			_mode = Mode.Normal;

			_animator = GetComponent<Animator>();

			AssignedControllerNo = null;

			AIToggle.onValueChanged.AsObservable().Subscribe(b =>
			{
				Debug.Log("Changed");
				IsAi = b;
			});
		}

		void Update()
		{
			var character = Environment.Game.CharacterSelections[CharacterNo];
			if (character != null)
			{
				_playerText.text = !IsAi ? string.Format("Player {0}", CharacterNo + 1) : "COM";
				_teamText.text = string.Format("Team {0}", TeamNo + 1);
				_characterText.text = character.Name;
				_largeImage.sprite = character.LargeImage;

				_characterText.enabled = true;

				switch (CurrentMode)
				{
					case Mode.Normal:
						_playerText.enabled = true;
						_teamText.enabled = Environment.Game.GameModeSelection == Environment.Game.GameMode.TeamMatch;
						_largeImage.enabled = true;
						_largeImage.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
						_descriptionText.enabled = false;

						break;

					case Mode.Description:
						_playerText.enabled = false;
						_teamText.enabled = false;
						//_largeImage.enabled = false;
						_largeImage.color = new Color(1.0f, 1.0f, 1.0f, 0.3f);

						DescriptionPage %= character.Descriptions.Length;

						if (DescriptionPage < character.Descriptions.Length)
						{
							_descriptionText.enabled = true;
							_descriptionText.text = character.Descriptions[DescriptionPage];
						}

						break;
				}

				if (CharacterNo < MappedInput.InputDevices.Count)
				{
					var pad = MappedInput.InputDevices[CharacterNo];
					if (pad.GetButtonDown(MappedButton.Second))
					{
						SwitchDisplay();
					}
				}
			}

			//_backImage.color = _playerColors.Colors[PlayerNo];
		}

		[ContextMenu("表示切り替え")]
		public void SwitchDisplay()
		{
			if (_mode == Mode.Normal)
			{
				_mode = Mode.Description;
				DescriptionPage = 0;
			}
			else
			{
				DescriptionPage++;

				var character = Environment.Game.CharacterSelections[CharacterNo];
				if (DescriptionPage >= character.Descriptions.Length)
				{
					_mode = Mode.Normal;
				}
			}
		}

		[ContextMenu("決定")]
		public void Decide()
		{
			_isDecided = true;

			_animator.SetTrigger("Select");
		}

		[ContextMenu("決定解除")]
		public void Unselect()
		{
			_isDecided = false;

			_mode = Mode.Normal;
			DescriptionPage = 0;

			_animator.SetTrigger("Unselect");
		}

		// TODO for debug select
		[ContextMenu("キャラセレ:ガイ")]
		private void CharacterChange1()
		{
			Environment.Game.CharacterSelections[CharacterNo] = Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Hammer");
			Decide();
		}
		[ContextMenu("キャラセレ:マークツー")]
		private void CharacterChange2()
		{
			Environment.Game.CharacterSelections[CharacterNo] = Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Blitzcrank");
			Decide();
		}
		[ContextMenu("キャラセレ:ハナビ")]
		private void CharacterChange3()
		{
			Environment.Game.CharacterSelections[CharacterNo] = Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Ziggs");
			Decide();
		}
		[ContextMenu("キャラセレ:セト")]
		private void CharacterChange4()
		{
			Environment.Game.CharacterSelections[CharacterNo] = Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Anivia");
			Decide();
		}
		[ContextMenu("キャラセレ:エクレア")]
		private void CharacterChange5()
		{
			Environment.Game.CharacterSelections[CharacterNo] = Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Eclair");
			Decide();
		}
		[ContextMenu("キャラセレ:キバ")]
		private void CharacterChange6()
		{
			Environment.Game.CharacterSelections[CharacterNo] = Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Warwick");
			Decide();
		}
		[ContextMenu("キャラセレ:ドラクル")]
		private void CharacterChange7()
		{
			Environment.Game.CharacterSelections[CharacterNo] = Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/AurelionSol");
			Decide();
		}
		[ContextMenu("キャラセレ:ランダム")]
		private void CharacterChange8()
		{
			Environment.Game.CharacterSelections[CharacterNo] = Resources.Load<CharacterScriptableObject>("ScriptableObjects/Characters/Random");
			Decide();
		}
	}
}