﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ProjectGold.CharacterSelect
{
	[ExecuteInEditMode]
	public class ScaleUI : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler
	{
		public float ScaleRate = 1.1f;

		public void OnPointerDown(PointerEventData eventData)
		{
			//Debug.Log("Down:\n" + eventData);
			this.GetComponent<RectTransform>().localScale = Vector3.one;
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			this.GetComponent<RectTransform>().localScale = Vector3.one * 1.1f;
			//Debug.Log("Enter:\n" + eventData);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			this.GetComponent<RectTransform>().localScale = Vector3.one;
			//Debug.Log("Exit:\n" + eventData);
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			//Debug.Log("Up:\n" + eventData);
		}


	}
}