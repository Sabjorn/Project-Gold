﻿using System.Collections;
using System.Collections.Generic;
using ProjectGold;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class SettingRoundsEntity : MonoBehaviour
{
	public TextMeshProUGUI NumberText;
	public Button PlusButton, MinusButton;
	public int MinScore, MaxScore;

	// Use this for initialization
	void Start()
	{
		PlusButton.onClick.AsObservable().Subscribe(_ =>
		{
			Environment.Game.ScoreThreshold++;
			if (Environment.Game.ScoreThreshold > MaxScore)
				Environment.Game.ScoreThreshold = MaxScore;

			Refresh();

		});
		MinusButton.onClick.AsObservable().Subscribe(_ =>
		{
			Environment.Game.ScoreThreshold--;
			if (Environment.Game.ScoreThreshold < MinScore)
				Environment.Game.ScoreThreshold = MinScore;

			Refresh();

		});

		Refresh();
	}

	void Refresh()
	{
		NumberText.text = Environment.Game.ScoreThreshold.ToString();
	}
}
