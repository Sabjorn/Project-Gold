﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectGold.CharacterSelect
{
	public class SettingModalBehaviour : MonoBehaviour {

		public void Show()
		{
			var canvasGroup = GetComponent<CanvasGroup>();

			canvasGroup.alpha = 1;
			canvasGroup.blocksRaycasts = true;
		}

		public void Hide()
		{
			var canvasGroup = GetComponent<CanvasGroup>();

			canvasGroup.alpha = 0;
			canvasGroup.blocksRaycasts = false;
		}
	}
}