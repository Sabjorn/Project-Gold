﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ProjectGold.CharacterSelect
{
	[ExecuteInEditMode]
	public class CharacterButton : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler
	{
		public CharacterScriptableObject Character;

		private Subject<MultiPointerEventData> _onClickSubject;
		private Subject<MultiPointerEventData> _onEnterSubject;

		public IObservable<MultiPointerEventData> OnClickAsObservable { get { return _onClickSubject; } }
		public IObservable<MultiPointerEventData> OnEnterAsObservable { get { return _onEnterSubject; } }

		[SerializeField] private Image _iconImage;

		void Awake()
		{
			_onClickSubject = new Subject<MultiPointerEventData>();
			_onEnterSubject = new Subject<MultiPointerEventData>();
		}

		void Update()
		{
			if (Character != null)
			{
				_iconImage.sprite = Character.Icon;
			}
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			_onClickSubject.OnNext(eventData as MultiPointerEventData);
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			//Debug.Log("Down:\n" + eventData);
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			_onEnterSubject.OnNext(eventData as MultiPointerEventData);
			//Debug.Log("Enter:\n" + eventData);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			//Debug.Log("Exit:\n" + eventData);
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			//Debug.Log("Up:\n" + eventData);
		}
	}
}