﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MultiPointerEventData : PointerEventData
{
	public int ControllerNo;
	public MultiPointerEventData(EventSystem eventSystem) : base(eventSystem)
	{
	}

	public override string ToString()
	{
		var str = base.ToString();
		return "<b>Controller No:</b> " + ControllerNo + "\n" + str;
	}
}
