﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalInputDevice : IControllerInput
{
	private readonly InputDevice _inputDevice;

	public LocalInputDevice(InputDevice inputDevice=null)
	{
		_inputDevice = inputDevice;
	}

	public float GetAxis(MappedAxis axis)
	{
		if (_inputDevice != null)
			return _inputDevice.GetAxis(axis);
		return 0;
	}

	public bool GetButton(MappedButton button)
	{
		if (_inputDevice != null)
			return _inputDevice.GetButton(button);
		return false;
	}

	public bool GetButtonDown(MappedButton button)
	{
		if (_inputDevice != null)
			return _inputDevice.GetButtonDown(button);
		return false;
	}

	public bool GetButtonUp(MappedButton button)
	{
		if (_inputDevice != null)
			return _inputDevice.GetButtonUp(button);
		return false;
	}
}
