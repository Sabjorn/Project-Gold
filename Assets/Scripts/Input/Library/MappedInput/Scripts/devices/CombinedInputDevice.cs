﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CombinedInputDevice : InputDevice
{
	public InputDevice[] InputDevices;

	public override string GetButtonName(MappedButton button)
	{
		return InputDevices.Select(d => d.GetButtonName(button)).Aggregate((acc, e) => acc + " / " + e);
	}

	public override string GetAxisName(MappedAxis axis)
	{
		return InputDevices.Select(d => d.GetAxisName(axis)).Aggregate((acc, e) => acc + " / " + e);
	}

	public override bool GetButton(MappedButton button)
	{
		return InputDevices.Select(d => d.GetButton(button)).Aggregate(false, (acc, e) => acc || e);
	}

	public override bool GetButtonDown(MappedButton button)
	{
		return InputDevices.Select(d => d.GetButtonDown(button)).Aggregate(false, (acc, e) => acc || e);
	}

	public override bool GetButtonUp(MappedButton button)
	{
		return InputDevices.Select(d => d.GetButtonUp(button)).Aggregate(false, (acc, e) => acc || e);
	}

	protected override float GetAxisValueRaw(MappedAxis axis)
	{
		return InputDevices.Select(d => d.GetAxisRaw(axis)).Aggregate(0f, (acc, e) => Mathf.Abs(acc) < Mathf.Abs(e) ? e : acc);
	}
}
