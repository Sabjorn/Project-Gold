﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IControllerInput
{
	float GetAxis(MappedAxis axis);

	bool GetButton(MappedButton button);
	bool GetButtonDown(MappedButton button);
	bool GetButtonUp(MappedButton button);
}
