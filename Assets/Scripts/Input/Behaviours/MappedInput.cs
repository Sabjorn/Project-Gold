using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ProjectGold.Utilities;

/// <summary>
/// GamepadInputで取得した入力をゲーム用の入力に変換する。
/// </summary>
public class MappedInput : SingletonMonoBehaviour<MappedInput>
{
	public float LastInputTime { get; private set; }

	public static bool UseDisabledDevices;

	static InputDevice _activeDevice;
	public static InputDevice ActiveDevice
	{
		get
		{
			return _activeDevice;
		}
		set
		{
			if (_activeDevice == value)
				return;

			_activeDevice = value;
			if (OnActiveDeviceChanged != null)
				OnActiveDeviceChanged(_activeDevice);
		}
	}

	public static System.Action<InputDevice> OnDeviceAdded;
	public static System.Action<InputDevice> OnDeviceRemoved;

	public static List<InputDevice> InputDevices = new List<InputDevice>();

	public GamepadInputMapping GamepadInputMapping;
	public KeyboardInputMapping KeyboardInputMapping;
	public MouseInputMapping MouseInputMapping;

	public static System.Action<InputDevice> OnActiveDeviceChanged;

	new void Awake()
	{
		base.Awake();

#if UNITY_STANDALONE || UNITY_EDITOR

		if (MouseInputMapping != null)
			AddMouseDevice();

		if (KeyboardInputMapping != null)
			AddKeyboardDevice();

#elif UNITY_IOS || UNITY_ANDROID
		AddMouseDevice ();
#endif

		DontDestroyOnLoad(gameObject);
	}

	void Start()
	{
		for (int i = 0; i < GamepadInput.Instance.Gamepads.Count; i++)
		{
			OnGamepadAdded(GamepadInput.Instance.Gamepads[i]);
		}

		GamepadInput.Instance.OnGamepadAdded += OnGamepadAdded;
		GamepadInput.Instance.OnGamepadRemoved += OnGamepadRemoved;
	}

	void OnDestroy()
	{
		GamepadInput.Instance.OnGamepadAdded -= OnGamepadAdded;
		GamepadInput.Instance.OnGamepadRemoved -= OnGamepadRemoved;
	}

	void Update()
	{
		for (int i = 0; i < InputDevices.Count; i++)
		{
			if (!InputDevices[i].autoActive && !UseDisabledDevices)
				continue;
			if (!ActiveDevice || InputDevices[i].lastInputTime > ActiveDevice.lastInputTime)
				ActiveDevice = InputDevices[i];
			LastInputTime = Mathf.Max(InputDevices[i].lastInputTime, LastInputTime);
		}
	}

	void OnGamepadAdded(GamepadDevice gamepad)
	{
		if (InputDevices.Count == 0 || InputDevices[0] is GamepadInputDevice || InputDevices[0] is CombinedInputDevice)
		{
			GameObject obj = new GameObject();

			var device = obj.AddComponent<GamepadInputDevice>();
			(device as GamepadInputDevice).gamepad = gamepad;
			obj.transform.parent = transform;
			obj.name = "Device: " + gamepad.displayName;

			InputDevices.Add(device);

			if (OnDeviceAdded != null)
				OnDeviceAdded(device);
		}
		else
		{
			GameObject obj = InputDevices[0].gameObject;

			var device = obj.AddComponent<GamepadInputDevice>();
			(device as GamepadInputDevice).gamepad = gamepad;

			var deviceCombined = obj.AddComponent<CombinedInputDevice>();
			(deviceCombined as CombinedInputDevice).InputDevices = new InputDevice[]
			{
				InputDevices[0],
				device
			};

			obj.name = "Combined";
			InputDevices[0] = deviceCombined;

			if (OnDeviceAdded != null)
				OnDeviceAdded(device);
		}
	}

	void OnGamepadRemoved(GamepadDevice gamepad)
	{
		for (int i = 0; i < InputDevices.Count; i++)
		{
			var device = InputDevices[i];
			if (device is GamepadInputDevice && (device as GamepadInputDevice).gamepad == gamepad)
			{
				InputDevices.Remove(device);

				if (OnDeviceRemoved != null)
					OnDeviceRemoved(device);
				Destroy(device.gameObject);
			}

			var deviceCombined = device as CombinedInputDevice;
			if (deviceCombined != null &&
				deviceCombined.InputDevices.Any(
					d => d is GamepadInputDevice && ((GamepadInputDevice)d).gamepad == gamepad))
			{
				var childDevices = deviceCombined.InputDevices.ToList();
				if (childDevices.Count == 1)
				{
					InputDevices.Remove(deviceCombined);

					if (OnDeviceRemoved != null)
						OnDeviceRemoved(deviceCombined);
					Destroy(deviceCombined.gameObject);
				}
				else
				{
					var devicePad = deviceCombined.InputDevices.First(
						d => d is GamepadInputDevice && ((GamepadInputDevice)d).gamepad == gamepad) as GamepadInputDevice;

					childDevices.Remove(devicePad);
					deviceCombined.InputDevices = childDevices.ToArray();
					if (OnDeviceRemoved != null)
						OnDeviceRemoved(devicePad);
					Destroy(devicePad);
				}
			}
		}
	}

	void AddKeyboardDevice()
	{
		if (InputDevices.Count == 0 || InputDevices[0] is KeyboardInputDevice || InputDevices[0] is CombinedInputDevice)
		{
			GameObject obj = new GameObject("Keyboard", typeof(KeyboardInputDevice));
			obj.transform.parent = transform;
			InputDevice device = obj.GetComponent<KeyboardInputDevice>();
			InputDevices.Add(device);
		}
		else
		{
			GameObject obj = InputDevices[0].gameObject;
			var keyboard = obj.AddComponent<KeyboardInputDevice>();

			var combined = obj.AddComponent<CombinedInputDevice>();
			combined.InputDevices = new InputDevice[] { keyboard, InputDevices[0] };
			InputDevices[0] = combined;
		}
	}

	void AddMouseDevice()
	{
		GameObject obj = new GameObject("Mouse", typeof(MouseInputDevice));
		obj.transform.parent = transform;
		InputDevice device = obj.GetComponent<MouseInputDevice>();
		InputDevices.Add(device);
	}
}