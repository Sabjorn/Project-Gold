﻿using System.Collections;
using System.Collections.Generic;
using ProjectGold;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CursorBehaviour : MonoBehaviour {
	private int _controllerNo;
	public int ControllerNo
	{
		get { return _controllerNo; }
		set
		{
			_controllerNo = value;
			_playerNoText.text = string.Format("{0}P", value + 1);
			_playerNoText.color = _playerColors.Colors[value];
			var image = GetComponent<Image>();
			image.color = _playerColors.Colors[value];
		}
	}

	[SerializeField] private TextMeshProUGUI _playerNoText;
	[SerializeField] private PlayerColors _playerColors;
}
