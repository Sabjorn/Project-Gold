﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProjectGold.Character;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class NetworkCharacterController : NetworkBehaviour, IControllerInput
{
	private float _horizontalInput, _verticalInput;

	private Dictionary<MappedButton, bool> _buttonStateDictionary;
	private Dictionary<MappedButton, bool> _buttonDownDictionary;
	private Dictionary<MappedButton, bool> _buttonUpDictionary;

	private InputDevice _localInputDevice;

	private Matrix4x4 _matrix;

	public CharacterBaseBehaviour TargetCharacter;

	void Start()
	{
		if (MappedInput.InputDevices.Any())
			_localInputDevice = MappedInput.InputDevices[0];

		_buttonStateDictionary = new Dictionary<MappedButton, bool>();
		_buttonDownDictionary = new Dictionary<MappedButton, bool>();
		_buttonUpDictionary = new Dictionary<MappedButton, bool>();
		foreach (var mappedButton in (MappedButton[])Enum.GetValues(typeof(MappedButton)))
		{
			_buttonStateDictionary[mappedButton] = false;
			_buttonDownDictionary[mappedButton] = false;
			_buttonUpDictionary[mappedButton] = false;
		}

		_matrix = new Matrix4x4();
	}

	void Update()
	{
		foreach (var mappedButton in (MappedButton[])Enum.GetValues(typeof(MappedButton)))
		{
			_buttonDownDictionary[mappedButton] = false;
			_buttonUpDictionary[mappedButton] = false;
		}

		if (isLocalPlayer)
		{
			_horizontalInput = _localInputDevice.GetAxis(MappedAxis.Horizontal);
			_verticalInput = _localInputDevice.GetAxis(MappedAxis.Vertical);

			foreach (var mappedButton in (MappedButton[])Enum.GetValues(typeof(MappedButton)))
			{
				var state = _localInputDevice.GetButton(mappedButton);
				if (_buttonStateDictionary[mappedButton] != state)
				{
					_buttonStateDictionary[mappedButton] = state;

					CmdSendButtonState(mappedButton, state);
				}

				_buttonDownDictionary[mappedButton] = _localInputDevice.GetButtonDown(mappedButton);
				_buttonUpDictionary[mappedButton] = _localInputDevice.GetButtonUp(mappedButton);
			}

			CmdSendAxesState(_horizontalInput, _verticalInput);

			CmdSendPosture(TargetCharacter.transform.localToWorldMatrix);
		}

		if (isServer)
		{
			RpcSyncAxesState(_horizontalInput, _verticalInput);
		}
	}

	void SyncAxesState(float horizontal, float vertical)
	{
		_horizontalInput = horizontal;
		_verticalInput = vertical;
	}

	[Command(channel = 1)]
	void CmdSendAxesState(float horizontal, float vertical)
	{
		SyncAxesState(horizontal, vertical);
		RpcSyncAxesState(horizontal, vertical);
	}

	[ClientRpc(channel = 1)]
	void RpcSyncAxesState(float horizontal, float vertical)
	{
		if (!isLocalPlayer)
		{
			SyncAxesState(horizontal, vertical);
		}
	}

	void SyncButtonState(MappedButton button, bool state)
	{
		if (_buttonStateDictionary[button] != state)
		{
			if (state)
				_buttonDownDictionary[button] = true;
			else
				_buttonUpDictionary[button] = true;
		}

		_buttonStateDictionary[button] = state;
	}

	[Command(channel = 0)]
	void CmdSendButtonState(MappedButton button, bool state)
	{
		SyncButtonState(button, state);
		RpcSyncButtonState(button, state);
	}

	[ClientRpc(channel = 0)]
	void RpcSyncButtonState(MappedButton button, bool state)
	{
		if (!isLocalPlayer)
			SyncButtonState(button, state);
	}

	void SyncPosture(Matrix4x4 matrix)
	{
		_matrix = matrix;

		TargetCharacter.transform.position = (Vector3)_matrix.GetColumn(3);
		TargetCharacter.transform.rotation = _matrix.rotation;
	}

	[Command(channel = 1)]
	void CmdSendPosture(Matrix4x4 matrix)
	{
		SyncPosture(matrix);
		RpcSyncPosture(matrix);
	}

	[ClientRpc(channel = 1)]
	void RpcSyncPosture(Matrix4x4 matrix)
	{
		if (!isLocalPlayer)
			SyncPosture(matrix);
	}

	public float GetAxis(MappedAxis axis)
	{
		switch (axis)
		{
			case MappedAxis.Horizontal: return _horizontalInput;
			case MappedAxis.Vertical: return _verticalInput;
			default: return 0;
		}
	}

	public bool GetButton(MappedButton button)
	{
		return _buttonStateDictionary[button];
	}

	public bool GetButtonDown(MappedButton button)
	{
		return _buttonDownDictionary[button];
	}

	public bool GetButtonUp(MappedButton button)
	{
		return _buttonUpDictionary[button];
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(NetworkCharacterController))]
public class NetworkInputDeviceEditor : Editor
{
	private Material mat;
	private NetworkCharacterController targetInput;

	void OnEnable()
	{
		var shader = Shader.Find("Hidden/Internal-Colored");
		mat = new Material(shader);

		targetInput = target as NetworkCharacterController;

		EditorApplication.update += Update;
	}

	void OnDisable()
	{
		EditorApplication.update -= Update;
	}

	void Update()
	{
		Repaint();
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		EditorGUILayout.LabelField("Type: " + (targetInput.isLocalPlayer ? "Local Controller" : "Network Controller"));

		var height = 60;
		var rect = GUILayoutUtility.GetRect(10, 2000, height, height);

		//if (Event.current.type == EventType.repaint)
		{
			GUI.BeginClip(rect);
			GL.PushMatrix();
			GL.LoadPixelMatrix();

			GL.Clear(true, false, Color.black);
			mat.SetPass(0);

			/*
			GL.Begin(GL.TRIANGLE_STRIP);
			GL.Color(new Color(0, 0, 0, 0.5f));
			GL.Vertex3(0, 0, 0);
			GL.Vertex3(rect.width, 0, 0);
			GL.Vertex3(0, rect.height, 0);
			GL.Vertex3(rect.width, rect.height, 0);
			GL.End();
			*/

			GL.Begin(GL.LINE_STRIP);
			GL.Color(Color.black);
			GL.Vertex3(0, 0, 0);
			GL.Vertex3(height, 0, 0);
			GL.Vertex3(height, height, 0);
			GL.Vertex3(0, height, 0);
			GL.Vertex3(0, 0, 0);
			GL.End();

			var x = Mathf.Lerp(0, height, Mathf.InverseLerp(-1, 1, targetInput.GetAxis(MappedAxis.Horizontal)));
			var y = Mathf.Lerp(0, height, Mathf.InverseLerp(-1, 1, -targetInput.GetAxis(MappedAxis.Vertical)));
			GL.Begin(GL.TRIANGLE_STRIP);
			GL.Color(Color.red);
			GL.Vertex3(x - 5, y - 5, 0);
			GL.Vertex3(x + 5, y - 5, 0);
			GL.Vertex3(x - 5, y + 5, 0);
			GL.Vertex3(x + 5, y + 5, 0);
			GL.End();

			var buttonSize = 20;
			var spaceSize = 5;

			int i = 0;
			foreach (MappedButton button in Enum.GetValues(typeof(MappedButton)))
			{
				x = height + spaceSize + i * (buttonSize + spaceSize);
				y = 0;
				GL.Begin(GL.LINE_STRIP);
				GL.Color(Color.black);
				GL.Vertex3(x, y, 0);
				GL.Vertex3(x + buttonSize, y, 0);
				GL.Vertex3(x + buttonSize, y + buttonSize, 0);
				GL.Vertex3(x, y + buttonSize, 0);
				GL.Vertex3(x, y, 0);
				GL.End();
				i++;
			}

			GL.PopMatrix();
			GUI.EndClip();
		}

		var m = GUI.matrix;
		GUI.matrix = Matrix4x4.Translate(new Vector3(-rect.width, -rect.height, 0)) * m;
		EditorGUI.LabelField(rect, "Hoge");
		GUI.matrix = m;

		EditorGUILayout.Space();
	}
}
#endif