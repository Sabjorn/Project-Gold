﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using ProjectGold;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GamepadInputModule : BaseInputModule
{
	public float Speed;

	public MappedButton PressButton;

	[SerializeField] private Canvas _canvas;
	[SerializeField] private GameObject _cursorPrefab;

	public PlayerColors PlayerColors;

	public float CursorMargin;

	private List<MultiPointerEventData> _pointersData;
	private List<RectTransform> _cursorTransforms;

	private List<Vector2> _positions;

	void Start()
	{
		_pointersData = new List<MultiPointerEventData>();
		_positions = new List<Vector2>();//new Vector2(Screen.width / 2f, Screen.height / 2f);
		_cursorTransforms = new List<RectTransform>();
	}

	public override void Process()
	{
		for (int i = 0; i < MappedInput.InputDevices.Count; i++)
		{
			if (_pointersData.Count <= i)
			{
				_pointersData.Add(new MultiPointerEventData(eventSystem) { ControllerNo = i });
				_pointersData[i].Reset();

				_positions.Add(new Vector2(Screen.width / 2f + Mathf.Lerp(-100, 100, (float)i / MappedInput.InputDevices.Count), Screen.height / 2f + 50));

				var cursor = Instantiate(_cursorPrefab, _canvas.transform);
				_cursorTransforms.Add((RectTransform)cursor.transform);

				var cursorComponent = cursor.GetComponent<CursorBehaviour>();
				cursorComponent.ControllerNo = i;
			}

			MoveCursor(i);

			EarlyUpdatePointerData(i);

			InputPointerData(i);
		}
	}

	private void MoveCursor(int controllerNo)
	{
		if (_cursorTransforms.Count > controllerNo)
		{
			if (MappedInput.InputDevices.Count > controllerNo)
			{
				var pad = MappedInput.InputDevices[controllerNo];

				var v = new Vector2(pad.GetAxisRaw(MappedAxis.Horizontal), pad.GetAxisRaw(MappedAxis.Vertical));

				var p = _positions[controllerNo];
				p += v * Speed * Screen.height * Time.deltaTime;
				p.x = Mathf.Clamp(p.x, Screen.height * CursorMargin, Screen.width - Screen.height * CursorMargin);
				p.y = Mathf.Clamp(p.y, Screen.height * CursorMargin, Screen.height - Screen.height * CursorMargin);

				_positions[controllerNo] = p;
			}

			var cp = _cursorTransforms[controllerNo].position;
			cp.x = _positions[controllerNo].x;
			cp.y = _positions[controllerNo].y;

			_cursorTransforms[controllerNo].position = cp;
		}
	}

	RaycastResult Raycast(PointerEventData pointerData)
	{
		List<RaycastResult> results = new List<RaycastResult>();
		eventSystem.RaycastAll(pointerData, results);
		return FindFirstRaycast(results);
	}

	void EarlyUpdatePointerData(int controllerNo)
	{
		var oldPosition = _pointersData[controllerNo].position;
		_pointersData[controllerNo].position = _positions[controllerNo];
		_pointersData[controllerNo].delta = _pointersData[controllerNo].position - oldPosition;

		_pointersData[controllerNo].pointerCurrentRaycast = Raycast(_pointersData[controllerNo]);
	}

	void InputPointerData(int controllerNo)
	{
		if (_pointersData[controllerNo].pointerCurrentRaycast.gameObject != _pointersData[controllerNo].pointerEnter)
		{
			if (_pointersData[controllerNo].pointerEnter != null)
			{
				ExecuteEvents.ExecuteHierarchy(_pointersData[controllerNo].pointerEnter, _pointersData[controllerNo], ExecuteEvents.pointerExitHandler);
			}
			_pointersData[controllerNo].pointerEnter = _pointersData[controllerNo].pointerCurrentRaycast.gameObject;
			ExecuteEvents.ExecuteHierarchy(_pointersData[controllerNo].pointerCurrentRaycast.gameObject, _pointersData[controllerNo], ExecuteEvents.pointerEnterHandler);
		}

		if (MappedInput.InputDevices.Count > controllerNo)
		{
			var pad = MappedInput.InputDevices[controllerNo];

			if (pad.GetButtonDown(PressButton))
			{
				_pointersData[controllerNo].pointerPressRaycast = Raycast(_pointersData[controllerNo]);
				var firstObject = _pointersData[controllerNo].pointerPressRaycast.gameObject;

				ExecuteEvents.ExecuteHierarchy(firstObject, _pointersData[controllerNo], ExecuteEvents.pointerDownHandler);

				_pointersData[controllerNo].pointerPress = firstObject;
			}
			if (pad.GetButtonUp(PressButton))
			{
				if (_pointersData[controllerNo].pointerPress != null)
				{
					ExecuteEvents.ExecuteHierarchy(_pointersData[controllerNo].pointerPress, _pointersData[controllerNo], ExecuteEvents.pointerUpHandler);
					if (_pointersData[controllerNo].pointerEnter == _pointersData[controllerNo].pointerPress)
						ExecuteEvents.ExecuteHierarchy(_pointersData[controllerNo].pointerPress, _pointersData[controllerNo], ExecuteEvents.pointerClickHandler);

					_pointersData[controllerNo].pointerPress = null;
				}
			}
		}
	}
}
