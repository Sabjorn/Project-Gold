﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ProjectGold.Utilities;

/// <summary>
/// 様々なコントローラの入力を共通化する。
/// </summary>
public class GamepadInput : SingletonMonoBehaviour<GamepadInput>
{
	GamepadManager _manager;

	public List<GamepadDevice> Gamepads
	{
		get
		{
			return _manager.gamepads;
		}
	}

	public GamepadLayout XGamepadLayout;
	public List<UnGamepadConfig> UnGamepadConfigs;

	public event System.Action<GamepadDevice> OnGamepadAdded;
	public event System.Action<GamepadDevice> OnGamepadRemoved;

	new void Awake()
	{
		base.Awake();
		DontDestroyOnLoad(gameObject);
	}

	void Start()
	{
		// manager = new XGamepadManager (xGamepadLayout);
		_manager = new UnGamepadManager(UnGamepadConfigs);

		_manager.OnGamepadAdded += GamepadAdded;
		_manager.OnGamepadRemoved += GamepadRemoved;

		_manager.Init();
	}

	void OnDestroy()
	{
		if (_manager != null)
		{
			_manager.OnGamepadAdded -= GamepadAdded;
			_manager.OnGamepadRemoved -= GamepadRemoved;
		}
	}

	void Update()
	{
		_manager.Update();
	}

	void GamepadAdded(GamepadDevice gamepadDevice)
	{
		if (OnGamepadAdded != null)
			OnGamepadAdded(gamepadDevice);
	}

	void GamepadRemoved(GamepadDevice gamepadDevice)
	{
		if (OnGamepadRemoved != null)
			OnGamepadRemoved(gamepadDevice);
	}
}

