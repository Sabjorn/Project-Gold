﻿using UnityEngine;
using System.Collections;

public enum MappedButton
{
	// インゲーム
	Grab,
	Skill1,
	Skill2,
	Skill3,

	// メニュー
	Decide,
	Second,
	Cancel,
	LTrigger,
	RTrigger,

	// 共通
	Start
}
