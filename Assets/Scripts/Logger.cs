﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Policy;
using UnityEngine;

namespace ProjectGold
{
	public class Logger
	{
		private static readonly List<string[]> _logs = new List<string[]>();

		public static string LogPath = "log.txt";

		public static void Export()
		{
			if (!File.Exists(LogPath))
			{
				File.Create(LogPath).Close();
			}

			using (StreamWriter sw = new StreamWriter(LogPath, true))
			{
				foreach (var log in _logs)
				{
					sw.WriteLine(string.Join("\t", log));
				}
			}

			_logs.Clear();
		}

		public static void Push(params string[] data)
		{
			var e = new List<string>() { DateTime.Now.ToString("O"), Time.frameCount.ToString() };

			e.AddRange(data);

			_logs.Add(e.ToArray());
		}
	}
}