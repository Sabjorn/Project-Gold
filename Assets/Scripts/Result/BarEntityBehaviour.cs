﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;

namespace ProjectGold.Result
{
	public class BarEntityBehaviour : MonoBehaviour
	{
		public int PlayerNo;
		public float ElementSpeed;

		private float _score;
		public float Score { get { return _score; } }

		[SerializeField] private TextMeshProUGUI _playerText;
		[SerializeField] private TextMeshProUGUI _scoreText;

		[SerializeField] private Transform _elementParentTransform;
		[SerializeField] private GameObject _elementPrefab;

		void Start()
		{
			_playerText.text = (PlayerNo + 1) + "P";
			_playerText.color = Resources.Load<PlayerColors>("ScriptableObjects/DefaultColors").Colors[PlayerNo];

			_score = 0;
		}

		public IObservable<Unit> Add(float width, Color color = default(Color))
		{
			if (width > 0)
			{
				var element = Instantiate(_elementPrefab, _elementParentTransform);
				element.GetComponent<Image>().color = color;

				var w = 0f;
				return element.UpdateAsObservable()
					.Do(_ =>
					{
						var size = element.GetComponent<RectTransform>().sizeDelta;
						size.x = w;
						element.GetComponent<RectTransform>().sizeDelta = size;

						var delta = Time.deltaTime * ElementSpeed;
						if (w + delta > width) delta -= w + delta - width;

						w += delta;
						_score += delta;
					})
					.SkipWhile(_ => w < width)
					.First();
			}
			else return Observable.ReturnUnit();
		}

		void Update()
		{
			_scoreText.text = _score.ToString("N0");
		}

		[ContextMenu("追加テスト")]
		void Test()
		{
			Add(100).Subscribe();
		}
	}
}