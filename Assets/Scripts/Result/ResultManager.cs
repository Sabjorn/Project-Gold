﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProjectGold.Utilities;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace ProjectGold.Result
{
	public class ResultManager : MonoBehaviour
	{
		public UnityEvent OnNext;

		[SerializeField] private BarEntityBehaviour[] _bars;
		[SerializeField] private TextMeshProUGUI _categoryText;
		[SerializeField] private TextMeshProUGUI _legendsText;
		[SerializeField] private TextMeshProUGUI _winsText;
		[SerializeField] private Animator _animator;
		[SerializeField] private AnimationClip _startAnimationClip;

		[SerializeField] private AudioClip _taikoClip, _gaugeClip, _applauseClip;

		private bool _canNext;
		private AudioSource _audioSource;

		// Use this for initialization
		void Start()
		{
			var obs = Observable.Timer(TimeSpan.FromSeconds(_startAnimationClip.length + 1)).AsUnitObservable();

			_categoryText.text = "";
			_legendsText.text = "";
			_winsText.text = "";

			var colors = Resources.Load<PlayerColors>("ScriptableObjects/ResultColors");

			// var distanceOffset = 0;
			var distanceOffset = -Environment.PlayerDatas.Min(data => data[Environment.DataCategory.Distance]) * Environment.DataCategoryCoefficients[Environment.DataCategory.Distance] + 5;
			foreach (var dataCategory in (Environment.DataCategory[])Enum.GetValues(typeof(Environment.DataCategory)))
			{
				var category = dataCategory;
				obs = obs.SelectMany(
					_ => Observable.Defer(() =>
						{
							_categoryText.text = Environment.DataCategoryNames[category];
							_animator.SetTrigger("Category Pop");

							_audioSource.PlayOneShot(_taikoClip);

							return Observable.Timer(TimeSpan.FromSeconds(0.8));
						})
						.Do(__ =>
						{
							_audioSource.clip = _gaugeClip;
							_audioSource.Play();
						})
						.SelectMany(__ =>
						{
							var observables = _bars.Select((b, i) =>
							{
								var amount = 0f;
								if (category == Environment.DataCategory.Distance)
								{
									amount = (Environment.PlayerDatas[i][category]) * Environment.DataCategoryCoefficients[category] + distanceOffset;
								}
								else
									amount = Environment.PlayerDatas[i][category] * Environment.DataCategoryCoefficients[category];

								return b.Add(amount, colors.Colors[(int)category]);
							}).ToArray();

							// デバッグ用
							// var observables = _bars.Select((b, i) => b.Add(UnityEngine.Random.Range(0, 100), colors.Colors[(int)category])).ToArray();

							return Observable.Zip(observables);
						})
						.Do(__ =>
						{
							_audioSource.Stop();
						})
						.Delay(TimeSpan.FromSeconds(0.5))
						.AsUnitObservable()
				);

				_legendsText.text += "<color=#" + ColorUtility.ToHtmlStringRGB(colors.Colors[(int)dataCategory]) + ">・" + Environment.DataCategoryNames[dataCategory] + "</color>\n";
			}
			obs.Subscribe(_ =>
			{
				int win = 0;
				for (int i = 0; i < 4; i++)
				{
					if (_bars[win].Score < _bars[i].Score) win = i;
				}

				_winsText.text = (win + 1) + "P WINS!!";
				_animator.SetTrigger("Wins");

				_audioSource.PlayOneShot(_applauseClip);

				Observable.Timer(TimeSpan.FromSeconds(1.5)).Subscribe(__ =>
				{
					_canNext = true;
				});
			});

			_canNext = false;

			_audioSource = GetComponent<AudioSource>();

			// デバッグ用
			for (int i = 0; i < 4; i++)
			{
				var param = new List<string>() { "Result", "Player " + i };
				param.AddRange(Environment.PlayerDatas[i].Select(kv => kv.Key.ToString() + " " + kv.Value));
				// Logger.Push(param.ToArray());

				var score = Environment.PlayerDatas[i].Select((kv) => kv.Value * Environment.DataCategoryCoefficients[kv.Key])
					.Sum();
				// Logger.Push("Total Score", score.ToString());
			}
		}

		// Update is called once per frame
		void Update()
		{
			if (_canNext)
			{
				foreach (var inputDevice in MappedInput.InputDevices)
				{
					if (inputDevice.GetButtonDown(MappedButton.Start))
					{
						OnNext.Invoke();
						SceneManager.LoadScene("game_over");
					}
				}
			}
		}
	}
}