﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ProjectGold;
using ProjectGold.Character;
using UniRx;
using UniRx.Triggers;
using BehaviourTree;

public class AurelionSolAI_1 : CharacterAI {

	[SerializeField]
	private GameObject breathSearch;
	[SerializeField]
	private GameObject stumpSearch;

	private GameObject breathTargetCharacter;
	private List<GameObject> breathTargetCoins;
	private GameObject breathTargetZone;
	private GameObject breathTarget;
	private GameObject stumpTarget;

	private bool isBreathCharge;
	private float remainingTimeAtStart;

	// Use this for initialization
	void Start () {
		base.Start();
		behaviour = GetComponentInParent<AurelionSolBehaviour>();
		breathTargetCoins = new List<GameObject>();
		breathTarget = null;
		remainingTimeAtStart = 0.0f;

		breathSearch.OnTriggerEnterAsObservable()
			.Where(col => col.gameObject.tag == "Character" || col.gameObject.tag == "Gold" || col.gameObject == myZone.gameObject)
			.Subscribe(col =>
			{
				if(col.gameObject.tag == "Character"){
					breathTargetCharacter = col.gameObject;
				}
				else if(col.gameObject.tag == "Gold"){
					breathTargetCoins.Add(col.gameObject);
				}
				else {
					breathTargetZone = col.gameObject;
				}
			});
		breathSearch.OnTriggerExitAsObservable()
			.Where(col => (breathTargetCharacter != null && breathTargetCharacter == col.gameObject)
						   || breathTargetCoins.Contains(col.gameObject)
						   || (breathTargetZone != null && breathTargetZone == col.gameObject))
			.Subscribe(col =>
			{
				if(col.gameObject.tag == "Character"){
					breathTargetCharacter = null;
				}
				else if(col.gameObject.tag == "Gold"){
					breathTargetCoins.Remove(col.gameObject);
				}
				else {
					breathTargetZone = null;
				}
			});
		stumpSearch.OnTriggerEnterAsObservable()
			.Where(col => col.gameObject.tag == "Character")
			.Subscribe(col =>
			{
				stumpTarget = col.gameObject;
			});
		stumpSearch.OnTriggerExitAsObservable()
			.Where(col => stumpTarget != null && stumpTarget == col.gameObject)
			.Subscribe(col =>
			{
				stumpTarget = null;
			});

		rootNode = new SequencerNode(new List<NodeBase>
        {
            //new ActionNode(Think),
            new SelectorNode(new List<NodeBase>
            {
            	// いつでも
            	new DecoratorNode(IsBreathReleaseTiming, new ActionNode(ReleaseBreath)),
            	new DecoratorNode(IsBreathCharge, new SelectorNode(new List<NodeBase>
            	{ 
            		new ActionNode(MoveBreathTarget),
            		new SequencerNode(new List<NodeBase>
            		{
            			new ActionNode(BreathCharge),
            			new SelectorNode(new List<NodeBase>
            			{
							new DecoratorNode(IsGrabbingCoin, new SelectorNode(new List<NodeBase>{
				    			new DecoratorNode(IsOnHomeWithCoin, new ActionNode(ReleaseCoin)),
								new ActionNode(BackHome)
							})),
							commonNode
						}),
            		})
            	})),
            	new DecoratorNode(IsStumpTarget, new ActionNode(Stump)),
            	new DecoratorNode(InTheSky, new SelectorNode(new List<NodeBase>
            	{ 
            		new DecoratorNode(IsGrabbingCoin, new ActionNode(BackHome)),
            		new ActionNode(MoveStumpTarget)
            	})),
            	new DecoratorNode(IsBreathTarget, new ActionNode(BreathCharge)),
            	// コインを掴んでいるとき
				new DecoratorNode(IsGrabbingCoin, new SelectorNode(new List<NodeBase>{
		    		new DecoratorNode(IsOnHomeWithCoin, new ActionNode(ReleaseCoin)),
					new ActionNode(BackHome)
		    	})),
				// 掴んでない時
		    	commonNode
            })
        });
	}

	// Actions
	bool BreathCharge ()
	{
		if (behaviour.CooldownSessions [0].IsCool && remainingTimeAtStart >= 5.0f) {
			Input.SetButton(MappedButton.Skill1);
			return true;
		}
		return false;
	}
	bool MoveBreathTarget ()
	{
		if (breathTarget != null) {
			var v = breathTarget.transform.position - behaviour.transform.position;
			Input.SetAxis(new Vector2(v.x, v.z));
			Input.SetButton(MappedButton.Skill1);
			return true;
		}
		return false;
	}
	bool ReleaseBreath ()
	{
		if (breathTarget != null && breathTarget.tag == "Gold" && IsGrabbingCoin()) {
			Input.SetButton(MappedButton.Grab);
		}
		return true;
	}
	bool Stump ()
	{	
		if(isBreathCharge) return false;
		if (behaviour.CooldownSessions [1].IsCool) {
			Input.SetButton(MappedButton.Skill2);
			return true;
		}
		return false;
	}
	bool MoveStumpTarget()
	{
		if (stumpTarget != null) {
			var v = stumpTarget.transform.position - behaviour.transform.position;
			Input.SetAxis(new Vector2(v.x, v.z));
			return true;
		}
		return false;
	}

	// Conditions
	bool IsBreathTarget()
	{
		if(breathTarget != null) return true;
		return false;
	}
	bool IsBreathTargetCharacter ()
	{
		if(breathTargetCharacter != null) return true;
		return false;
	}
	bool IsBreathCoinTarget()
	{
		if(breathTargetCoins.Count > 0) return true;
		else return false;
	}
	bool IsBreathReachMyZone()
	{
		if(breathTargetZone != null) return true;
		return false;
	}
	bool IsBreathCharge()
	{
		return isBreathCharge;
	}
	bool IsBreathReleaseTiming ()
	{
		if (isBreathCharge) {
			if(((AurelionSolBehaviour)behaviour).BreathIndicator.activeSelf == false) return false;
			var breathLength = ((AurelionSolBehaviour)behaviour).BreathIndicator.transform.localScale.z;
			var breathMaxLength = ((AurelionSolBehaviour)behaviour).BreathDistance;
			if (breathLength >= breathMaxLength) {
				return true;
			}

		}
		return false;
	}

	bool IsStumpTarget ()
	{
		if(stumpTarget != null) return true;
		return false;
	}
	bool InTheSky()
	{
		if(behaviour.HasEffect("Float")) return true;
		return false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (IsBreathCoinTarget () && IsBreathReachMyZone ()) {
			breathTarget = breathTargetCoins.First ();
		} else if (IsBreathTargetCharacter () && breathTargetCoins.Count < 3) {
			breathTarget = breathTargetCharacter;
		} else {
			breathTarget = null;
		}
		if (remainingTimeAtStart < 5.0f) {
			remainingTimeAtStart += Time.deltaTime;
		}
		base.Update ();
		isBreathCharge = Input.Button[MappedButton.Skill1];
	}
}
