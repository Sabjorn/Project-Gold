﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace ProjectGold.Character
{
	public class AurelionSolBehaviour : CharacterBaseBehaviour
	{
		[Header("ファイアブレス")]
		public float BreathCooldown;
		public float BreathDistance;
		public float BreathWidth;
		public float BreathChargeDuration;
		public float BreathForce;
		public float BreathStunDuration;
		public float BreathPauseDuration;

		public ParticleSystem BreathParticle;
		[SerializeField] private ParticleSystem ChargeParticle;
		[SerializeField] private AnimationCurve BreathSpeedCurve;

		[SerializeField] private GameObject BreathTrigger;
		[SerializeField] public GameObject BreathIndicator;

		[Header("飛行")]
		public float FloatCooldown;
		public float StumpRange;
		public float AttractRange;
		public float AttractImpulse;
		public float StumpStunDuration;

		[SerializeField] private GameObject StumpTrigger;
		[SerializeField] private GameObject AttractTrigger;
		[SerializeField] private ParticleSystem FloatParticlePrefab;
		[SerializeField] private ParticleSystem StumpParticlePrefab;
		[SerializeField] private SpriteRenderer _characterRenderer;
		[SerializeField] private Animator _floatAnimator;
		[SerializeField] private AnimationClip _floatClip;

		[Header("効果音")] [SerializeField] private AudioClip _chargeClip;
		[SerializeField]
		private AudioClip BreathClip;
		[SerializeField] private AudioClip FloatClip;
		[SerializeField] private AudioClip StumpClip;


		private float _baseSpeed;

		private Subject<Unit> _releaseSubject;

		private List<GameObject> _stunCandidates;
		private List<GameObject> _attractCandidates;

		new void Awake()
		{
			base.Awake();

			CooldownSessions = new[]
			{
				new CooldownSession(BreathCooldown),
				new CooldownSession(FloatCooldown)
			};
			CooldownSessions[0].InstantHeat(7.0f);// ブレス最初はクールダウンに

			_baseSpeed = Speed;

			_releaseSubject = new Subject<Unit>();

			BreathIndicator.SetActive(false);
			BreathTrigger.SetActive(false);

			BreathTrigger.GetComponent<Animator>().SetTrigger("Breath");

			// スタン多段ヒット対策
			BreathTrigger.OnTriggerEnterAsObservable().Subscribe(col =>
			{
				if (col.gameObject.tag == "Character")
				{
					var character = col.gameObject.GetComponent<CharacterBaseBehaviour>();
					character.Stun(BreathStunDuration);

					_hitSubject.OnNext(Unit.Default);
					// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
				}
			});
			BreathTrigger.OnTriggerStayAsObservable().Subscribe(col =>
			{
				if (col.gameObject.tag == "Character" || col.gameObject.tag == "Gold")
				{
					if (col.gameObject.tag == "Gold")
					{
						col.gameObject.GetComponent<GoldBehaviour>().OccupyTime(this, OccupationTier.Skill, 0.5f);
					}

					var rigidBody = col.gameObject.GetComponent<Rigidbody>();
					rigidBody.AddForce(Direction * BreathForce * Time.deltaTime, ForceMode.VelocityChange);
				}
			});

			_stunCandidates = new List<GameObject>();

			StumpTrigger.OnTriggerEnterAsObservable()
				.Where(col => col.tag == "Character")
				.Subscribe(col =>
				{
					_stunCandidates.Add(col.gameObject);
				});
			StumpTrigger.OnTriggerExitAsObservable()
				.Where(col => col.tag == "Character")
				.Subscribe(col =>
				{
					_stunCandidates.Remove(col.gameObject);
				});

			StumpTrigger.SetActive(false);

			_attractCandidates = new List<GameObject>();
			AttractTrigger.OnTriggerEnterAsObservable()
				.Where(col => col.tag == "Character" || col.tag == "Gold")
				.Subscribe(col =>
				{
					_attractCandidates.Add(col.gameObject);
				});
			AttractTrigger.OnTriggerExitAsObservable()
				.Where(col => col.tag == "Character" || col.tag == "Gold")
				.Subscribe(col =>
				{
					_attractCandidates.Remove(col.gameObject);
				});
			var c = AttractTrigger.GetComponent<Renderer>().material.color;
			c.a = 0.25f;
			AttractTrigger.GetComponent<Renderer>().material.color = c;
			AttractTrigger.SetActive(false);
		}

		protected override void Control()
		{
			base.Control();

			if (IsControllable)
			{
				if (Controller.GetButtonDown(MappedButton.Skill1) && CooldownSessions[0].IsCool)
				{
					Breath();
				}
				if (Controller.GetButtonUp(MappedButton.Skill1))
				{
					_releaseSubject.OnNext(Unit.Default);
				}

				if (Controller.GetButtonDown(MappedButton.Skill2) && CooldownSessions[1].IsCool)
				{
					Float();
					CooldownSessions[1].Heat();
				}
			}
		}

		/// <summary>
		/// ファイアブレスを開始する。
		/// </summary>
		void Breath()
		{
			CompositeDisposable disposables = new CompositeDisposable();

			// 終了処理
			Action finalize = () =>
			{
				BreathIndicator.SetActive(false);
				IsControllable = true;

				ChargeParticle.Stop();

				disposables.Dispose();
			};

			// ファイアブレスのシーケンス
			float charge = 0;
			var breathSequence = Observable.Defer(() =>
				{
					BreathIndicator.SetActive(true);

					ChargeParticle.Play();

					AudioSource.PlayOneShot(_chargeClip);

					return this.LateUpdateAsObservable();
				})
				.Do(_ =>
				{
					charge = Mathf.Min(charge + (Time.deltaTime / BreathChargeDuration), 1);
					BreathIndicator.transform.localScale = new Vector3(BreathWidth, 1, BreathDistance * charge);
				})
				.SkipUntil(this.UpdateAsObservable().Where(_ => !Controller.GetButton(MappedButton.Skill1))).First()
				.Do(_ =>
				{
					IsControllable = false;
					TargetVelocity = Vector3.zero;

					ChargeParticle.Stop();

					BreathIndicator.transform.localScale = new Vector3(BreathWidth, 1, 0);
					BreathIndicator.SetActive(false);

					BreathTrigger.SetActive(true);
					BreathTrigger.transform.localScale = new Vector3(BreathWidth, 1, BreathDistance * charge);
					BreathTrigger.GetComponent<Animator>().SetTrigger("Breath");

					var vel = BreathParticle.velocityOverLifetime;
					vel.z = new ParticleSystem.MinMaxCurve(BreathDistance * charge / 20f * 100f, BreathSpeedCurve);
					var main = BreathParticle.main;
					main.startSize = BreathWidth * 7f / 5f;
					BreathParticle.Play();

					AudioSource.Stop();
					AudioSource.PlayOneShot(BreathClip);

					CooldownSessions[0].Heat();
				})
				.Delay(TimeSpan.FromSeconds(BreathPauseDuration))
				.DoOnCompleted(finalize);

			AddEffect("Breath", new Effect(breathSequence, finalize)
			{
				Tags = new HashSet<EffectTag>()
				{
					EffectTag.SkillCasting
				}
			});
		}

		void Float()
		{
			CompositeDisposable disposables = new CompositeDisposable();

			Action finalize = () =>
			{
				StumpTrigger.SetActive(false);
				AttractTrigger.SetActive(false);

				_characterRenderer.sortingOrder = 0;
				_characterRenderer.sortingLayerName = "Default";

				gameObject.layer = LayerMask.NameToLayer("Team" + TeamNo + " Character");
			};

			var floatSequence = Observable.Defer(() =>
				{
					_stunCandidates.Clear();
					StumpTrigger.transform.localScale = Vector3.one * StumpRange;
					StumpTrigger.SetActive(true);

					_attractCandidates.Clear();
					AttractTrigger.transform.localScale = Vector3.one * AttractRange;
					AttractTrigger.SetActive(true);

					_characterRenderer.sortingOrder = 10;
					_characterRenderer.sortingLayerName = "Air";

					gameObject.layer = LayerMask.NameToLayer("Air");

					_floatAnimator.SetTrigger("Float");

					Utilities.Particle.InstantiateParticle(FloatParticlePrefab, transform.position);

					AudioSource.PlayOneShot(FloatClip);

					return Observable.Timer(TimeSpan.FromSeconds(_floatClip.length));
				})
				.Do(_ =>
				{
					foreach (var candidate in _stunCandidates)
					{
						if (candidate.tag == "Character")
						{
							var character = candidate.GetComponent<CharacterBaseBehaviour>();

							character.Stun(StumpStunDuration);

							_hitSubject.OnNext(Unit.Default);
							// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
						}
					}
					foreach (var candidate in _attractCandidates)
					{
						if(candidate == null || candidate.activeInHierarchy == false){
							continue;
						}
						if (candidate.tag == "Gold")
						{
							candidate.GetComponent<GoldBehaviour>().OccupyTime(this, OccupationTier.Skill, 0.5f);
						}

						var rigidBody = candidate.GetComponent<Rigidbody>();
						rigidBody.AddForce(-(candidate.transform.position - transform.position).normalized * AttractImpulse,
							ForceMode.VelocityChange);
					}
					AudioSource.PlayOneShot(StumpClip);

					Utilities.Particle.InstantiateParticle(StumpParticlePrefab, transform.position);
				})
				.DoOnCompleted(finalize)
				.AsUnitObservable();

			AddEffect("Float", new Effect(floatSequence, finalize)
			{
				Tags = new HashSet<EffectTag>()
				{
					EffectTag.SkillActive
				}
			});
		}
	}
}