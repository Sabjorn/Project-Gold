﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ProjectGold;
using ProjectGold.Character;
using UniRx;
using UniRx.Triggers;
using BehaviourTree;

public class JiggsAI_1 : CharacterAI {

	[SerializeField]
	private GameObject bombSearch;

	private GameObject bombTarget;

	private bool isBombCharge;
	private float bombChargeTime = -1.0f;

	// Use this for initialization
	void Start () {
		base.Start();
		behaviour = GetComponentInParent<JiggsBehaviour>();

		bombSearch.OnTriggerEnterAsObservable()
			.Where(col => col.gameObject.tag == "Character")
			.Subscribe(col =>
			{
				bombTarget = col.gameObject;
			});
		bombSearch.OnTriggerExitAsObservable()
			.Where(col => bombTarget != null && bombTarget == col.gameObject)
			.Subscribe(col =>
			{
				bombTarget = null;
			});

		rootNode = new SequencerNode(new List<NodeBase>
        {
            //new ActionNode(Think),
            new SelectorNode(new List<NodeBase>
            {
            	// いつでも
            	new DecoratorNode( IsBombFullCharge, new DecoratorNode(IsBombTarget, new ActionNode(BombRelease))),
            	new DecoratorNode(IsBombCharge, new ActionNode(BombCharge)),
            	new DecoratorNode( () => !IsBombCharge(), new ActionNode(BombCharge)),
            	// コインを掴んでいるとき
				new DecoratorNode(IsGrabbingCoin, new SelectorNode(new List<NodeBase>{
            		new ActionNode(Smoke),
		    		new DecoratorNode(IsOnHomeWithCoin, new ActionNode(ReleaseCoin)),
					new ActionNode(BackHome)
		    	})),
				// 掴んでない時
		    	commonNode
            })
        });
	}

	// Actions
	bool BombCharge ()
	{
		if (isBombCharge == false && behaviour.CooldownSessions [0].IsCool) {
			bombChargeTime = 0.0f;
			Input.SetButton (MappedButton.Skill1);
			return true;
		}
		if (isBombCharge) {
			Input.SetButton(MappedButton.Skill1);
		}
		return false;
	}
	bool BombRelease ()
	{
		return true;
	}
	bool Smoke ()
	{
		if (behaviour.CooldownSessions [1].IsCool) {
			Input.SetButton(MappedButton.Skill2);
			return true;
		}
		return false;
	}

	// Conditions
	bool IsBombCharge(){
		return isBombCharge;
	}
	bool IsBombFullCharge ()
	{
		if (isBombCharge && bombChargeTime > 1.0f) {
			return true;
		}
		return false;
	}
	bool IsBombTarget()
	{
		if(bombTarget != null)return true;
		return false;
	}

	// Update is called once per frame
	void Update ()
	{
		base.Update ();
		isBombCharge = Input.Button[MappedButton.Skill1];
		if (isBombCharge) {
			bombChargeTime += Time.deltaTime;
		}
	}
}
