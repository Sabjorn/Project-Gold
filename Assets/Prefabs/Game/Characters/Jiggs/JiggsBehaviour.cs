﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace ProjectGold.Character
{

	/// <summary>
	/// ジグス。
	/// ・爆弾を投げる。当たったキャラはスタン。
	/// ・煙幕を放つ。見えない。
	/// </summary>
	public class JiggsBehaviour : CharacterBaseBehaviour
	{
		/// <summary>
		/// 爆弾投げのクールダウン時間。sec。
		/// </summary>
		[Header("爆弾投げ")]
		public float BombCooldown;

		/// <summary>
		/// 爆弾の最大射程。unit。
		/// </summary>
		public float BombMaxDistance;

		/// <summary>
		/// 爆弾の最小射程。unit。
		/// </summary>
		public float BombMinDistance;

		/// <summary>
		/// 爆弾投げインジケータの速度。unit/sec。
		/// </summary>
		public float BombIndicatorSpeed;

		/// <summary>
		/// 爆弾の速度。unit/sec。
		/// </summary>
		public float BombSpeed;

		/// <summary>
		/// 爆弾での吹っ飛び速度。unit/sec。
		/// </summary>
		public float BombImpact;

		/// <summary>
		/// 爆弾での吹っ飛び範囲。unit。
		/// </summary>
		public float BombImpactRange;

		/// <summary>
		/// 爆弾でのスタン時間。sec。
		/// </summary>
		public float BombStunDuration;

		/// <summary>
		/// 煙幕のクールダウン時間。sec。
		/// </summary>
		[Header("偽コイン")]
		public float FakeCoinCooldown;
		[SerializeField]
		private GameObject FakeCoinPrefab;
		public GameObject myFakeCoin = null;

		[SerializeField]
		private GameObject BombPrefab;

		[SerializeField]
		private GameObject ReticleObject;
		[SerializeField]
		private GameObject BombRangeIndicator;

		[SerializeField] private GameObject BombImpactPointPrefab;

		[SerializeField] private ParticleSystem ExplosionParticle;
		[SerializeField] private ParticleSystem SmokeParticle;
		[SerializeField] private ParticleSystem BombSparkleParticle;

		[SerializeField] private AudioClip _bombClip, _smokeClip, _throwClip;

		private Subject<Unit> _releaseSubject;

		void Awake()
		{
			base.Awake();

			CooldownSessions = new CooldownSession[]
			{
				new CooldownSession(BombCooldown),
				new CooldownSession(FakeCoinCooldown)
			};

			ReticleObject.SetActive(false);
			BombRangeIndicator.SetActive(false);

			var c = BombRangeIndicator.GetComponent<Renderer>().material.color;
			c.a = 0.2f;
			BombRangeIndicator.GetComponent<Renderer>().material.color = c;

			_releaseSubject = new Subject<Unit>();
		}

		protected override void Control()
		{
			base.Control();

			if (IsControllable)
			{
				if (Controller.GetButtonDown(MappedButton.Skill1) && CooldownSessions[0].IsCool)
				{
					Throw();
				}
				if (Controller.GetButtonUp(MappedButton.Skill1))
				{
					ReleaseBomb();
				}
				if (Controller.GetButtonDown(MappedButton.Skill2) && CooldownSessions[1].IsCool)
				{
					CooldownSessions[1].Heat();
					Smoke();
				}
			}
		}

		/// <summary>
		/// 爆弾投げを開始する。
		/// </summary>
		public void Throw()
		{
			ReticleObject.SetActive(true);
			ReticleObject.transform.localPosition = Vector3.zero;
			ReticleObject.transform.localScale = Vector3.one;// * BombImpactRange;
			ReticleObject.transform.localPosition = Vector3.forward * BombMinDistance;
			//BombRangeIndicator.SetActive(true);
			BombRangeIndicator.transform.localScale = Vector3.one * BombMaxDistance;

			CompositeDisposable disposables = new CompositeDisposable();

			var desiredRange = BombMinDistance;
			var chargeSequence = this.UpdateAsObservable()
				.Do(_ =>
				{
					desiredRange += BombIndicatorSpeed * Time.deltaTime;
					desiredRange = Mathf.Min(desiredRange, BombMaxDistance);

					ReticleObject.transform.localPosition = Vector3.forward * desiredRange;
					ReticleObject.GetComponent<LineRenderer>().SetPosition(1, ReticleObject.transform.InverseTransformPoint(transform.position));
				})
				.TakeUntil(this.UpdateAsObservable().Where(_ => !Controller.GetButton(MappedButton.Skill1)))
				.Concat(Observable.Defer(() =>
				{
					CooldownSessions[0].Heat();

					AudioSource.PlayOneShot(_throwClip);

					ReticleObject.SetActive(false);
					BombRangeIndicator.SetActive(false);

					// var dir = Direction;
					var dir = PivotTransform.rotation * Vector3.forward;

					var bomb = Instantiate(BombPrefab);
					bomb.transform.position = transform.position + dir * 1.5f;
					var rotation = bomb.transform.rotation;
					rotation.SetLookRotation(dir);
					bomb.transform.rotation = rotation;

					var hitSphere = bomb.transform.GetChild(0).gameObject;
					hitSphere.GetComponent<SphereCollider>().radius = BombImpactRange;
					hitSphere.layer = LayerMask.NameToLayer("OmniHit");

					var candidates = new List<GameObject>();
					var hitEnter = hitSphere.OnTriggerEnterAsObservable()
						.Where(col => col.tag == "Character" || col.tag == "Gold")
						.Subscribe(col =>
						{
							candidates.Add(col.gameObject);
						})
						.AddTo(hitSphere);
					disposables.Add(hitEnter);
					var hitExit = hitSphere.OnTriggerExitAsObservable()
						.Where(col => col.tag == "Character" || col.tag == "Gold")
						.Subscribe(col =>
						{
							candidates.Remove(col.gameObject);
						})
						.AddTo(hitSphere);
					disposables.Add(hitExit);

					var sparkle = Utilities.Particle.InstantiateParticle(BombSparkleParticle, bomb.transform.position);

					var bombImpactPoint = Instantiate(BombImpactPointPrefab, transform.position + dir * desiredRange, Quaternion.identity);
					bombImpactPoint.transform.localScale = Vector3.one * BombImpactRange;

					bomb.UpdateAsObservable()
						.Do(_ =>
						{
							bomb.transform.position += dir * Time.deltaTime * BombSpeed;
							rotation = bomb.transform.rotation;
							rotation.SetLookRotation(dir);
							bomb.transform.rotation = rotation;

							sparkle.transform.position = bomb.transform.position;
						})
						.TakeUntil(Observable.Amb(
							Observable.Timer(TimeSpan.FromSeconds(desiredRange / BombSpeed)).AsUnitObservable(),
							bomb.OnTriggerStayAsObservable().Where(col => col.gameObject.tag == "Boundary").AsUnitObservable()
						))
						.Concat(Observable.Defer(() =>
						{
							AudioSource.PlayOneShot(_bombClip);

							foreach (var candidate in candidates)
							{
								if (candidate == null || candidate.activeInHierarchy == false)
								{
									continue;
								}
								if (candidate.tag == "Character")
								{
									candidate.GetComponent<CharacterBaseBehaviour>().Stun(BombStunDuration);

									_hitSubject.OnNext(Unit.Default);
									// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
								}
								else
								{
									candidate.GetComponent<GoldBehaviour>().OccupyTime(this, OccupationTier.Skill, 0.5f);
								}
								candidate.GetComponent<Rigidbody>()
									.AddForce((candidate.transform.position - bomb.transform.position).normalized * BombImpact,
										ForceMode.VelocityChange);
							}

							Utilities.Particle.InstantiateParticle(ExplosionParticle, bomb.transform.position);

							sparkle.Stop(true);

							Destroy(bomb);
							Destroy(bombImpactPoint);

							return Observable.ReturnUnit();
						}))
						.Subscribe()
						.AddTo(bomb);

					return Observable.ReturnUnit();
				}));

			AddEffect("Bomb", new Effect(chargeSequence, () =>
			{
				ReticleObject.SetActive(false);
				BombRangeIndicator.SetActive(false);

				disposables.Dispose();
			})
			{
				Tags = new HashSet<EffectTag>() { EffectTag.SkillCasting }
			});
		}
		public void ReleaseBomb()
		{
			_releaseSubject.OnNext(Unit.Default);
		}

		public void Smoke()
		{
			AudioSource.PlayOneShot(_smokeClip);


			var smoke = Utilities.Particle.InstantiateParticle(SmokeParticle);
			smoke.transform.position = transform.position + Direction * -3.0f;
			smoke.Play();
			Observable.Timer(TimeSpan.FromSeconds(2.5)).Subscribe(_ => Destroy(smoke));

			if (myFakeCoin != null && myFakeCoin.activeInHierarchy)
			{
				CharacterBaseBehaviour grabCharacter = null;
				if (myFakeCoin == Grabbing)
				{
					grabCharacter = this;
				}
				myFakeCoin.GetComponent<FakeGoldBehaviour>().Vanish(grabCharacter);
				myFakeCoin = null;
			}
			myFakeCoin = Instantiate(FakeCoinPrefab);
			myFakeCoin.transform.position = transform.position + Direction * -3.0f;
		}
	}
}