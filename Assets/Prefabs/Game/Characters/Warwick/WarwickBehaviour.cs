﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using TMPro;

namespace ProjectGold.Character
{
	public class WarwickBehaviour : CharacterBaseBehaviour
	{
		[SerializeField] private Animator _warwickAnimator;

		[Header("引っかき")]
		public float ClawCooldown;
		public float ClawMinInterval;
		public float ClawMaxInterval;
		public float ClawDashSpeed;
		public float ClawDashAccScale;
		public AnimationCurve ClawDashCurve;
		public float ClawImpact;
		public float ClawStunDuration;
		[SerializeField] private GameObject _clawObject;

		[Header("咆哮")]
		public float HowlCooldown;
		public float HowlRange;
		public float HowlStunDuration;
		[SerializeField] private GameObject _howlObject;
		[SerializeField] private ParticleSystem _howlParticle;

		[Header("変身")]
		public int MaxStack;
		public float InsanityDuration;
		[SerializeField] private ParticleSystem _insanityParticle;
		[SerializeField] private ParticleSystem _insanityInitialParticle;
		[SerializeField] private GameObject _stackIconPrefab;
		[SerializeField] private Transform _stackIconsTransform;
		[SerializeField] private GameObject _insanityRemainingGauge;

		private Subject<Unit> _pressSkill1Subject;

		private int _stack;

		private bool _isInsane;
		public bool IsInsane { get { return _isInsane; } }
		private float _insanityRemaining;

		private List<GameObject> _stackObjects;

		private float _baseAcceleration;

		[SerializeField] TextMeshPro[] buttonTexts;

		[SerializeField] private AudioClip _howlClip, _insanityClip, _clawHitClip;
		[SerializeField] private AudioClip[] _clawClips;

		protected new void Awake()
		{
			base.Awake();

			CooldownSessions = new CooldownSession[] {
				new CooldownSession (ClawCooldown),
				new CooldownSession (HowlCooldown)
			};

			_clawObject.SetActive(false);
			_clawObject.GetComponentInChildren<Collider>().gameObject.layer = LayerMask.NameToLayer("Team" + TeamNo + " Hit");
			_clawObject.GetComponent<Renderer>().sortingLayerName = "Front Effect";

			_clawObject.transform.GetChild(0).OnTriggerEnterAsObservable()
				.Where(col => col.tag == "Character" && col.gameObject != gameObject)
				.Subscribe(col =>
				{
					var charBehaviour = col.GetComponent<CharacterBaseBehaviour>();

					charBehaviour.RigidBody.AddForce(Direction * ClawImpact, ForceMode.VelocityChange);

					Utilities.Particle.InstantiateParticle(CommonParticles.HitParticle, col.transform.position);

					charBehaviour.Stun(ClawStunDuration);

					AudioSource.PlayOneShot(_clawHitClip);

					if (!_isInsane)
						_stack++;

					_hitSubject.OnNext(Unit.Default);
					// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
				});

			_howlObject.SetActive(false);
			_howlObject.transform.localScale = Vector3.one * HowlRange;
			_howlObject.layer = LayerMask.NameToLayer("Team" + TeamNo + " Hit");
			_howlObject.OnTriggerEnterAsObservable()
				.Where(col => col.tag == "Character" && col.gameObject != gameObject)
				.Subscribe(col =>
				{
					var charBehaviour = col.GetComponent<CharacterBaseBehaviour>();

					charBehaviour.Stun(HowlStunDuration);

					_hitSubject.OnNext(Unit.Default);
					// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
				});

			_pressSkill1Subject = new Subject<Unit>();

			_stack = 0;
			_isInsane = false;

			_stackObjects = new List<GameObject>();

			_baseAcceleration = Acceleration;

			this.ObserveEveryValueChanged(c => c.isAI)
			.DistinctUntilChanged()
			.Subscribe(b =>
			{
				foreach (var text in buttonTexts)
				{
					text.gameObject.SetActive(!b);
				}
			});
			foreach (var text in buttonTexts)
			{
				text.enabled = false;
			}

			_insanityRemainingGauge.SetActive(false);
		}

		protected new void Update()
		{
			base.Update();

			if (!_isInsane)
			{
				if (_stack >= MaxStack)
				{
					Insanity();
				}
			}

			// スタン時ボタン表示
			if (HasEffect("Stun"))
			{
				buttonTexts[0].enabled = false;
			}
			buttonTexts[1].enabled = CooldownSessions[1].IsCool && HasEffect("Stun");



			// 激昂スタック表示
			int iconNum = 0;
			if (_isInsane) iconNum = Mathf.CeilToInt(MaxStack * _insanityRemaining / InsanityDuration);
			else iconNum = _stack;
			for (int i = 0; i < iconNum || i < _stackObjects.Count; i++)
			{
				if (_stackObjects.Count <= i)
				{
					_stackObjects.Add(Instantiate(_stackIconPrefab, _stackIconsTransform));
				}

				var obj = _stackObjects[i];
				var p = Vector3.zero + Vector3.right * (i - (iconNum - 1) / 2f);

				if (i < iconNum)
				{
					obj.SetActive(true);
					obj.transform.localPosition = p;
				}
				else
				{
					obj.SetActive(false);
				}
			}

			// 応急処置
			if (_clawObject.GetComponentInChildren<Collider>() != null)
				_clawObject.GetComponentInChildren<Collider>().gameObject.layer = LayerMask.NameToLayer("Team" + TeamNo + " Hit");
		}

		protected override void Control()
		{
			base.Control();

			if (IsControllable)
			{
				if (Controller.GetButtonDown(MappedButton.Skill1))
					_pressSkill1Subject.OnNext(Unit.Default);

				if (Controller.GetButtonDown(MappedButton.Skill1) && CooldownSessions[0].IsCool)
				{
					CooldownSessions[0].Heat();

					Claw();
				}
			}
			if (Controller.GetButtonDown(MappedButton.Skill2) && CooldownSessions[1].IsCool && HasEffect("Stun"))
			{
				CooldownSessions[1].Heat();

				Howl();
			}
		}

		void Claw()
		{

			Action finish = () =>
			{
				IsControllable = true;

				TargetVelocity = Vector3.zero;
				Acceleration = _baseAcceleration;

				buttonTexts[0].enabled = true;
			};

			float t = 0;

			// 1段目
			var clawSequence1 = Observable.Defer(() =>
				{
					IsControllable = false;

					_warwickAnimator.SetTrigger("Claw 1");

					TargetVelocity = Vector3.zero;
					Acceleration *= ClawDashAccScale;

					t = 0;

					AudioSource.PlayOneShot(_clawClips[0]);

					return this.FixedUpdateAsObservable();
				})
				.Do(_ =>
				{
					TargetVelocity = Direction * ClawDashSpeed * ClawDashCurve.Evaluate(Mathf.InverseLerp(0, ClawMinInterval, t));
					t += Time.deltaTime;
				})
				.SkipUntil(Observable.Timer(TimeSpan.FromSeconds(ClawMinInterval))).First()
				.Do(_ => finish());

			// 2段目
			var clawSequence2 = Observable.Defer(() =>
				{
					IsControllable = false;

					_warwickAnimator.SetTrigger("Claw 2");

					// TargetVelocity = Vector3.zero;
					Acceleration *= ClawDashAccScale;

					t = 0;

					AudioSource.PlayOneShot(_clawClips[1]);

					return this.FixedUpdateAsObservable();
				})
				.Do(_ =>
				{
					TargetVelocity = Direction * ClawDashSpeed * ClawDashCurve.Evaluate(Mathf.InverseLerp(0, ClawMinInterval, t));
					t += Time.deltaTime;
				})
				.SkipUntil(Observable.Timer(TimeSpan.FromSeconds(ClawMinInterval))).First()
				.Do(_ => finish());

			// 3段目
			var clawSequence3 = Observable.Defer(() =>
				{
					IsControllable = false;

					_warwickAnimator.SetTrigger("Claw 3");

					// TargetVelocity = Vector3.zero;
					Acceleration *= ClawDashAccScale;

					t = 0;

					AudioSource.PlayOneShot(_clawClips[2]);

					return this.FixedUpdateAsObservable();
				})
				.Do(_ =>
				{
					TargetVelocity = Direction * ClawDashSpeed * ClawDashCurve.Evaluate(Mathf.InverseLerp(0, ClawMinInterval, t));
					t += Time.deltaTime;
				})
				.SkipUntil(Observable.Timer(TimeSpan.FromSeconds(ClawMinInterval))).First()
				.Do(_ => finish());

			var seq = clawSequence1
				.SelectMany(_ => Observable.Merge(
						Observable.Timer(TimeSpan.FromSeconds(ClawMaxInterval)).Select(__ => 0),
						_pressSkill1Subject.Select(__ => 1)
					)
					.First()
				)
				.SelectMany(type =>
				{
					switch (type)
					{
						case 0:
							// 時間経過
							return Observable.Empty<Unit>();
						case 1:
							// 再度引っかき
							return clawSequence2;
						default:
							return Observable.Empty<Unit>();
					}
				})
				.Where(_ => _isInsane)
				.SelectMany(_ => Observable.Merge(
						Observable.Timer(TimeSpan.FromSeconds(ClawMaxInterval)).Select(__ => 0),
						_pressSkill1Subject.Select(__ => 1)
					)
					.First()
				)
				.SelectMany(type =>
				{
					switch (type)
					{
						case 0:
							// 時間経過
							return Observable.Empty<Unit>();
						case 1:
							// 再度引っかき
							return clawSequence3;
						default:
							return Observable.Empty<Unit>();
					}
				})
				.DoOnCompleted(() =>
				{
					CooldownSessions[0].Heat();
					buttonTexts[0].enabled = false;
				});

			AddEffect("Claw", new Effect(seq, finish)
			{
				Tags = new HashSet<EffectTag>()
				{
					EffectTag.SkillActive
				}
			});
		}

		void Howl()
		{
			// パーティクル
			var particle = Utilities.Particle.InstantiateParticle(_howlParticle);
			particle.gameObject.transform.SetParent(transform, false);

			// デバフ解消
			CancelEffect(EffectTag.Debuf);

			// 音声再生
			AudioSource.PlayOneShot(_howlClip);

			// スタック追加
			if (!_isInsane) _stack++;

			// 変身時効果
			if (_isInsane)
			{
				_howlObject.SetActive(true);

				var sequence = Observable.Timer(TimeSpan.FromSeconds(0.2f))
					.Do(_ =>
					{
						_howlObject.SetActive(false);
					})
					.AsUnitObservable();

				AddEffect("Howl", new Effect(sequence, () =>
				{
					_howlObject.SetActive(false);
				})
				{
					Tags = new HashSet<EffectTag>()
					{
						EffectTag.SkillActive
					}
				});
			}
		}

		void Insanity()
		{
			_isInsane = true;
			ParticleSystem insanityParticle = null;

			AudioSource.PlayOneShot(_insanityClip);

			// CD解消
			CooldownSessions[0].RemainingTime = 0;
			//CooldownSessions[1].RemainingTime = 0;

			_stackIconsTransform.gameObject.SetActive(false);
			_insanityRemainingGauge.SetActive(true);

			// 変身時の効果
			var sequence = Observable.Defer(() =>
				{
					Utilities.Particle.InstantiateParticle(_insanityInitialParticle, transform.position);

					insanityParticle = Utilities.Particle.InstantiateParticle(_insanityParticle, transform.position);
					insanityParticle.transform.SetParent(transform, true);

					insanityParticle.Clear(true);

					_insanityRemaining = InsanityDuration;

					_spriteAnimator.SetBool("Insanity", true);

					return this.UpdateAsObservable();
				})
				.Do(_ =>
				{
					_insanityRemaining -= Time.deltaTime;

					_insanityRemainingGauge.GetComponent<Renderer>().material.SetFloat("_Cutoff", 1 - _insanityRemaining / InsanityDuration);
				})
				.TakeUntil(this.UpdateAsObservable().Where(_ => _insanityRemaining <= 0).First())
				.DoOnCompleted(() =>
				{
					_stack = 0;
					_isInsane = false;
					_insanityRemaining = 0;
					_spriteAnimator.SetBool("Insanity", false);
					_stackIconsTransform.gameObject.SetActive(true);
					_insanityRemainingGauge.SetActive(false);
					insanityParticle.Stop(true);
				})
				.AsUnitObservable();

			AddEffect("Insanity", new Effect(sequence, () =>
			{
				_stack = 0;
				_isInsane = false;
				_spriteAnimator.SetBool("Insanity", false);
				_stackIconsTransform.gameObject.SetActive(true);
				_insanityRemainingGauge.SetActive(false);
				insanityParticle.Stop(true);
			})
			{
				Tags = new HashSet<EffectTag>()
				{
					EffectTag.SkillIntrinsic
				}
			});
		}
	}
}