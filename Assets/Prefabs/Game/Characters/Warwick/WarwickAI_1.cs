﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ProjectGold;
using ProjectGold.Character;
using UniRx;
using UniRx.Triggers;
using BehaviourTree;

public class WarwickAI_1 : CharacterAI {

	[SerializeField]
	private GameObject crawSearch;

	private GameObject crawTarget;

	private int crawCount;
	private float lastCrawErapsedTime;

	// Use this for initialization
	void Start () {
		base.Start();
		behaviour = GetComponentInParent<WarwickBehaviour>();
		crawCount = 0;
		lastCrawErapsedTime = 0.0f;

		crawSearch.OnTriggerEnterAsObservable()
			.Where(col => col.gameObject.tag == "Character")
			.Subscribe(col =>
			{
				crawTarget = col.gameObject;
			});
		crawSearch.OnTriggerExitAsObservable()
			.Where(col => crawTarget != null && crawTarget == col.gameObject)
			.Subscribe(col =>
			{
				crawTarget = null;
			});

		rootNode = new SequencerNode(new List<NodeBase>
        {
            //new ActionNode(Think),
            new SelectorNode(new List<NodeBase>
            {
            	// いつでも
            	new DecoratorNode(IsHowlTiming, new ActionNode(Howl)),
            	new DecoratorNode(IsCrawTiming, new SequencerNode(new List<NodeBase>
            	{ 
            		new ActionNode(MoveCrawTarget),
            		new ActionNode(Craw)
            	})),
            	// コインを掴んでいるとき
				new DecoratorNode(IsGrabbingCoin, new SelectorNode(new List<NodeBase>{
		    		new DecoratorNode(IsOnHomeWithCoin, new ActionNode(ReleaseCoin)),
					new ActionNode(BackHome)
		    	})),
				// 掴んでない時
		    	commonNode
            })
        });
	}

	// Actions
	bool Craw ()
	{
		if (behaviour.CooldownSessions [0].IsCool || crawCount > 0) {
			Input.SetButton(MappedButton.Skill1);
			crawCount++;
			lastCrawErapsedTime = 0.0f;
			return true;
		}
		return false;
	}
	bool Howl ()
	{	
		if (behaviour.CooldownSessions [1].IsCool) {
			Input.SetButton(MappedButton.Skill2);
			return true;
		}
		return false;
	}
	bool MoveCrawTarget ()
	{
		if (crawTarget != null) {
			var v = crawTarget.transform.position - this.transform.position;
			Input.SetAxis(new Vector2(v.x, v.z));
			return true;
		}
		return false;
	}

	// Conditions
	bool IsCrawTarget ()
	{
		if(crawTarget != null) return true;
		return false;
	}
	bool IsCrawTiming()
	{
		if(IsCrawTarget())return true;
		if(crawCount > 0) return true;
		return false;
	}

	bool IsHowlTiming ()
	{
		if(behaviour.HasEffect("Stun"))return true;
		return false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		base.Update ();

		if (crawCount > 0) {
			lastCrawErapsedTime += Time.deltaTime;
			int maxCrawCount = 2;
			if(((WarwickBehaviour)behaviour).IsInsane) maxCrawCount = 3;
			if ( crawCount >= maxCrawCount || lastCrawErapsedTime >= ((WarwickBehaviour)behaviour).ClawMaxInterval) {
				crawCount = 0;
			}
		}
	}
}
