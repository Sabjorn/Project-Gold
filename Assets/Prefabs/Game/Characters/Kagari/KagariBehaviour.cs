﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectGold.Character;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace ProjectGold.Character
{
	/// <summary>
	/// カガリ
	/// ・影をその場に設置 再使用で入れ替わり
	/// ・影を爆破
	/// </summary>
	public class KagariBehaviour : CharacterBaseBehaviour
	{
		[Header("影")]
		public float ShadowCooldown;
		[SerializeField] private GameObject _shadowPrefab;
		[SerializeField] private ParticleSystem _shadowSpornParticle;
		private ParticleSystem _shadowSwapParticleInstance;

		[Header("影爆破")]
		public float ShadowBombCooldown;
		public float ShadowBombStunDuration;
		public float ShadowBombInpact;
		[SerializeField] private ParticleSystem _shadowBombStartParticle;
		[SerializeField] private ParticleSystem _shadowBombParticle;
		private GameObject ShadowBombIndicator;
		private bool isShadowBombIgnittion = false;

		[SerializeField] private AudioClip _bombClip,_swapClip,_spawnClip;


		private GameObject _shadowObject;
		public GameObject ShadowObject { get { return _shadowObject; } }
		private List<GameObject> bombCandidates; 

		private float _colliderRadius;

		private Subject<Unit> _releaseSubject;

		private int _stack;
		private List<GameObject> _stackObjects;
		public int Stack { get { return _stack; } }

		new void Awake()
		{
			base.Awake();

			CooldownSessions = new[]
			{
				new CooldownSession(ShadowCooldown),
				new CooldownSession(ShadowBombCooldown)
			};

			_shadowObject = null;
			bombCandidates = new List<GameObject>();

			_releaseSubject = new Subject<Unit>();

			_colliderRadius = GetComponent<CapsuleCollider>().radius;

			_shadowSwapParticleInstance = null;
		}

		new void Update()
		{
			base.Update();

		
		}

		protected override void Control ()
		{
			base.Control ();

			if (IsControllable) {
				if (Controller.GetButtonDown (MappedButton.Skill1) && CooldownSessions [0].IsCool) {
					if (_shadowObject != null) {
						if (isShadowBombIgnittion == false) {
							ShadowSwap ();
							CooldownSessions [0].Heat ();
						}
					} else {
						ShadowSporn ();
						CooldownSessions [0].Heat ();
					}
				}
				if (Controller.GetButtonDown (MappedButton.Skill2) && CooldownSessions [1].IsCool) {
					CooldownSessions[1].Heat();
					if (_shadowObject != null) {
						ShadowBomb();
					}
				}

			}
		}

		void ShadowSporn ()
		{
			if (_shadowObject != null)
				Destroy (_shadowObject);

			//AudioSource.PlayOneShot(_boltShotClip);

			var dir = Direction;

			_shadowObject = Instantiate (_shadowPrefab);
			_shadowObject.transform.position = transform.position;

			var playerNoText = Instantiate (Resources.Load<GameObject> ("Prefabs/Game/Player No Text"), _shadowObject.transform.position,
				                   Quaternion.identity);
			playerNoText.UpdateAsObservable ().Subscribe (_ => {
				playerNoText.transform.position = _shadowObject.transform.position + new Vector3 (0.0f, 1.0f, 0.0f);

				var text = playerNoText.GetComponentInChildren<TextMeshPro> ();
				text.text = (PlayerNo + 1) + "P";
				text.color = PlayerColors.Colors [PlayerNo];
			});
			_shadowObject.OnDestroyAsObservable ()
			.Subscribe (_ => {
				Destroy (playerNoText);
			});

			CompositeDisposable disposables = new CompositeDisposable ();

			var hitSphere = _shadowObject.GetComponentInChildren<SphereCollider> ();
			/*
			foreach (var ray in Physics.SphereCastAll(hitSphere.transform.position, hitSphere.radius, Vector3.zero, 0.0f)) {
				if (ray.collider.gameObject.tag == "Character" || ray.collider.gameObject.tag == "Gold") {
					bombCandidates.Add (ray.collider.gameObject);
				}
			}
			*/
			var hitEnter = hitSphere.OnTriggerEnterAsObservable ()
			.Where (col => col.tag == "Character" || col.tag == "Gold")
			.Subscribe (col => {
				bombCandidates.Add (col.gameObject);
			})
			.AddTo (hitSphere);
			disposables.Add (hitEnter);

			
			var hitExit = hitSphere.OnTriggerExitAsObservable()
			.Where(col => col.tag == "Character" || col.tag == "Gold")
			.Subscribe(col =>
			{
				bombCandidates.Remove(col.gameObject);
			}).AddTo(hitSphere);
			disposables.Add(hitExit);

			ShadowBombIndicator = _shadowObject.GetComponentInChildren<MeshRenderer>().gameObject;
			ShadowBombIndicator.SetActive(false);

			Utilities.Particle.InstantiateParticle(_shadowSpornParticle, _shadowObject.transform.position);

			AudioSource.PlayOneShot(_spawnClip);
			
		}
		void ShadowSwap(){
			var shadowPos = _shadowObject.transform.position;
			_shadowObject.transform.position = transform.position;
			transform.position = shadowPos;
			if(Grabbing != null) Grabbing.transform.position = shadowPos;
			Utilities.Particle.InstantiateParticle(_shadowSpornParticle, transform.position);
			_shadowSwapParticleInstance = Utilities.Particle.InstantiateParticle(_shadowSpornParticle, _shadowObject.transform.position);
			_shadowSwapParticleInstance.gameObject.transform.parent = _shadowObject.transform;

			AudioSource.PlayOneShot(_swapClip);
		}

		void ShadowBomb ()
		{
			Observable.Defer(()=>
			{
				ShadowBombIndicator.SetActive(true);
				Utilities.Particle.InstantiateParticle(_shadowBombStartParticle, _shadowObject.transform.position);
				if(_shadowSwapParticleInstance != null) Destroy(_shadowSwapParticleInstance.gameObject);
				_shadowSwapParticleInstance = null;
				isShadowBombIgnittion = true;
				return Observable.Timer(TimeSpan.FromSeconds(0.5f));
			})
			.Do(_ =>
			{
				isShadowBombIgnittion = false;
				ShadowBombIndicator.SetActive(false);
				Utilities.Particle.InstantiateParticle (_shadowBombParticle, _shadowObject.transform.position);
				foreach (var candidate in bombCandidates) {
					if(candidate == null || candidate.activeInHierarchy == false){
						continue;
					}
					if (candidate.tag == "Character") {
						candidate.GetComponent<CharacterBaseBehaviour>().Stun(ShadowBombStunDuration);
						_hitSubject.OnNext(Unit.Default);
					}else
					{
						candidate.GetComponent<GoldBehaviour>().OccupyTime(this, OccupationTier.Skill, 0.5f);
					}
					candidate.GetComponent<Rigidbody>()
						.AddForce((candidate.transform.position - _shadowObject.transform.position).normalized * ShadowBombInpact,
							ForceMode.VelocityChange);
				}
				bombCandidates.Clear();
				AudioSource.PlayOneShot(_bombClip);
				Destroy(_shadowObject);
			}).Subscribe();
		}


	}
}