﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ProjectGold;
using ProjectGold.Character;
using UniRx;
using UniRx.Triggers;
using BehaviourTree;

public class KagariAI_1 : CharacterAI {

	[SerializeField]private GameObject bombSearch;
	private GameObject bombTarget;

	private GameObject moveTarget;
	private bool LastInputSkill1 = false;

	// Use this for initialization
	void Start () {
		base.Start();
		behaviour = GetComponentInParent<KagariBehaviour>();
		moveTarget = null;

		bombSearch.OnTriggerEnterAsObservable()
			.Where(col => col.gameObject.tag == "Character" && col.gameObject != behaviour.gameObject)
			.Subscribe(col =>
			{
				bombTarget = col.gameObject;
			});
		bombSearch.OnTriggerExitAsObservable()
			.Where(col => bombTarget != null && bombTarget == col.gameObject)
			.Subscribe(col =>
			{
				bombTarget = null;
			});

		rootNode = new SequencerNode(new List<NodeBase>
        {
            //new ActionNode(Think),
            new SelectorNode(new List<NodeBase>
            {
            	// いつでも
				new DecoratorNode(IsNoShadowObject, new ActionNode(ShadowSporn)),
		    	new DecoratorNode(IsShadowObject, new DecoratorNode(IsSwapTiming, new ActionNode(ShadowSwap))),
		    	new DecoratorNode(IsBombTarget, new ActionNode(ShadowBomb)),
            	// コインを掴んでいるとき
				new DecoratorNode(IsGrabbingCoin, new SelectorNode(new List<NodeBase>{
		    		new DecoratorNode(IsOnHomeWithCoin, new ActionNode(ReleaseCoin)),
					new ActionNode(BackHome)
				})),
				// コインを掴んでいないとき
				commonNode
		    })
		});
	}

	// Actions
	bool ShadowSporn ()
	{
		if (behaviour.CooldownSessions [0].IsCool && LastInputSkill1 == false) {
			Input.SetButton(MappedButton.Skill1);
			return true;
		}
		return false;
	}
	bool ShadowSwap ()
	{
		if (behaviour.CooldownSessions [0].IsCool) {
			Input.SetButton(MappedButton.Skill1);
			return true;
		}
		return false;
	}
	bool ShadowBomb ()
	{
		if (behaviour.CooldownSessions [1].IsCool) {
			Input.SetButton(MappedButton.Skill2);
			return true;
		}
		return false;
	}



	// Conditions
	bool IsBombTarget ()
	{
		if(bombTarget != null) return true;
		return false;
	}
	bool IsShadowObject ()
	{
		if(((KagariBehaviour)behaviour).ShadowObject != null) return true;
		return false;
	}
	bool IsNoShadowObject ()
	{
		return !IsShadowObject();
	}
	bool IsSwapTiming ()
	{
		if (moveTarget == null) {
			return false;
		}
		var characterToTarget = Vector3.Distance (moveTarget.transform.position, behaviour.transform.position);
		var shadowToTarget = Vector3.Distance (moveTarget.transform.position, ((KagariBehaviour)behaviour).ShadowObject.transform.position);
		if (characterToTarget - shadowToTarget > 15.0f) {
			return true;
		}
		return false;
	}

	// Update is called once per frame
	void Update ()
	{
		var shadowObject = ((KagariBehaviour)behaviour).ShadowObject;
		if (shadowObject != null) {
			bombSearch.transform.position = shadowObject.transform.position;
		}

		base.Update ();

		if (IsGrabbingCoin ()) {
			moveTarget = myZone.gameObject;
		} else if (thinkData.targetCoin != null) {
			moveTarget = thinkData.targetCoin;
		} else if (thinkData.targetCoin != null) {
			moveTarget = thinkData.redCoin;
		} else {
			moveTarget = null;
		}

		LastInputSkill1 = Input.Button[MappedButton.Skill1] ;

	}
}
