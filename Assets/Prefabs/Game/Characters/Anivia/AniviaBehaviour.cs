﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace ProjectGold.Character
{
	/// <summary>
	/// アニビア。
	/// ・自身の横方向に壁を発生させる。
	/// ・蟻地獄を発生させオブジェクトを引き寄せる。
	/// </summary>
	public class AniviaBehaviour : CharacterBaseBehaviour
	{
		[Header("壁")]
		public float WallCooldown;
		public float WallLength;
		public float WallWidth;
		public float WallDelay;
		public float WallDuration;
		public float WallStunDuration;

		[SerializeField]
		private Transform _wallTransform;

		[SerializeField] private ParticleSystem WallParticlePrefab;
		[SerializeField] private ParticleSystem SandParticlePrefab;

		[Header("蟻地獄")]
		public float BlackholeCooldown;
		public float BlackholeDuration;
		public float BlackholeRadius;
		public float BlackholeForce;
		public float BlackholeSpeedIncrement;

		[SerializeField]
		private Transform _blackholeTransform;

		[SerializeField] private GameObject SpeedEffect;

		[SerializeField] private GameObject WallPrefab;
		[SerializeField] private GameObject WallIndicator;
		[SerializeField] private GameObject BlackholePrefab;
		[SerializeField] private ParticleSystem BlackholeParticle;

		[SerializeField] private AudioClip _wallClip, _blackHoleClip;

		private Subject<Unit> _releaseSubject;

		void Awake()
		{
			base.Awake();

			CooldownSessions = new CooldownSession[]
			{
				new CooldownSession(WallCooldown),
				new CooldownSession(BlackholeCooldown)
			};

			WallIndicator.SetActive(false);

			_releaseSubject = new Subject<Unit>();

			SpeedEffect.SetActive(false);
		}

		protected override void Control()
		{
			base.Control();

			if (IsControllable)
			{
				if (Controller.GetButtonDown(MappedButton.Skill1) && CooldownSessions[0].IsCool)
				{
					Wall();
				}
				if (Controller.GetButtonUp(MappedButton.Skill1)) _releaseSubject.OnNext(Unit.Default);

				if (Controller.GetButtonDown(MappedButton.Skill2) && CooldownSessions[1].IsCool)
				{
					CooldownSessions[1].Heat();
					Blackhole();
				}
			}
		}

		void Wall()
		{
			GameObject wallObject = null;

			var wallSequence = Observable.Defer(() =>
				{
					WallIndicator.SetActive(true);
					WallIndicator.transform.localScale = new Vector3(WallLength, 1, WallWidth);

					return Observable.ReturnUnit();
				})
				.Zip(this.UpdateAsObservable().Where(_ => !Controller.GetButton(MappedButton.Skill1)), (a, b) => Unit.Default)
				.Do(_ =>
				{
					CooldownSessions[0].Heat();

					AudioSource.PlayOneShot(_wallClip);

					var sandParticle = Utilities.Particle.InstantiateParticle(SandParticlePrefab, _wallTransform.position, _wallTransform.rotation);
					var sandEmitShape = sandParticle.shape;
					sandEmitShape.scale = new Vector3(WallLength * 2, WallWidth, 1);
					var wallParticle = Utilities.Particle.InstantiateParticle(WallParticlePrefab, _wallTransform.position, _wallTransform.rotation);
					var wallEmitShape = wallParticle.shape;
					wallEmitShape.scale = new Vector3(WallLength * 2, WallWidth, 1);

					WallIndicator.SetActive(false);

					wallObject = Instantiate(WallPrefab);
					foreach (var col in wallObject.GetComponentsInChildren<Collider>())
					{
						col.gameObject.layer = LayerMask.NameToLayer("Team" + TeamNo + " Hit");
					}
					wallObject.layer = LayerMask.NameToLayer("Team" + TeamNo + " Hit");
					wallObject.transform.position = _wallTransform.position;
					wallObject.transform.rotation = _wallTransform.rotation;
					wallObject.transform.Rotate(new Vector3(0, 0, 90));
					wallObject.transform.localScale = new Vector3(1, WallLength, WallWidth);

					wallObject.OnCollisionEnterAsObservable()
						.Where(col => col.gameObject.tag == "Character")
						.TakeUntil(Observable.Timer(TimeSpan.FromSeconds(0.2)))
						.Subscribe(col =>
						{
							Debug.Log("Wall Stun");
							col.gameObject.GetComponent<CharacterBaseBehaviour>().Stun(WallStunDuration);
							Utilities.Particle.InstantiateParticle(CommonParticles.HitParticle, col.contacts[0].point);

							_hitSubject.OnNext(Unit.Default);
							// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
						})
						.AddTo(wallObject);
					Observable.Timer(TimeSpan.FromSeconds(WallDuration)).Subscribe(__ =>
					{
						Destroy(wallObject);
					});
				}).AsUnitObservable();

			this.AddEffect("Wall", new Effect(wallSequence, () =>
			{
				WallIndicator.SetActive(false);
			})
			{ Tags = new HashSet<EffectTag> { EffectTag.SkillCasting } });
		}

		void Blackhole()
		{
			AudioSource.PlayOneShot(_blackHoleClip);

			var blackhole = Instantiate(BlackholePrefab);
			blackhole.transform.position = _blackholeTransform.position;
			blackhole.GetComponent<SphereCollider>().radius = BlackholeRadius;
			blackhole.transform.GetChild(0).localScale = Vector3.one * BlackholeRadius;

			var blackholeParticle = Utilities.Particle.InstantiateParticle(BlackholeParticle, blackhole.transform.position);

			blackhole.OnTriggerStayAsObservable()
				.Where(col => col.gameObject.tag == "Character" || col.gameObject.tag == "Gold")
				.Subscribe(col =>
				{
					if (col.gameObject != this.gameObject)
					{
						col.gameObject.GetComponent<Rigidbody>().AddForce(
							(blackhole.transform.position - col.transform.position).normalized * BlackholeForce, ForceMode.Acceleration);
						if (col.gameObject.tag == "Gold")
						{
							col.gameObject.GetComponent<GoldBehaviour>().OccupyTime(this, OccupationTier.Skill, 0.2f);
						}
					}
				}).AddTo(blackhole);

			var speedBuf = Observable.EveryUpdate()
				.Do(_ =>
				{
					SpeedIncrement = BlackholeSpeedIncrement;
					SpeedEffect.SetActive(true);
				})
				.AsUnitObservable();

			blackhole.OnTriggerEnterAsObservable()
				.Where(col => col.gameObject == this.gameObject)
				.Subscribe(col =>
				{
					AddEffect("BlackholeBuf", new Effect(speedBuf, () =>
					{
						SpeedIncrement = 0;
						SpeedEffect.GetComponent<ParticleSystem>().Clear();
						SpeedEffect.SetActive(false);
					})
					{ Tags = new HashSet<EffectTag>() { EffectTag.SkillActive } });
				});
			blackhole.OnTriggerExitAsObservable()
				.Where(col => col.gameObject == this.gameObject)
				.Subscribe(col =>
				{
					CancelEffect("BlackholeBuf");
				});
			Observable.Timer(TimeSpan.FromSeconds(BlackholeDuration)).Subscribe(_ =>
			{
				Destroy(blackhole);
				blackholeParticle.Stop(true);
				CancelEffect("BlackholeBuf");
			});
		}
	}
}