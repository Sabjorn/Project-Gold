﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ProjectGold;
using ProjectGold.Character;
using UniRx;
using UniRx.Triggers;
using BehaviourTree;

public class AniviaAI_1 : CharacterAI {

	[SerializeField]
	private GameObject wallSearch;
	[SerializeField]
	private GameObject blackHoleSearch;

	private GameObject wallTarget;
	private int blackHoleCount = 0;

	// Use this for initialization
	void Start () {
		base.Start();
		behaviour = GetComponentInParent<AniviaBehaviour>();

		wallSearch.OnTriggerEnterAsObservable()
			.Where(col => col.gameObject.tag == "Character")
			.Subscribe(col =>
			{
				wallTarget = col.gameObject;
			});
		wallSearch.OnTriggerExitAsObservable()
			.Where(col => wallTarget != null && wallTarget == col.gameObject)
			.Subscribe(col =>
			{
				wallTarget = null;
			});
		blackHoleSearch.OnTriggerEnterAsObservable()
			.Where(col => col.gameObject.tag == "Character" || col.gameObject.tag == "Gold")
			.Subscribe(col =>
			{
				blackHoleCount++;
			});
		blackHoleSearch.OnTriggerExitAsObservable()
			.Where(col => col.gameObject.tag == "Character" || col.gameObject.tag == "Gold")
			.Where(col => blackHoleCount > 0)
			.Subscribe(col =>
			{
				blackHoleCount--;
			});

		rootNode = new SequencerNode(new List<NodeBase>
        {
            //new ActionNode(Think),
            new SelectorNode(new List<NodeBase>
            {
            	// いつでも
            	new DecoratorNode(IsBlackHoleTiming, new ActionNode(BlackHole)),
            	new DecoratorNode(IsWallTarget, new ActionNode(Wall)),
            	// コインを掴んでいるとき
				new DecoratorNode(IsGrabbingCoin, new SelectorNode(new List<NodeBase>{
		    		new DecoratorNode(IsOnHomeWithCoin, new ActionNode(ReleaseCoin)),
					new ActionNode(BackHome)
		    	})),
				// 掴んでない時
		    	commonNode
            })
        });
	}

	// Actions
	bool Wall ()
	{
		if (behaviour.CooldownSessions [0].IsCool) {
			Input.SetButton(MappedButton.Skill1);
			return true;
		}
		return false;
	}
	bool BlackHole ()
	{	
		if (behaviour.CooldownSessions [1].IsCool) {
			Input.SetButton(MappedButton.Skill2);
			return true;
		}
		return false;
	}

	// Conditions
	bool IsWallTarget ()
	{
		if(wallTarget != null) return true;
		return false;
	}
	bool IsBlackHoleTiming ()
	{
		if(blackHoleCount >= 3) return true;
		return false;
	}
	
	// Update is called once per frame
	void Update () {
		base.Update ();

	}
}
