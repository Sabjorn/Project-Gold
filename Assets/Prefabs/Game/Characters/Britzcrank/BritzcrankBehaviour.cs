﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace ProjectGold.Character
{
	/// <summary>
	/// ブリッツ。
	/// ・伸びる腕で他プレイヤーor金塊を掴み引き寄せる。
	/// ・回転パンチで周りの敵をスタンさせる。
	/// </summary>
	public class BritzcrankBehaviour : CharacterBaseBehaviour
	{
		/// <summary>
		/// ロケットグラブのクールダウン時間。sec。
		/// </summary>
		[Header("ロケットグラブ")]
		public float RocketGrabCooldown;

		/// <summary>
		/// ロケットグラブが届く範囲。unit。
		/// </summary>
		public float RocketGrabDistance;

		/// <summary>
		/// ロケットグラブの速度。unit/sec。
		/// </summary>
		public float RocketGrabSpeed;

		public float RocketGrabStunDuration;

		[SerializeField] private GameObject HandPrefab;

		/// <summary>
		/// 回転パンチのクールダウン時間。sec。
		/// </summary>
		[Header("回転パンチ")]
		public float SwingCooldown;

		/// <summary>
		/// 回転パンチの発生遅延。sec。
		/// </summary>
		public float SwingDelay;

		/// <summary>
		/// 回転パンチの持続時間。sec。
		/// </summary>
		public float SwingDuration;

		/// <summary>
		/// 回転パンチのスタン時間。sec。
		/// </summary>
		public float SwingStunDuration;

		/// <summary>
		/// 回転パンチのキャラクターの吹っ飛び速度。unit/sec。
		/// </summary>
		public float SwingImpact;

		/// <summary>
		/// 回転パンチの金塊の吹っ飛び速度。unit/sec。
		/// </summary>
		public float SwingGoldImpact;

		[SerializeField]
		private GameObject GroveObject;

		[SerializeField] private GameObject SwingIndicator;

		[SerializeField] private LineRenderer _rocketGrabLineRenderer;
		[SerializeField] private GameObject _rocketGrabDestinationObject;

		[SerializeField] private ParticleSystem _grabHitParticlePrefab;

		[SerializeField] private AudioClip _rocketLaunchClip,_rocketGrabClip,_swingHitClip, _swingClip;

		private Subject<Unit> _skill1ReleaseSubject;

		protected new void Awake()
		{
			base.Awake();

			CooldownSessions = new CooldownSession[]
			{
				new CooldownSession(RocketGrabCooldown),
				new CooldownSession(SwingCooldown)
			};

			GroveObject.SetActive(false);
			GroveObject.layer = LayerMask.NameToLayer("OmniHit");

			SwingIndicator.SetActive(false);

			_rocketGrabLineRenderer.SetPosition(1, Vector3.forward * RocketGrabDistance);
			_rocketGrabLineRenderer.enabled = false;

			_rocketGrabDestinationObject.transform.localPosition = Vector3.forward * RocketGrabDistance;
			_rocketGrabDestinationObject.SetActive(false);

			_skill1ReleaseSubject = new Subject<Unit>();
		}

		protected override void Control()
		{
			base.Control();

			if (IsControllable)
			{
				if (Controller.GetButtonDown(MappedButton.Skill1) && CooldownSessions[0].IsCool)
				{
					RocketGrab();
				}
				if (Controller.GetButtonDown(MappedButton.Skill2) && CooldownSessions[1].IsCool)
				{
					CooldownSessions[1].Heat();
					Swing();
				}

				if (Controller.GetButtonUp(MappedButton.Skill1)) _skill1ReleaseSubject.OnNext(Unit.Default);
			}
		}

		/// <summary>
		/// ロケットグラブを開始する。
		/// </summary>
		public void RocketGrab()
		{
			GameObject handObject = null;
			GameObject grabbedObject = null;

			IDisposable grabbedEffect = null;

			var rocketSequence = Observable.Defer(() =>
			{
				_rocketGrabLineRenderer.enabled = true;
				_rocketGrabDestinationObject.SetActive(true);

				return this.UpdateAsObservable();
			})
			.TakeUntil(this.UpdateAsObservable().Where(_ => !Controller.GetButton(MappedButton.Skill1)))
			.Concat(Observable.Defer(() =>
			{
				CooldownSessions[0].Heat();
				_rocketGrabLineRenderer.enabled = false;
				_rocketGrabDestinationObject.SetActive(false);

				AudioSource.PlayOneShot(_rocketLaunchClip);

				IsControllable = false;
				TargetVelocity = Vector3.zero;

				handObject = Instantiate(HandPrefab);
				handObject.layer = LayerMask.NameToLayer("OmniHit");
				handObject.transform.position = transform.position;
				handObject.UpdateAsObservable().Subscribe(_ =>
				{
					handObject.transform.rotation =
						Quaternion.LookRotation(handObject.transform.position - transform.position, Vector3.up);

					var lineRenderer = handObject.GetComponent<LineRenderer>();
					lineRenderer.SetPositions(new Vector3[]
					{
						handObject.transform.position,
						transform.position
					});
				}).AddTo(handObject)
					.AddTo(this.gameObject);

				// var dir = Direction;
				var dir = PivotTransform.rotation * Vector3.forward;
				var handAdvance = handObject.UpdateAsObservable()
					.Do(_ =>
					{
						handObject.transform.position += dir * RocketGrabSpeed * Time.deltaTime;
					})
					.TakeUntil(
						handObject.OnTriggerStayAsObservable()
							.Where(col => (col.tag == "Character" || col.tag == "Gold") && (col.gameObject != gameObject))
							.Do(col => { grabbedObject = col.gameObject; }).AsUnitObservable()
							.Amb(Observable.Timer(TimeSpan.FromSeconds(RocketGrabDistance / RocketGrabSpeed)).AsUnitObservable())
							.Amb(handObject.OnTriggerStayAsObservable()
								.Where(col => col.gameObject.tag == "Boundary" || col.gameObject.tag == "Obstacle").AsUnitObservable())
					)
					.DoOnCompleted(() =>
					{
						if (grabbedObject != null)
						{
							Utilities.Particle.InstantiateParticle(_grabHitParticlePrefab, grabbedObject.transform.position);

							AudioSource.Stop();
							AudioSource.PlayOneShot(_rocketGrabClip);

							grabbedEffect = RocketGrabEffect(grabbedObject, handObject);
						}
					});
				var handRetract = handObject.UpdateAsObservable()
					.Do(_ =>
					{
						handObject.transform.position += (transform.position - handObject.transform.position).normalized *
														 RocketGrabSpeed * Time.deltaTime;
					})
					.TakeUntil(handObject.OnTriggerStayAsObservable()
						.Where(col => col.gameObject == gameObject))
					.DoOnCompleted(() =>
						{
							if (grabbedEffect != null) grabbedEffect.Dispose();
							if (grabbedObject != null)
							{
								if (grabbedObject.tag == "Gold")
								{
									Release();
									Grab(grabbedObject);
								}
								if (grabbedObject.tag == "Character")
								{
									grabbedObject.GetComponent<CharacterBaseBehaviour>().Stun(RocketGrabStunDuration);

									_hitSubject.OnNext(Unit.Default);
									// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
								}
							}
						}
					);

				handAdvance
					.Concat(Observable.Timer(TimeSpan.FromSeconds(0.1)).AsUnitObservable())
					.Concat(handRetract)
					.DoOnCompleted(() =>
					{
						IsControllable = true;
						Destroy(handObject);
					})
					.Subscribe().AddTo(handObject)
					.AddTo(this.gameObject);

				return Observable.ReturnUnit();
			}));

			AddEffect("RocketGrab", new Effect(rocketSequence, () =>
			{
				IsControllable = true;
				Destroy(handObject);

				_rocketGrabLineRenderer.enabled = false;
				_rocketGrabDestinationObject.SetActive(false);

				if (grabbedEffect != null)
				grabbedEffect.Dispose();
			})
			{
				Tags = new HashSet<EffectTag>() { EffectTag.SkillActive }
			});
		}

		/// <summary>
		/// ロケットグラブで以てオブジェクト／キャラクターを捕縛状態にする。
		/// </summary>
		/// <param name="grabbed">つかまれるGameObject。</param>
		/// <param name="grabbing">つかむロケットグラブの手のGameObject。</param>
		/// <returns>Disposable。Disposeで捕縛を解放する。</returns>
		IDisposable RocketGrabEffect(GameObject grabbed, GameObject grabbing)
		{
			var localPosition = grabbing.transform.InverseTransformPoint(grabbed.transform.position);

			if (grabbed.tag == "Character")
			{
				var c = grabbed.GetComponent<CharacterBaseBehaviour>();

				if (c.Grabbing != null)
				{
					c.Grabbing.GetComponent<GoldBehaviour>().OccupyTime(this, OccupationTier.Skill, 0.5f);
				}

				c.CancelEffect(EffectTag.SkillActive);
				c.CancelEffect(EffectTag.SkillCasting);

				var grabbedEffect = grabbed.UpdateAsObservable()
					.Do(_ =>
					{
						c.IsControllable = false;
						c.TargetVelocity = Vector3.zero;

						grabbed.transform.position = grabbing.transform.TransformPoint(localPosition);
					});
				var skillEffect = new Effect(grabbedEffect, () =>
				{
					c.IsControllable = true;
				})
				{
					Tags = new HashSet<EffectTag>() { EffectTag.Debuf }
				};
				c.AddEffect("Grabbed", skillEffect);

				return skillEffect;
			}
			else
			{
				grabbed.layer = LayerMask.NameToLayer("Grabbed Gold");

				grabbed.GetComponent<GoldBehaviour>().OccupyTime(this, OccupationTier.Skill, 0.5f);

				return grabbed.UpdateAsObservable().Subscribe(_ =>
				{
					grabbed.transform.position = grabbing.transform.TransformPoint(localPosition);
				});
			}
		}

		/// <summary>
		/// 回転パンチを開始する。
		/// </summary>
		public void Swing()
		{
			IDisposable groveHitEffect = null;
			CompositeDisposable disposables = new CompositeDisposable();

			var swingSequence = Observable.Defer(() =>
				{
					TargetVelocity = Vector3.zero;

					SwingIndicator.SetActive(true);

					return Observable.Timer(TimeSpan.FromSeconds(SwingDelay));
				}).Do(_ =>
				{
					GroveObject.SetActive(true);
					SwingIndicator.SetActive(false);

					AudioSource.PlayOneShot(_swingClip);

					GroveObject.GetComponent<Animator>().SetTrigger("Swing");
					foreach (Transform child in GroveObject.transform)
					{
						disposables.Add(
							groveHitEffect = child.OnTriggerEnterAsObservable()
								.Where(col => col.gameObject.tag == "Character" || col.gameObject.tag == "Gold")
								.Subscribe(col =>
								{
									if (col.gameObject.tag == "Character")
									{
										AudioSource.PlayOneShot(_swingHitClip);

										var character = col.gameObject.GetComponent<CharacterBaseBehaviour>();

										character.Stun(SwingStunDuration);
										character.GetComponent<Rigidbody>().AddForce(
											(character.transform.position - transform.position).normalized * SwingImpact,
											ForceMode.VelocityChange
										);

										Utilities.Particle.InstantiateParticle(CommonParticles.HitParticle,
											(transform.position + character.transform.position) / 2);

										_hitSubject.OnNext(Unit.Default);
										// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
									}
									else
									{
										col.gameObject.GetComponent<GoldBehaviour>().OccupyTime(this, OccupationTier.Skill, 0.5f);

										var rigidBody = col.gameObject.GetComponent<Rigidbody>();
										rigidBody.AddForce((col.gameObject.transform.position - transform.position).normalized * SwingGoldImpact, ForceMode.VelocityChange);
									}
								})
						);
					}
				})
				.Delay(TimeSpan.FromSeconds(SwingDuration))
				.DoOnCompleted(() =>
				{
					disposables.Dispose();
					GroveObject.SetActive(false);
					SwingIndicator.SetActive(false);
				})
				.AsUnitObservable();

			AddEffect("Swing", new Effect(swingSequence, () =>
			 {
				 disposables.Dispose();
				 GroveObject.SetActive(false);
				 SwingIndicator.SetActive(false);
			 })
			{
				Tags = new HashSet<EffectTag>() { EffectTag.SkillActive }
			});
		}
	}
}