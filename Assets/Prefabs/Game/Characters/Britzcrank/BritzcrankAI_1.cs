﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ProjectGold;
using ProjectGold.Character;
using UniRx;
using UniRx.Triggers;
using BehaviourTree;

public class BritzcrankAI_1 : CharacterAI {

	[SerializeField]
	private GameObject rocketGrabSearch;
	[SerializeField]
	private GameObject swingSearch;

	private GameObject rocketGrabTarget;
	private GameObject swingTarget;

	// Use this for initialization
	void Start () {
		base.Start();
		behaviour = GetComponentInParent<BritzcrankBehaviour>();

		rocketGrabSearch.OnTriggerEnterAsObservable()
			.Where(col => col.gameObject.tag == "Gold")
			.Subscribe(col =>
			{
				rocketGrabTarget = col.gameObject;
			});
		rocketGrabSearch.OnTriggerExitAsObservable()
			.Where(col => rocketGrabTarget != null && rocketGrabTarget == col.gameObject)
			.Subscribe(col =>
			{
				rocketGrabTarget = null;
			});
		swingSearch.OnTriggerEnterAsObservable()
			.Where(col => col.gameObject.tag == "Character")
			.Subscribe(col =>
			{
				swingTarget = col.gameObject;
			});
		swingSearch.OnTriggerExitAsObservable()
			.Where(col => swingTarget != null && swingTarget == col.gameObject)
			.Subscribe(col =>
			{
				swingTarget = null;
			});

		rootNode = new SequencerNode(new List<NodeBase>
        {
            //new ActionNode(Think),
            new SelectorNode(new List<NodeBase>
            {
            	// いつでも
            	new DecoratorNode(IsSwingTarget, new ActionNode(Swing)),
            	// コインを掴んでいるとき
				new DecoratorNode(IsGrabbingCoin, new SelectorNode(new List<NodeBase>{
		    		new DecoratorNode(IsOnHomeWithCoin, new ActionNode(ReleaseCoin)),
					new ActionNode(BackHome)
		    	})),
				// 掴んでない時
            	new DecoratorNode(IsRocketGrabTarget, new ActionNode(RocketGrab)),
		    	commonNode
            })
        });
	}

	// Actions
	bool RocketGrab ()
	{
		if (behaviour.CooldownSessions [0].IsCool && !Input.GetButtonStateLast(MappedButton.Skill1)) {
			Input.SetButton(MappedButton.Skill1);
			return true;
		}
		return false;
	}
	bool Swing ()
	{	
		if (behaviour.CooldownSessions [1].IsCool) {
			Input.SetButton(MappedButton.Skill2);
			return true;
		}
		return false;
	}

	// Conditions
	bool IsRocketGrabTarget ()
	{
		if(rocketGrabTarget != null) return true;
		return false;
	}
	bool IsSwingTarget ()
	{
		if(swingTarget != null) return true;
		return false;
	}
	
	// Update is called once per frame
	void Update () {
		base.Update ();

	}
}
