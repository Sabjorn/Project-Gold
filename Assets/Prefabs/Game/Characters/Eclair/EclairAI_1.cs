﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ProjectGold;
using ProjectGold.Character;
using UniRx;
using UniRx.Triggers;
using BehaviourTree;

public class EclairAI_1 : CharacterAI {

	[SerializeField]
	private GameObject thunderSearch;

	private GameObject thunderTarget;
	private GameObject boltObject;


	private float holdThreshold;
	private float skill2PushedTime;
	private bool isThunderPushed;
	private float elapsedTimeAtBoltShot;
	private float nextBoltUpdateTime;

	// Use this for initialization
	void Start () {
		base.Start();
		behaviour = GetComponentInParent<EclairBehaviour>();
		holdThreshold = ((EclairBehaviour)behaviour).HoldThreshold;
		skill2PushedTime = 0.0f;
		isThunderPushed = false;
		elapsedTimeAtBoltShot = Mathf.Infinity;
		nextBoltUpdateTime = Random.Range(2.0f, 10.0f);

		thunderSearch.OnTriggerEnterAsObservable()
			.Where(col => col.gameObject.tag == "Character" && col.gameObject != behaviour.gameObject)
			.Subscribe(col =>
			{
				thunderTarget = col.gameObject;
			});
		thunderSearch.OnTriggerExitAsObservable()
			.Where(col => thunderTarget != null && thunderTarget == col.gameObject)
			.Subscribe(col =>
			{
				thunderTarget = null;
			});

		rootNode = new SequencerNode(new List<NodeBase>
        {
            //new ActionNode(Think),
            new SelectorNode(new List<NodeBase>
            {
            	// いつでも
            	new DecoratorNode(IsBoltUpdateTime, new SelectorNode(new List<NodeBase>{
            		new ActionNode(BoltShot)
            	})),
            	// コインを掴んでいるとき
				new DecoratorNode(IsGrabbingCoin, new SelectorNode(new List<NodeBase>{
		    		new DecoratorNode(IsOnHomeWithCoin, new ActionNode(ReleaseCoin)),
		    		new DecoratorNode(IsLeapHomeTiming, new SelectorNode(new List<NodeBase>{
						new DecoratorNode(IsLeapButtonDownTime, new ActionNode(LeapButtonPush)),
			    		new ActionNode(LeapButtonUp)
					})),
            		new DecoratorNode(IsThunderTarget, new ActionNode(Thunder)),
					new ActionNode(BackHome)
				})),
				// コインを掴んでいないとき
				new SequencerNode(new List<NodeBase>{
		    		new ActionNode(SearchCoin),
		    		new SelectorNode(new List<NodeBase>{
						new ActionNode(GrabCoin),
						new DecoratorNode(IsLeapCoinTiming, new SelectorNode(new List<NodeBase>{
			    			new DecoratorNode(IsLeapButtonDownTime, new ActionNode(LeapButtonPush)),
			    			new ActionNode(LeapButtonUp)
			    		})),
		                new ActionNode(GoToCoin),
		    		}),
		    		new DecoratorNode(IsThunderTarget, new ActionNode(Thunder)),
		    	})
            })
        });
	}

	// Actions
	bool BoltShot ()
	{
		if (behaviour.CooldownSessions [0].IsCool) {
			Input.SetButton(MappedButton.Skill1);
			elapsedTimeAtBoltShot = 0.0f;
			nextBoltUpdateTime = Random.Range(2.0f, 10.0f);
			return true;
		}
		return false;
	}
	bool Thunder ()
	{
		if (isThunderPushed == false && ((EclairBehaviour)behaviour).Stack > 0) {
			Input.SetButton(MappedButton.Skill2);
			isThunderPushed = true;
			return true;
		}
		isThunderPushed = false;
		return false;
	}
	bool LeapButtonPush ()
	{
		if (((EclairBehaviour)behaviour).Stack > 0) {
			Input.SetButton(MappedButton.Skill2);
			skill2PushedTime += Time.deltaTime;
			return true;
		}
		skill2PushedTime = 0.0f;
		return false;
	}
	bool LeapButtonUp ()
	{	
		skill2PushedTime = 0.0f;
		return false;
	}



	// Conditions
	bool IsThunderTarget ()
	{
		if(thunderTarget != null) return true;
		return false;
	}
	bool IsBoltObject ()
	{
		if(((EclairBehaviour)behaviour).BoltObject != null) return true;
		return false;
	}
	bool IsNoBoltObject ()
	{
		return !IsBoltObject();
	}
	bool IsBoltUpdateTime ()
	{
		if (elapsedTimeAtBoltShot > nextBoltUpdateTime) {
			return true;
		}
		return false;
	}
	bool IsLeapHomeTiming()
	{
		if(Vector3.Distance(boltObject.transform.position, myZone.transform.position) <= 15.0f && Vector3.Distance(behaviour.transform.position, myZone.transform.position) >= 15.0f) return true;
		return false;
	}
	bool IsLeapCoinTiming()
	{
		if(thinkData.targetCoin == null) return false;
		if(Vector3.Distance(boltObject.transform.position, thinkData.targetCoin.transform.position) <= 15.0f && Vector3.Distance(behaviour.transform.position, boltObject.transform.position) >= 15.0f) return true;
		return false;
	}
	bool IsLeapButtonDownTime()
	{
		if(((EclairBehaviour)behaviour).HoldThreshold + 0.5f > skill2PushedTime)return true;
		return false;
	}

	// Update is called once per frame
	void Update ()
	{
		boltObject = ((EclairBehaviour)behaviour).BoltObject;
		base.Update ();

		if (IsBoltObject()) {
			elapsedTimeAtBoltShot += Time.deltaTime;

			var collider = thunderSearch.GetComponent<CapsuleCollider>();
			thunderSearch.transform.position = transform.position;
			thunderSearch.transform.LookAt(boltObject.transform);
			collider.height = (transform.position - boltObject.transform.position).magnitude;
			collider.center = Vector3.forward * collider.height / 2;
		}
	}
}
