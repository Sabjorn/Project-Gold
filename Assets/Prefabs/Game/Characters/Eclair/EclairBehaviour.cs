﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectGold.Character;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace ProjectGold.Character
{
	/// <summary>
	/// エクレア。
	/// ・ボルトを放ち、壁で停止する。
	/// ・自分とボルトの間に電撃、或いはボルト位置にブリンク。
	/// </summary>
	public class EclairBehaviour : CharacterBaseBehaviour
	{
		[Header("ボルトショット")]
		public float BoltCooldown;
		public float BoltSpeed;
		public float BoltHitStunDuration;
		public float BoltHitKnockback;
		public float BoltHitCoolRatio;
		[SerializeField] private GameObject _boltPrefab;

		[Header("電撃・リープ")]
		public int MaxStack;
		public float HoldThreshold;
		public float ActionCooldown;
		public float ThunderDuration;
		public float ThunderWidth;
		public float ShockStunDuration;
		public float LeapSpeed;
		public float LeapStunImpact;
		public float LeapStunDuration;
		public float LeapColliderScale;

		[SerializeField] private Texture2D[] _thunderTextures;
		[SerializeField] private GameObject _thunderPrefab;
		[SerializeField] private GameObject _leapParticlePrefab;

		[SerializeField] private GameObject _stackIconPrefab;
		[SerializeField] private Transform _stackIconsTransform;

		[SerializeField] private AudioClip _boltShotClip, _boltHitClip, _shockClip, _leapClip;

		private GameObject _boltObject;
		public GameObject BoltObject { get { return _boltObject; } }

		private float _colliderRadius;

		private Subject<Unit> _releaseSubject;

		private int _stack;
		private List<GameObject> _stackObjects;
		public int Stack { get { return _stack; } }

		new void Awake()
		{
			base.Awake();

			CooldownSessions = new[]
			{
				new CooldownSession(BoltCooldown),
				new CooldownSession(ActionCooldown)
			};

			_boltObject = null;

			_releaseSubject = new Subject<Unit>();

			_colliderRadius = GetComponent<CapsuleCollider>().radius;

			_stack = 0;
			_stackObjects = new List<GameObject>();
			CooldownSessions[1].Heat();
		}

		new void Update()
		{
			base.Update();

			if (_stack < MaxStack && CooldownSessions[1].IsCool)
			{
				_stack++;
				if (_stack < MaxStack) CooldownSessions[1].Heat();
			}

			for (int i = 0; i < _stack || i < _stackObjects.Count; i++)
			{
				if (_stackObjects.Count <= i)
				{
					_stackObjects.Add(Instantiate(_stackIconPrefab, _stackIconsTransform));
				}

				var obj = _stackObjects[i];
				var p = Vector3.zero + Vector3.right * (i - (_stack - 1) / 2f);

				if (i < _stack)
				{
					obj.SetActive(true);
					obj.transform.localPosition = p;
				}
				else
				{
					obj.SetActive(false);
				}
			}
		}

		protected override void Control()
		{
			base.Control();

			if (IsControllable)
			{
				if (Controller.GetButtonDown(MappedButton.Skill1) && CooldownSessions[0].IsCool)
				{
					CooldownSessions[0].Heat();
					BoltShot();
				}
				if (Controller.GetButtonDown(MappedButton.Skill2) && _stack > 0)
				{
					if (_boltObject != null)
						BoltAction();
				}
				if (Controller.GetButtonUp(MappedButton.Skill2)) _releaseSubject.OnNext(Unit.Default);
			}
		}

		void BoltShot()
		{
			if (_boltObject != null) Destroy(_boltObject);

			AudioSource.PlayOneShot(_boltShotClip);

			var dir = Direction;

			_boltObject = Instantiate(_boltPrefab);
			_boltObject.transform.position = transform.position;
			_boltObject.transform.rotation = Quaternion.LookRotation(dir);

			var playerNoText = Instantiate(Resources.Load<GameObject>("Prefabs/Game/Player No Text"), _boltObject.transform.position,
				Quaternion.identity);
			playerNoText.UpdateAsObservable().Subscribe(_ =>
			{
				playerNoText.transform.position = _boltObject.GetComponentInChildren<BoxCollider>().transform.position + new Vector3(0.0f, 1.0f, 0.0f);

				var text = playerNoText.GetComponentInChildren<TextMeshPro>();
				text.text = (PlayerNo + 1) + "P";
				text.color = PlayerColors.Colors[PlayerNo];
			});
			_boltObject.OnDestroyAsObservable()
			.Subscribe(_ => 
			{
				Destroy(playerNoText);
			});

			// 壁衝突時のシーケンス
			var wallHitObservable = _boltObject.OnTriggerEnterAsObservable()
				.Where(col => col.gameObject.tag == "Boundary" || col.gameObject.tag == "Obstacle");
			wallHitObservable.Subscribe(_ =>
			{
				AudioSource.PlayOneShot(_boltHitClip);
			});

			// ボルト移動のシーケンス
			_boltObject.UpdateAsObservable()
				.Do(_ =>
				{
					_boltObject.transform.position += dir * BoltSpeed * Time.deltaTime;
				})
				.TakeUntil(wallHitObservable)
				.Subscribe();

			// 他キャラ衝突時のシーケンス
			_boltObject.OnTriggerEnterAsObservable()
				.Where(col => col.gameObject != gameObject && col.gameObject.tag == "Character")
				.TakeUntil(wallHitObservable)
				.Subscribe(col =>
				{
					Utilities.Particle.InstantiateParticle(CommonParticles.HitParticle,
						(col.transform.position + _boltObject.transform.position) / 2);

					col.gameObject.GetComponent<CharacterBaseBehaviour>().Stun(BoltHitStunDuration);
					col.gameObject.GetComponent<Rigidbody>()
						.AddForce(dir * BoltHitKnockback, ForceMode.VelocityChange);

					_hitSubject.OnNext(Unit.Default);
					// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;

					//CooldownSessions[1].RemainingTime *= BoltHitCoolRatio;
					//Destroy(_boltObject);
				});
		}

		void BoltAction()
		{
			GameObject leapParticle = null;
			CompositeDisposable disposables = new CompositeDisposable();

			var boltSequence = Observable.Defer(() =>
				{
					IsMobile = false;
					TargetVelocity = Vector3.zero;
					return Observable.ReturnUnit();
				}).SelectMany(
					Observable.Timer(TimeSpan.FromSeconds(HoldThreshold)).Select(_ => 0)
						.Merge(_releaseSubject.Select(_ => 1))
						.First())
				.SelectMany(type =>
				{
					IsMobile = true;

					switch (type)
					{
						case 0:
							// リープ
							AudioSource.PlayOneShot(_leapClip);

							IsControllable = false;

							leapParticle = Instantiate(_leapParticlePrefab);

							var arrivedObservable = this.UpdateAsObservable()
								.Where(_ => (_boltObject.transform.position - transform.position).magnitude <= 5);

							var stunHitDisposable = this.OnCollisionEnterAsObservable()
								.Where(col => col.gameObject.tag == "Character")
								.Select(col => col.gameObject.GetComponent<CharacterBaseBehaviour>())
								.Do(character =>
								{
									character.Stun(LeapStunDuration);

									Utilities.Particle.InstantiateParticle(CommonParticles.HitParticle, (transform.position + character.transform.position) / 2);

									character.RigidBody.AddForce((character.transform.position - transform.position).normalized * LeapStunImpact,
										ForceMode.VelocityChange);

									_hitSubject.OnNext(Unit.Default);
									// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
								})
								.Subscribe();
							disposables.Add(stunHitDisposable);

							return this.UpdateAsObservable()
								.Do(_ =>
								{
									GetComponent<CapsuleCollider>().radius = _colliderRadius * LeapColliderScale;

									transform.position += (_boltObject.transform.position - transform.position).normalized * LeapSpeed *
														  Time.deltaTime;
									if (leapParticle != null) leapParticle.transform.position = transform.position;
								}).TakeUntil(arrivedObservable);

						case 1:
							// 電撃
							AudioSource.PlayOneShot(_shockClip);

							var thunderObject = Instantiate(_thunderPrefab);
							thunderObject.layer = LayerMask.NameToLayer("Team" + TeamNo + " Hit");
							thunderObject.transform.position = transform.position;
							thunderObject.transform.LookAt(_boltObject.transform);

							thunderObject.OnTriggerEnterAsObservable()
								.Where(col => col.gameObject != gameObject && col.gameObject.tag == "Character")
								.Subscribe(col =>
								{
									var charBehaviour = col.gameObject.GetComponent<CharacterBaseBehaviour>();
									charBehaviour.Stun(ShockStunDuration);

									_hitSubject.OnNext(Unit.Default);
									// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
								})
								.AddTo(thunderObject);

							var capsuleCollider = thunderObject.GetComponent<CapsuleCollider>();
							var lineRenderer = thunderObject.GetComponent<LineRenderer>();

							thunderObject.UpdateAsObservable()
								.Subscribe(_ =>
								{
									capsuleCollider.isTrigger = true;
									capsuleCollider.height = (transform.position - _boltObject.transform.position).magnitude;
									capsuleCollider.center = Vector3.forward * capsuleCollider.height / 2;
									capsuleCollider.radius = ThunderWidth;

									lineRenderer.useWorldSpace = true;
									lineRenderer.SetPositions(new[]
									{
										transform.position,
										_boltObject.transform.position
									});

									lineRenderer.material.mainTexture = _thunderTextures[UnityEngine.Random.Range(0, _thunderTextures.Length)];
								})
								.AddTo(thunderObject);

							Destroy(thunderObject, ThunderDuration);

							return Observable.ReturnUnit();
						default:
							return Observable.ReturnUnit();
					}
				})
				.DoOnCompleted(() =>
				{
					IsMobile = true;
					IsControllable = true;

					_stack--;
					if(CooldownSessions[1].IsCool)CooldownSessions[1].Heat();

					disposables.Dispose();

					GetComponent<CapsuleCollider>().radius = _colliderRadius;

					if (leapParticle != null) Destroy(leapParticle);
				})
				.AsUnitObservable();

			AddEffect("BoltAction", new Effect(boltSequence, () =>
			{
				IsMobile = true;
				IsControllable = true;

				disposables.Dispose();

				GetComponent<CapsuleCollider>().radius = _colliderRadius;

				if (leapParticle != null) Destroy(leapParticle);
			})
			{
				Tags = new HashSet<EffectTag>()
				{
					EffectTag.SkillActive
				}
			});
		}
	}
}