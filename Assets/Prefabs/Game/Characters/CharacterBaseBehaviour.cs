﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProjectGold.Character;
using ProjectGold.Game;
using TMPro;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace ProjectGold.Character
{
	/// <summary>
	/// キャラクターのベースクラス。
	/// </summary>
	[DisallowMultipleComponent]
	public class CharacterBaseBehaviour : MonoBehaviour
	{
		private int _playerNo;
		/// <summary>
		/// プレイヤー番号。操作しているプレイヤーを識別する。
		/// </summary>
		public int PlayerNo
		{
			get { return _playerNo; }
			set
			{
				_playerNo = value;

				var text = playerNoText.GetComponentInChildren<TextMeshPro>();
				text.text = (PlayerNo + 1) + "P";
				text.color = PlayerColors.Colors[PlayerNo];
			}
		}

		/// <summary>
		/// チーム番号。所属しているチームを識別する。
		/// </summary>
		private int teamNo;
		public int TeamNo
		{
			get
			{
				return teamNo;
			}

			set
			{
				teamNo = value;
				gameObject.layer = LayerMask.NameToLayer("Team" + TeamNo + " Character");
			}
		}

		/// <summary>
		/// 入力を取得するコントローラー。
		/// </summary>
		public IControllerInput Controller;

		[SerializeField] protected PlayerColors PlayerColors;
		/// <summary>
		/// AIかどうか
		/// </summary>
		[Header("AI")] public bool isAI = false;
		public GameObject AIPrefab;
		private GameObject aiObject;

		/// <summary>
		/// プレイヤーからの入力が有効かどうか。
		/// </summary>
		[Header("基本動作")] public bool IsControllable = true;

		public bool IsMobile = true;

		/// <summary>
		/// プレイヤーの入力に対する移動速度。unit/sec。
		/// </summary>
		public float Speed;

		/// <summary>
		/// バフ・デバフによる移動速度の増減。unit/sec。
		/// </summary>
		public float SpeedIncrement;

		/// <summary>
		/// 静止状態でのキャラクターの加速度。unit/sec^2。
		/// </summary>
		public float Acceleration;

		/// <summary>
		/// 方向転換での減速の度合い。
		/// </summary>
		[Range(0, 10)]
		public float TurnLoss;

		/// <summary>
		/// 目標となる速度。unit/sec。
		/// </summary>
		[HideInInspector]
		public Vector3 TargetVelocity;

		/// <summary>
		/// キャラクターが持つRigidBody。
		/// </summary>
		[HideInInspector]
		public Rigidbody RigidBody;

		[SerializeField]
		protected Animator _spriteAnimator;

		[SerializeField] protected Transform PivotTransform;

		[SerializeField] protected Transform DirectionIndicator;

		[SerializeField] protected GameObject[] CooldownGaugeObjects;
		[SerializeField] protected GameObject StunGauge;

		/// <summary>
		/// つかんだオブジェクトの配置場所を参照するTransform。
		/// </summary>
		[Header("掴み関連")]
		[SerializeField]
		protected Transform GrabbingTransform;

		/// <summary>
		/// つかみの当たり判定。
		/// </summary>
		[SerializeField]
		protected GrabTrigger GrabTrigger;

		/// <summary>
		/// つかんだオブジェクトを離すときの速度。
		/// </summary>
		[SerializeField]
		protected float ReleaseImpulse;

		/// <summary>
		/// 共通パーティクル。
		/// </summary>
		[Header("エフェクト関連")]
		[SerializeField]
		protected CommonParticles CommonParticles;

		/// <summary>
		/// スタンエフェクトのプレハブ。
		/// </summary>
		[SerializeField]
		protected GameObject StunnedEffectPrefab;

		public CooldownSession[] CooldownSessions;

		private Vector3 _direction;
		/// <summary>
		/// 今向いている方向。
		/// </summary>
		public Vector3 Direction { get { return _direction; } }

		[SerializeField] private Gradient _cooldownGaugeGradient;

		[SerializeField] protected AudioClip GrabClip, ReleaseClip;

		public GameObject Grabbing;
		private GameObject _candidateMarker;
		private GameObject _grabbingMarker;

		protected Dictionary<string, Effect> _skillEffects;

		protected static Material _lineMaterial;

		protected AudioSource AudioSource;

		private float _stunTime;

        public bool onMyZone = false;

		// 集計用
		protected Subject<float> _travelSubject;
		public IObservable<float> TravelObservable { get { return _travelSubject; } }

		protected Subject<Unit> _hitSubject;
		public IObservable<Unit> HitObservable { get { return _hitSubject; } }

		protected Subject<Unit> _hittedSubject;
		public IObservable<Unit> HittedObservable { get { return _hittedSubject; } }

		private GameObject playerNoText;

		protected void Awake ()
		{
			RigidBody = GetComponent<Rigidbody> ();

			TargetVelocity = Vector3.zero;

			CooldownSessions = null;

			_direction = transform.TransformDirection (Vector3.forward);

			_skillEffects = new Dictionary<string, Effect> ();

			Shader shader = Shader.Find ("Hidden/Internal-Colored");
			_lineMaterial = new Material (shader);

			DirectionIndicator.localPosition = new Vector3 (0, 0, 2);
			DirectionIndicator.localScale = Vector3.one * 0.6f;

			StunGauge.GetComponent<Renderer> ().enabled = false;


			AudioSource = GetComponent<AudioSource>();

			_stunTime = 0;

			// AIフラグ監視
			this.ObserveEveryValueChanged(c => c.isAI)
				.DistinctUntilChanged()
				.Subscribe(aiFlag =>
				{
					ToggleAIController(aiFlag);
				});

			// PlayerNumber表示
			playerNoText = Instantiate(Resources.Load<GameObject>("Prefabs/Game/Player No Text"), transform.position,
				Quaternion.identity);
			playerNoText.GetComponent<Animator>().SetTrigger("Intro");

			playerNoText.LateUpdateAsObservable().Subscribe(_ =>
			{
				playerNoText.transform.position = transform.position;
				playerNoText.SetActive(this.gameObject.activeInHierarchy);
			});

			// 掴み候補マーカー表示処理
			this.ObserveEveryValueChanged(c =>
				{
					if (Grabbing != null) return null;
					return c.GrabTrigger.GrabbingCandidate;
				})
				.DistinctUntilChanged()
				.Subscribe(candidate =>
				{
					if (candidate == null)
					{
						if (_candidateMarker != null) Destroy(_candidateMarker);
						_candidateMarker = null;
					}
					else
					{
						if (_candidateMarker != null) Destroy(_candidateMarker);

						_candidateMarker = Instantiate(Resources.Load<GameObject>("Prefabs/Game/Grab Candidate Mark"));

						_candidateMarker.LateUpdateAsObservable()
							.Where(_ => candidate != null)
							.Subscribe(_ =>
							{
								_candidateMarker.transform.position = candidate.transform.position;
								_candidateMarker.transform.Rotate(Vector3.up, 180 * Time.deltaTime);

								_candidateMarker.GetComponent<Renderer>().material.color = PlayerColors.Colors[PlayerNo];
							})
							.AddTo(_candidateMarker);
					}
				});

			this.ObserveEveryValueChanged(c => c.Grabbing)
				.DistinctUntilChanged()
				.Subscribe(grabbing =>
				{
					if (grabbing == null)
					{
						if (_grabbingMarker != null) Destroy(_grabbingMarker);
						_grabbingMarker = null;
					}
					else
					{
						if (_grabbingMarker != null) Destroy(_grabbingMarker);

						_grabbingMarker = Instantiate(Resources.Load<GameObject>("Prefabs/Game/Grabbing Mark"));

						_grabbingMarker.LateUpdateAsObservable()
							.Subscribe(_ =>
							{
								if (Grabbing != null)
									_grabbingMarker.transform.position = Grabbing.transform.position;

								_grabbingMarker.GetComponent<Renderer>().material.color = PlayerColors.Colors[PlayerNo];
							})
							.AddTo(_grabbingMarker);
					}
				});

			this.OnCollisionEnterAsObservable()
				.Where(col => col.gameObject.tag == "Gold")
				.Subscribe(col =>
				{
					var component = col.gameObject.GetComponent<GoldBehaviour>();

					component.Occupy(this, OccupationTier.Contact);
				});
			this.OnCollisionExitAsObservable()
				.Where(col => col.gameObject.tag == "Gold")
				.Subscribe(col =>
				{
					var component = col.gameObject.GetComponent<GoldBehaviour>();

					component.Free(this, OccupationTier.Contact);
				});

			_travelSubject = new Subject<float>();
			_hitSubject = new Subject<Unit>();
			_hittedSubject = new Subject<Unit>();
		}

		protected void Start ()
		{
			// AI Load
			if (isAI) {
				aiObject = Instantiate (AIPrefab, transform.position, Quaternion.identity);
				aiObject.transform.parent = this.transform;
				Controller = aiObject.GetComponent<CharacterAI>();
			}
			// 移動距離集計
			this.FixedUpdateAsObservable ()
				.Select (_ => transform.position)
				.Buffer (2, 1)
				.Where (t => t.Count == 2)
				.Select (t => (t [1] - t [0]).magnitude)
				.Where (_ => !HasEffect ("Stun"))
				.Subscribe (d => {
				_travelSubject.OnNext (d);
				// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Distance] += d;
			});

		}

		protected void FixedUpdate()
		{
			var tgtVel = TargetVelocity;
			var rgdVel = RigidBody.velocity;

			var dAng = Vector3.Angle(tgtVel, rgdVel);
			if (TurnLoss > 0) tgtVel *= 0.9f * Mathf.Pow(1 - Mathf.Abs(dAng) / 180f, TurnLoss) + 0.1f;

			RigidBody.AddForce((tgtVel - rgdVel) * Acceleration / Speed, ForceMode.Acceleration);

			if (Grabbing != null)
			{
				Grabbing.GetComponent<Rigidbody>().AddForce((GrabbingTransform.position - Grabbing.transform.position) * 10, ForceMode.VelocityChange);
			}
		}

		protected void Update()
		{
			DirectionIndicator.GetComponent<Renderer>().material.color = PlayerColors.Colors[PlayerNo];

			int i = 0;
			foreach (var cooldownSession in CooldownSessions)
			{
				if (CooldownGaugeObjects.Length > i)
				{
					if (cooldownSession.IsCool)
					{
						CooldownGaugeObjects[i].GetComponent<Renderer>().enabled = false;
					}
					else
					{
						CooldownGaugeObjects[i].GetComponent<Renderer>().enabled = true;
						CooldownGaugeObjects[i].GetComponent<Renderer>().material
							.SetFloat("_Cutoff", Mathf.Max(cooldownSession.RemainingProgress, 0.001f));
						CooldownGaugeObjects[i].GetComponent<Renderer>().material
							.color = _cooldownGaugeGradient.Evaluate(1 - cooldownSession.RemainingProgress);
					}
				}
				i++;
			}

			if (Controller != null)
				Control();

			_direction = TargetVelocity.magnitude >= 0.1f ? TargetVelocity.normalized : _direction;

			_spriteAnimator.SetFloat("X", _direction.x * Mathf.Max(RigidBody.velocity.magnitude / Speed, 0.1f));
			_spriteAnimator.SetFloat("Y", _direction.z * Mathf.Max(RigidBody.velocity.magnitude / Speed, 0.1f));

			PivotTransform.rotation = Quaternion.Slerp(PivotTransform.rotation, Quaternion.LookRotation(_direction, Vector3.up), 0.1f);

			var finishedKeys = new List<string>();
			foreach (var kv in _skillEffects)
			{
				if (kv.Value.IsDone) finishedKeys.Add(kv.Key);
			}
			foreach (var key in finishedKeys)
			{
				_skillEffects.Remove(key);
			}

			if (CooldownSessions != null)
			{
				for (i = 0; i < CooldownSessions.Length; i++)
				{
					var isCoolBefore = CooldownSessions[i].IsCool;
					CooldownSessions[i].Update();
					if (CooldownSessions[i].IsCool && !isCoolBefore)
						Utilities.Particle.InstantiateParticle(CommonParticles.CooledParticle, transform.position);
				}
			}
		}

		/// <summary>
		/// 基礎となる入力処理関数。
		/// </summary>
		/// <param name="pad">PlayerNoに対応するパッド。</param>
		protected virtual void Control()
		{
			if (IsControllable)
			{
				if (IsMobile)
				{
					var v = new Vector3
					{
						x = Controller.GetAxis(MappedAxis.Horizontal),
						z = Controller.GetAxis(MappedAxis.Vertical)
					};
					if (v.magnitude > 1) v.Normalize();
                    Move(v);
				}

				if (Controller.GetButtonDown(MappedButton.Grab))
				{
					if (Grabbing == null)
					{
						if (GrabTrigger.GrabbingCandidate != null)
						{
							Grab(GrabTrigger.GrabbingCandidate);
						}
					}
					else
					{
						Release();
					}
				}
			}
		}

        public void Move(Vector3 v)
        {
            if (v.magnitude >= 1) v.Normalize();
            TargetVelocity = v * (Speed + SpeedIncrement);
        }

        public void Look (Vector3 v)
		{
			_direction = v.normalized;
		}

		public void Stop ()
		{
			TargetVelocity = Vector3.zero;
		}

		/// <summary>
		/// GrabbableなComponentを持つGameObjectをつかむ。
		/// </summary>
		/// <param name="grabbable">つかむGameObject。</param>
		public void Grab (GameObject grabbable)
		{
			Release ();

			Grabbing = grabbable;

			var c = Grabbing.GetComponent<GrabbableBehaviour> ();

			c.Occupy (this, OccupationTier.Grab);

			c.IsGrabbable = false;
			c.grabPlayer = this;
			c.OnGrabbed ();


			Utilities.Particle.InstantiateParticle(CommonParticles.GrabParticle, Grabbing.transform.position);

			AudioSource.PlayOneShot(GrabClip);
		}

		/// <summary>
		/// つかんでいるGameObjectを離す。
		/// </summary>
		public void Release()
		{
			if (Grabbing == null) return;

			var c = Grabbing.GetComponent<GrabbableBehaviour>();
			c.Free(this, OccupationTier.Grab);
			c.IsGrabbable = true;
			c.grabPlayer = null;
			Grabbing.GetComponent<Rigidbody>().AddForce(Direction * ReleaseImpulse, ForceMode.VelocityChange);
			c.OnReleased();


			Utilities.Particle.InstantiateParticle(CommonParticles.ReleaseParticle, Grabbing.transform.position);

			AudioSource.PlayOneShot(ReleaseClip);

			Grabbing = null;
		}

		/// <summary>
		/// スキル効果を付与する。
		/// </summary>
		/// <param name="effectName">スキル効果の名前。</param>
		/// <param name="effect">付与するスキル効果。</param>
		public void AddEffect(string effectName, Effect effect)
		{
			if (_skillEffects.ContainsKey(effectName))
			{
				_skillEffects[effectName].Dispose();
				_skillEffects.Remove(effectName);
			}

			_skillEffects.Add(effectName, effect);
			effect.Subscribe();
		}

		public bool HasEffect(string effectName)
		{
			return _skillEffects.ContainsKey(effectName);
		}

		public void CancelEffect(string effectName)
		{
			var candidates = new List<KeyValuePair<string, Effect>>(_skillEffects.Where(e => e.Key == effectName));
			foreach (var candidate in candidates)
			{
				candidate.Value.Dispose();
				_skillEffects.Remove(candidate.Key);
			}
		}

		public void CancelEffectAll()
		{
			foreach (var entity in _skillEffects)
			{
				entity.Value.Dispose();
			}

			_skillEffects.Clear();
		}

		public void CancelEffect(EffectTag tag)
		{
			var candidates = new List<KeyValuePair<string, Effect>>(_skillEffects.Where(e => e.Value.Tags.Contains(tag)));
			foreach (var candidate in candidates)
			{
				candidate.Value.Dispose();
				_skillEffects.Remove(candidate.Key);
			}
		}

		protected void OnRenderObject()
		{
			// デバッグ用の線描画

			/*
			_lineMaterial.SetPass(0);

			GL.PushMatrix();

			GL.Begin(GL.LINES);

			GL.Color(Color.cyan);
			GL.Vertex(transform.position);
			GL.Vertex(transform.position + TargetVelocity);

			GL.Color(Color.blue);
			GL.Vertex(transform.position);
			GL.Vertex(transform.position + RigidBody.velocity);

			GL.End();

			GL.PopMatrix();
			*/
		}

		/// <summary>
		/// スタンする。
		/// </summary>
		/// <param name="duration">スタンする時間。</param>
		public void Stun(float duration)
		{
			GameObject stunEffectObject = null;
			IDisposable subscription = null;

			var stunSequence = Observable.Defer(() =>
				{
					this.Release();
					this.TargetVelocity = Vector3.zero;

					stunEffectObject = Instantiate(StunnedEffectPrefab);
					stunEffectObject.transform.position = this.transform.position; // フリッカー防止
					stunEffectObject.UpdateAsObservable().Subscribe(_ =>
					{
						stunEffectObject.transform.position = this.transform.position;
					}).AddTo(stunEffectObject);

					StunGauge.GetComponent<Renderer>().enabled = true;

					float stunRemaining = 0.0f;
					subscription = this.UpdateAsObservable()
						.Subscribe(_ =>
						{
							this.IsControllable = false;
							this.TargetVelocity = Vector3.zero;
							StunGauge.GetComponent<Renderer>().material.SetFloat("_Cutoff", Mathf.Max(stunRemaining / duration, 0.001f));
							stunRemaining += Time.deltaTime;
						});

					_stunTime = duration;

					return Observable.ReturnUnit();
				})
				.SelectMany(_ => this.UpdateAsObservable())
				.Do(_ =>
				{
					_stunTime -= Time.deltaTime;
					if (_stunTime < 0) _stunTime = 0;
				})
				.TakeUntil(Observable.Timer(TimeSpan.FromSeconds(duration)))
				.DoOnCompleted(() =>
				{
					subscription.Dispose();
					Destroy(stunEffectObject);
					StunGauge.GetComponent<Renderer>().enabled = false;

					this.IsControllable = true;
				})
				.AsUnitObservable();

			this.CancelEffect(EffectTag.SkillActive);
			this.CancelEffect(EffectTag.SkillCasting);

			if (!HasEffect("Stun") || _stunTime < duration)
			{
				this.AddEffect("Stun", new Effect(stunSequence, () =>
				{
					subscription.Dispose();
					Destroy(stunEffectObject);
					StunGauge.GetComponent<Renderer>().enabled = false;


					this.IsControllable = true;
				})
				{
					Tags = new HashSet<EffectTag>() { EffectTag.Debuf }
				});
			}

			// 喰らい数集計
			_hittedSubject.OnNext(Unit.Default);
			// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Damages]++;
		}

        public GameObject GetGrabCandidate()
        {
            return GrabTrigger.GrabbingCandidate;
        }

        public void SetDefaultPosition (int playerNo)
		{
			Vector3 pos = Vector3.zero;
			switch (playerNo) {
			case 0:
				pos = new Vector3(-20, 1, 20);
				break;
			case 1:
				pos = new Vector3(20, 1, 20);
				break;
			case 2:
				pos = new Vector3(-20, 1, -20);
				break;
			case 3:
				pos = new Vector3(20, 1, -20);
				break;
			}
			transform.position = pos;
		}

		public void ToggleAIController (bool AIflag)
		{
            if (this.isAI == AIflag)
            {
                return;
            }
            else
            {
                this.isAI = AIflag;
            };
			if (AIflag) {
				if (aiObject == null) {
					aiObject = Instantiate (AIPrefab, transform.position, Quaternion.identity);
					aiObject.transform.parent = this.transform;
				}
				Controller = aiObject.GetComponent<CharacterAI> ();
			} else {
				if (aiObject != null)
					Destroy (aiObject);
				if (MappedInput.InputDevices.Count > PlayerNo) {
					Controller = new LocalInputDevice (MappedInput.InputDevices [PlayerNo]);
				} else {
					Controller = new LocalInputDevice();
				}
			}
		}

		void OnDestroy()
		{
			CancelEffectAll();
		}
	}
}