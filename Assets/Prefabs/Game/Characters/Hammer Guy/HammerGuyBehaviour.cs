﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace ProjectGold.Character
{
	/// <summary>
	/// ハンマーくん。
	/// ・ダッシュ可能。ダッシュ中のハンマーくんに当たった相手はスタン。
	/// ・ハンマー振り下ろし。当たった相手はスタン。
	/// </summary>
	public class HammerGuyBehaviour : CharacterBaseBehaviour
	{
		/// <summary>
		/// ダッシュのクールダウン時間。sec。
		/// </summary>
		[Header("ダッシュ")]
		public float DashCooldown;

		/// <summary>
		/// ダッシュ時のスピード。unit/sec。
		/// </summary>
		public float DashSpeed;

		/// <summary>
		/// ダッシュの時間。sec。
		/// </summary>
		public float DashDuration;

		/// <summary>
		/// ダッシュで当たった相手の吹っ飛び速度。unit/sec。
		/// </summary>
		public float DashImpact;

		/// <summary>
		/// ダッシュで当たった相手のスタン時間。sec。
		/// </summary>
		public float DashStunDuration;

		/// <summary>
		/// ダッシュ時に当たり判定を増やす倍率。
		/// </summary>
		public float DashColliderScale;

		[SerializeField]
		private ParticleSystem _dashParticlePrefab;
		[SerializeField]
		private GameObject _dashAuraObject;

		/// <summary>
		/// ハンマー振り下ろしのクールダウン時間。sec。
		/// </summary>
		[Header("ハンマー振り下ろし")]
		public float HammerCooldown;

		/// <summary>
		/// ハンマー振り下ろしのチャージ時間。sec。
		/// </summary>
		public float HammerChargeTime;

		/// <summary>
		/// ハンマー振り下ろしの最大中心射程。unit。
		/// </summary>
		public float HammerMaxDistance;

		/// <summary>
		/// ハンマー振り下ろしの最大半径。unit。
		/// </summary>
		public float HammerMaxRadius;

		/// <summary>
		/// ハンマー振り下ろしのスタンする時間。sec。
		/// </summary>
		public float HammerStunDuration;

		/// <summary>
		/// ハンマー振り上げ中の移動速度倍率。
		/// </summary>
		public float HammerSlowdownScale;

		private float _colliderRadius;

		[SerializeField] public GameObject HammerTrigger;

		[SerializeField] private GameObject HammerIndicator;

		[SerializeField] private ParticleSystem _hammerParticlePrefab;
		[SerializeField] private ParticleSystem _chargeParticle;

		[SerializeField] private Animator HammerEffectAnimator;

		[SerializeField] private AudioClip _dashClip, _hammerClip, _chargeClip, _hitClip;

		private List<CharacterBaseBehaviour> _hammerStunCandidates;

		private Subject<Unit> _releaseSubject;

		private float _baseSpeed;

		protected new void Awake()
		{
			base.Awake();

			CooldownSessions = new CooldownSession[]
			{
				new CooldownSession(DashCooldown),
				new CooldownSession(HammerCooldown)
			};

			_colliderRadius = GetComponent<CapsuleCollider>().radius;

			HammerIndicator.SetActive(false);
			HammerTrigger.layer = LayerMask.NameToLayer("OmniHit");

			_dashAuraObject.SetActive(false);

			_releaseSubject = new Subject<Unit>();

			_baseSpeed = Speed;

		}

		protected new void Start()
		{
			base.Start();

			_hammerStunCandidates = new List<CharacterBaseBehaviour>();
			HammerTrigger.OnTriggerEnterAsObservable()
				.Subscribe(go =>
				{
					var character = go.GetComponent<CharacterBaseBehaviour>();
					if (character != null)
						_hammerStunCandidates.Add(character);
				});
			HammerTrigger.OnTriggerExitAsObservable()
				.Subscribe(go =>
				{
					var character = go.GetComponent<CharacterBaseBehaviour>();
					if (character != null)
						_hammerStunCandidates.Remove(character);
				});
		}

		protected override void Control()
		{
			//if (isAI) return;
			base.Control();

			if (IsControllable)
			{
				if (Controller.GetButtonDown(MappedButton.Skill1) && CooldownSessions[0].IsCool && !HasEffect("Hammer"))
				{
					Dash();
				}
				if (Controller.GetButtonDown(MappedButton.Skill2) && CooldownSessions[1].IsCool)
				{
					Hammer();
				}
				if (Controller.GetButtonUp(MappedButton.Skill2)) ReleaseHammer();
			}
		}

		/// <summary>
		/// ダッシュを開始する。
		/// </summary>
		public void Dash()
		{
			var disposables = new IDisposable[2];
			CooldownSessions[0].Heat();

			var dashSequence = Observable.Defer(() =>
				{
					_dashAuraObject.SetActive(true);

					AudioSource.PlayOneShot(_dashClip);

					var dashParticle = Utilities.Particle.InstantiateParticle(_dashParticlePrefab);
					dashParticle.gameObject.transform.position = transform.position;
					dashParticle.gameObject.transform.rotation = PivotTransform.rotation;

					GetComponent<CapsuleCollider>().radius = _colliderRadius * DashColliderScale;

					var dashVel = Direction * DashSpeed;
					var dashAutomation = this.UpdateAsObservable()
						.Subscribe(_ =>
						{
							IsControllable = false;
							TargetVelocity = dashVel;
						});

					var stunHit = this.OnCollisionEnterAsObservable()
						.Where(col => col.gameObject.tag == "Character" || col.gameObject.tag == "Gold")
						//.Select(col => col.gameObject.GetComponent<CharacterBaseBehaviour>())
						.Do(col =>
						{
							if (col.gameObject.tag == "Character")
							{
								var character = col.gameObject.GetComponent<CharacterBaseBehaviour>();

								character.Stun(DashStunDuration);

								Utilities.Particle.InstantiateParticle(CommonParticles.HitParticle,
									(transform.position + character.transform.position) / 2);

								character.RigidBody.AddForce((character.transform.position - transform.position).normalized * DashImpact,
									ForceMode.VelocityChange);

								AudioSource.PlayOneShot(_hitClip);

								_hitSubject.OnNext(Unit.Default);
								// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
							}
							else
							{
								var gold = col.gameObject.GetComponent<GoldBehaviour>();

								gold.OccupyTime(this, OccupationTier.Skill, 0.5f);
							}
						})
						.Subscribe();

					disposables = new[] { dashAutomation, stunHit };

					return Observable.ReturnUnit()
						.Delay(TimeSpan.FromSeconds(DashDuration));
				})
				.DoOnCompleted(() =>
				{
					_dashAuraObject.SetActive(false);

					GetComponent<CapsuleCollider>().radius = _colliderRadius;

					foreach (var effect in disposables)
					{
						effect.Dispose();
					}

					IsControllable = true;
				})
				.AsUnitObservable();

			AddEffect("Dash", new Effect(dashSequence, () =>
			{
				_dashAuraObject.SetActive(false);

				GetComponent<CapsuleCollider>().radius = _colliderRadius;

				foreach (var effect in disposables)
				{
					effect.Dispose();
				}

				IsControllable = true;
			})
			{
				Tags = new HashSet<EffectTag>() { EffectTag.SkillActive }
			});
		}

		/// <summary>
		/// ハンマー振り下ろしを開始する。
		/// </summary>
		public void Hammer()
		{
			var tempCharge = 0f;
			var hammerSequence = Observable.Defer(() =>
				{
					// IsControllable = false;
					// TargetVelocity = Vector3.zero;

					HammerIndicator.SetActive(true);

					HammerTrigger.transform.localPosition = Vector3.zero;
					HammerTrigger.GetComponent<SphereCollider>().radius = 0;

					HammerIndicator.transform.localPosition = Vector3.zero;
					HammerIndicator.transform.localScale = Vector3.zero;

					Speed *= HammerSlowdownScale;

					_spriteAnimator.SetBool("Charge", true);
					_chargeParticle.Play();

					AudioSource.PlayOneShot(_chargeClip);

					return this.UpdateAsObservable();
				})
				.Do(_ =>
				{
					tempCharge += Time.deltaTime / HammerChargeTime;
					if (tempCharge > 1) tempCharge = 1;

					HammerTrigger.transform.localPosition = Vector3.forward * Mathf.Lerp(0, HammerMaxDistance, tempCharge);
					HammerTrigger.GetComponent<SphereCollider>().radius = Mathf.Lerp(0, HammerMaxRadius, tempCharge);

					HammerIndicator.transform.localPosition = Vector3.forward * Mathf.Lerp(0, HammerMaxDistance, tempCharge);
					HammerIndicator.transform.localScale = Vector3.one * Mathf.Lerp(0, HammerMaxRadius, tempCharge);
				})
				.SkipUntil(this.UpdateAsObservable().Where(_ => !Controller.GetButton(MappedButton.Skill2)))
				.Take(1)
				.Do((_) =>
				{
					IsControllable = false;
					TargetVelocity = Vector3.zero;

					HammerIndicator.SetActive(false);

					_chargeParticle.Clear();
					_chargeParticle.Stop();

					CooldownSessions[1].Heat();

					HammerEffectAnimator.SetTrigger("Hammer");
					AudioSource.PlayOneShot(_hammerClip);
				})
				.Delay(TimeSpan.FromMilliseconds(1000.0 / 4))
				.DoOnCompleted(() =>
				{
					IsControllable = true;
					AudioSource.Stop();

					Speed = _baseSpeed;

					Utilities.Particle.InstantiateParticle(_hammerParticlePrefab, HammerTrigger.transform.position);

					IsControllable = true;
					foreach (var character in _hammerStunCandidates)
					{
						character.Stun(HammerStunDuration);

						character.GetComponent<Rigidbody>().AddForce((character.transform.position - transform.position).normalized * 100, ForceMode.VelocityChange);

						Utilities.Particle.InstantiateParticle(CommonParticles.HitParticle, character.transform.position);

						_hitSubject.OnNext(Unit.Default);
						// Environment.PlayerDatas[PlayerNo][Environment.DataCategory.Hits]++;
					}
					_spriteAnimator.SetBool("Charge", false);
				})
				.AsUnitObservable();

			AddEffect("Hammer", new Effect(hammerSequence, () =>
			{
				IsControllable = true;
				HammerIndicator.SetActive(false);

				Speed = _baseSpeed;

				_spriteAnimator.SetBool("Charge", false);
				_chargeParticle.Stop();

				AudioSource.Stop();
			})
			{
				Tags = new HashSet<EffectTag>() { EffectTag.SkillActive }
			});

		}

		/// <summary>
		/// ハンマーのチャージ解除
		/// </summary>
		public void ReleaseHammer()
		{
			_releaseSubject.OnNext(Unit.Default);
		}


	}
}