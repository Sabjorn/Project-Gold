﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ProjectGold;
using ProjectGold.Character;
using UniRx;
using UniRx.Triggers;
using BehaviourTree;

public class HammerGuyAI_1 : CharacterAI{

	
	[SerializeField] private GameObject hammerSearch;
	private GameObject hammerTarget;
	[SerializeField] private GameObject dashSearch;
	private GameObject dashTarget;
	private bool isHammerCharge = false;


	// Use this for initialization
	new void Start () {
        base.Start();
        behaviour = GetComponentInParent<HammerGuyBehaviour>();

        dashSearch.OnTriggerEnterAsObservable()
        	.Where(col => col.gameObject.tag == "Character")
        	.Subscribe(col => 
        	{
        		dashTarget = col.gameObject;
        	});
		dashSearch.OnTriggerExitAsObservable()
			.Where(col => dashTarget != null && dashTarget == col.gameObject)
			.Subscribe(col =>
			{
				dashTarget = null;
			});

        hammerSearch.OnTriggerEnterAsObservable()
        	.Where(col => col.gameObject.tag == "Character")
			.Subscribe(col =>
			{
				hammerTarget = col.gameObject;
				
			});
		hammerSearch.OnTriggerExitAsObservable()
			.Where(col => hammerTarget != null && hammerTarget == col.gameObject)
			.Subscribe(col =>
			{
				hammerTarget = null;
			});

        // AI Node
        rootNode = new SequencerNode(new List<NodeBase>
        {
            //new ActionNode(Think),
            new SelectorNode(new List<NodeBase>
            {
            	new DecoratorNode(IsHammerReleaseTiming, new ActionNode(ReleaseHammer)),
				new DecoratorNode(IsHammerCharge, new ActionNode(MoveHammerTarget)),
            	new DecoratorNode(IsDashTarget, new ActionNode(Dash)),
            	new DecoratorNode(IsHammerTarget, new ActionNode(HammerCharge)),
				new DecoratorNode(IsGrabbingCoin, new SelectorNode(new List<NodeBase>{
		    		new DecoratorNode(IsOnHomeWithCoin, new ActionNode(ReleaseCoin)),
		    		new ActionNode(Dash),
					new ActionNode(BackHome)
		    	})),
		    	commonNode
            })
        });

	}


	// Actions

    bool Dash ()
	{
		//return false;

		if (behaviour.CooldownSessions [0].IsCool && isHammerCharge == false) {
			Input.SetButton(MappedButton.Skill1);
			return true;
		}else return false;
    }

    bool HammerCharge ()
	{
		if (behaviour.CooldownSessions [1].IsCool) {
			Input.SetButton(MappedButton.Skill2);
			return true;
		}
		return false;
	}

	bool ReleaseHammer ()
	{
		return true;
	}

	bool MoveHammerTarget ()
	{
		if (hammerTarget != null) {
			var v = hammerTarget.transform.position - behaviour.transform.position;
			Input.SetAxis(new Vector2(v.x, v.z));
			Input.SetButton(MappedButton.Skill2);
			return true;
		}
		return false;
	}

    // Conditions

    bool IsHammerTarget ()
	{
		if(hammerTarget != null) return true;
		return false;
	}

	bool IsHammerReleaseTiming ()
	{
		if (isHammerCharge) {
			if (((HammerGuyBehaviour)behaviour).HammerTrigger.GetComponent<SphereCollider> ().radius == ((HammerGuyBehaviour)behaviour).HammerMaxRadius) {
				return true;
			}
			if (hammerTarget == false) {
				return true;
			}
		}
		return false;
	}

	bool IsHammerCharge()
	{
		return isHammerCharge;
	}

	bool IsDashTarget ()
	{
		if(dashTarget != null) return true;
		return false;
	}

    // Update is called once per frame
    void Update ()
	{
		base.Update ();
		isHammerCharge = Input.Button[MappedButton.Skill2];

	}
}
